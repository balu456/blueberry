module.exports = function(grunt) {

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
		less: {
			development: {
				options: {
					paths: ["css"],
					strictImports : true,
					sourceMap: true,
					sourceMapFilename: 'css/styles.css.map',
					sourceMapRootpath:'../',
					sourceMapURL: 'styles.css.map'
				},
				files: {"css/styles.css": "css/less/styles.less"}
			},
			minify: {
				options: {
					paths: ["css"],
					strictImports : true,
					sourceMap: true,
					compress:true,
					sourceMapFilename: 'css/styles.min.css.map',
					sourceMapRootpath:'../',
					sourceMapURL: 'styles.min.css.map'
				},
				files: {"css/styles.min.css": "css/less/styles.less"}
			}
		},
		autoprefixer:{
			options: {
				browsers:['last 3 versions', 'ie > 8']
			},
			styleDev: {
				options: {
					map:true,
				},
				files: {
					'css/styles.css': 'css/styles.css'
				}
			},
			style: {
				files: {
					'css/styles.min.css': 'css/styles.min.css'
				}
			}

		},
		watch: {
			styles: {
				files: ['css/less/**/*.less'],
				tasks: ['less:development', 'less:minify', 'autoprefixer'],
				options: {
						nospawn: true
				}
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');

	grunt.registerTask('default', [
		'less:development',
		'less:minify',
		'autoprefixer',
		'watch'
	]);

};