<% cached 'SplitPage', $Link, $ID, PageCacheKey %>

<% if $getSplitPageMenu.Count > 1 %>
<!-- tabs -->
	<% include PageTabs Children=$getSplitPageMenu %>
<!-- /tabs -->
<% end_if %>

<article class="article">
	<h1>$Title</h1>
	<%--
	<div class="grid">
	
		<% if $Images %>
			<div class="col span_1_of_2<% if $Content %><% else %> no-content-full<% end_if %>">
				<% if $Content %><% include ImageCarousel Images=$Images, width=600, height=600 %><% else %><% include ImageCarousel Images=$Images %><% end_if %>
				
				<div class="elements">
					<% include Files %>
					<% if $YoutubeLink %>
						<a href="$YoutubeLink" target="_blank" class="video-button">$YoutubeTitle</a>
					<% end_if %>
				</div>
			</div>
		<% end_if %>

		<% if $Content %>
		<div class="col <% if $Images %>span_1_of_2<% else %>span_1_of_1<% end_if %>">
			$Content
			
		</div>
		<% end_if %>
	</div>
	--%>
	
	<div class="grid">
		<script src="//assets.juicer.io/embed.js" type="text/javascript"></script>
		<link href="//assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
		<div class="juicer-feed" data-feed-id="big-berry"></div>
	</div>
</article>

<!-- share -->
<% include SocialShare %>
<!-- /share -->
<% end_cached %>