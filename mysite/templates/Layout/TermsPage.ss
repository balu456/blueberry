<% cached 'TermsPage', $Link, $ID %>
<article class="article">
	<h1>$Title</h1>

	<div class="grid">

		<div class="col span_1_of_2">
			$LeftColumnText

		</div>

		<div class="col span_1_of_2">
			$Content

		</div>
	</div>
</article>
<% end_cached %>