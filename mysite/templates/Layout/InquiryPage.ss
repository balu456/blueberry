<div class="section contact-form">
	<h1>$Title</h1>

	$Content

	<div class="inquiry-form">
		<% if $Success %>
			<p><%t InquiryPage.SuccessMessage 'You have applied to newsletter' %></p>
		<% else %>	
			$InquiryForm
		<% end_if %>
	</div>

</div>