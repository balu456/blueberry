<% with $Item %>
	<article class="article">
		<h1>$Title</h1>
		<div class="grid">
		
			<div class="col span_1_of_2">
			
				<% if $Image %>
				<img src="$Image.CroppedImage(832, 832).URL" alt="$Image.Title">
					
				<% end_if %>
			</div>
	
			<% if $Content %>
				<div class="col span_1_of_2">
					$Content
				</div>
			<% end_if %>
		</div>
	</article>

<% end_with %>