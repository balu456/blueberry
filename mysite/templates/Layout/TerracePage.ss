<% cached 'TerracePage', TerracePageCacheKey %>
<div class="puzzle">
	<div class="col-left">
	<%-- include ImageCarousel Images=$Images, width=832, height=684 --%>
	
	<% if $Images %>

	<div class="slideshow">
		<div class="slides">
			<ul>
				<% loop $Images %>
				<li>
					<a href="$URL" rel="fancybox-1">
						<figure>
							$Image.CroppedImage(832, 684)

							<% if $Top.CarouselData %>
							<figcaption>
								<h5>
									<% with $Top.CarouselData %>
										$Title
										<strong>$SubTitle</strong>
										<% if $BestPrice %><%t General.FROM "že od" %> $BestPrice€ / <%t General.NIGHT "noč" %><% end_if %>
									<% end_with %>
								</h5>
							</figcaption>
							<% end_if %>
						</figure>
					</a>
				</li>
				<% end_loop %>
			</ul>
		</div>
	</div>

	<% end_if %>
	
	</div>

	<!-- col-right -->
	<div class="col-right">

		<div class="thumb">
			<%--<img src="$BoxImage.CroppedImage(488, 303).URL" alt="">--%>
			
			<% if $SmallImages %>
				<ul class="connected-slides">
				<% loop $SmallImages.Sort('SortOrder') %>
					<li>
						<figure>
							$CroppedImage(488, 303)
						</figure>
					</li>
				<% end_loop %>
				</ul>
			<% end_if %>
			
		</div>

		<div class="title purple">
			<img src="mysite/images/title_responsive_ghost.png" alt="" class="responsive-container">
			<h1>
				<strong>$Title</strong>
			</h1>
		</div>

	</div>
	<!-- /col-right -->

</div>
<!-- /puzzle -->

<article class="apartment">

	<!-- general-info -->
	<div class="general-info">
		<div class="description">
			$Content

		</div>

		<!-- foursome -->
		<section class="section foursome">

			<h2><%t Terrace.EXPOSED_NEWS "Izpostavljeno" %></h2>


			<!-- TO JE ZA ZBRISAT, STATIKA! SPODAJ JE LOOP -->
			<% if $TerraceAdditionals %>
				<div class="grid">
					<% loop $TerraceAdditionals %>
					<div class="col span_1_of_4">

						<% if $Content %>
							<a href="$Link">
						<% end_if %>
						
						<% with $Images.First %>
							$CroppedImage(302,190)
						<% end_with %>
							
						<h3>$Title</h3>
						<p>$Description</p>
						
						<% if $Content %>
							</a>
						<% end_if %>

					</div>
					<% end_loop %>
				</div>
			<% end_if %>
			
		</section>
		<!-- foursome -->

		<%--<br>--%>
		<%--<% if $Files %>--%>
		<%--<p class="file-list buttons-center">--%>
		<%--<% loop $Files %>--%>
			<%--<a href="$Link" target="_blank" download="$Title" class="file">$Title <b>{$Extension}</b></a>--%>
		<%--<% end_loop %>--%>
		<%--</p>--%>
		<%--<% end_if %>--%>

	</div>
	<!-- /general-info -->

</article>

<!-- share -->
<% include SocialShare %>
<!-- /share -->
<% end_cached %>
