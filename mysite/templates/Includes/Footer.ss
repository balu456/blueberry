<footer class="footer">
	<% if $ClassName != 'NewsletterPage' && URLSegment != home %>
	
		<% if $pageByClass('NewsletterPage') %>
			<section class="section">
				<h2><%t Footer.SUBSCRIBTION "E-obveščanje" %></h2>
				<p><%t Footer.SUBSCRIBTION_TEXT "Bodite obveščeni o naših novih ponudbah, akcijah..." %></p>

				<form action="$pageByClass('NewsletterPage').Link" method="get">
					<div class="wrapper">
						<input type="submit" value="<%t NewsletterPage.SUBSCRIBE_BUTTON "Subscribe" %>">
					</div>
				</form>

			</section>
		<% end_if %>
		
	<% end_if %>

	<div class="divider">
		<% if URLSegment != home %>
			<a href="#www-hosekra" class="top-button">Top</a>
		<% end_if %>
	</div>
	<div class="wrapper-clear">

		<% cached 'MenuMain', MenuCacheKey %>
			<% if $vMenu(2) %>
				<div class="footer-links-container">
					<ul class="links">
						<% loop $vMenu(2) %>
							<li><a href="$Link">$MenuTitle.XML</a></li>
						<% end_loop %>
					</ul>
				</div>
			<% end_if %>
		<% end_cached %>

		<% with $SiteConfig %>
			<% if $Twitter || $Facebook || $Pinterest || $Youtube || $GooglePlus || $Instagram || $Flickr || $TripAdviser || $Medium || $Issuu %>
				<ul class="social">
					<% if $Twitter %><li><a class="tw" href="$Twitter" target="_blank">Twitter</a></li><% end_if %>
					<% if $Facebook %><li><a class="fb" href="$Facebook" target="_blank">Facebook</a></li><% end_if %>
					<% if $Pinterest %><li><a class="pi" href="$Pinterest" target="_blank">Pinterest</a></li><% end_if %>
					<% if $Youtube %><li><a class="yt" href="$Youtube" target="_blank">Youtube</a></li><% end_if %>
					<% if $GooglePlus %><li><a class="gp" href="$GooglePlus" target="_blank">Google Plus</a></li><% end_if %>
					<% if $Instagram %><li><a class="ig" href="$Instagram" target="_blank">Instagram</a></li><% end_if %>
					<% if $Flickr %><li><a class="fl" href="$Flickr" target="_blank">Flickr</a></li><% end_if %>
					<% if $TripAdviser %><li><a class="tr" href="$TripAdviser" target="_blank">Trip Adviser</a></li><% end_if %>
					<% if $Medium %><li><a class="md" href="$Medium" target="_blank">Medium</a></li><% end_if %>
					<% if $Issuu %><li><a class="is" href="$Issuu" target="_blank">Issuu</a></li><% end_if %>
					<% if $Tumblr %><li><a class="tm" href="$Tumblr" target="_blank">Tumblr</a></li><% end_if %>
					<% if $LinkedIn %><li><a class="li" href="$LinkedIn" target="_blank">LinkedIn</a></li><% end_if %>
					<% if $Vkontakte %><li><a class="vk" href="$Vkontakte" target="_blank">Vkontakte</a></li><% end_if %>
				</ul>
			<% end_if %>
		<% end_with %>

	</div>
</footer>

$RequiredJS