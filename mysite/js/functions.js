/*------------------------------------------------------------------
# [Custom Javascripts]

+ Navigation
+ Slideshow
+ Inquiry Resize
+ Inquiry Form Resize
+ (window).load
+ (document).ready
+ (window).resize
+ (window).scroll

------------------------------------------------------------------*/



var deviceMobile, deviceTablet, deviceTabletPortrait, deviceDesktop, CSStransitionsWorking,

	device = {
		mobile: function () {
			deviceMobile = Modernizr.mq('all and (max-width: 767px)');
		},
		tablet: function () {
			deviceTablet = Modernizr.mq('all and (min-width: 768px) and (max-width: 1024px)');
		},
		tabletPortrait: function () {
			deviceTabletPortrait = deviceTablet && Modernizr.mq('all and (orientation: portrait)');
		},
		desktop: function () {
			deviceDesktop = Modernizr.mq('all and (min-width: 1025px)');
		},
		CSStransitionsSupport: function () {
			CSStransitionsWorking = !$('html.no-csstransitions').length;
		},
		calculate: function () {
			this.mobile();
			this.tablet();
			this.tabletPortrait();
			this.desktop();
			this.CSStransitionsSupport();
		}
	}
;

device.calculate();



/*------------------------------------------------------------------
# [Navigation]
------------------------------------------------------------------*/

$('.navigation-trigger').click(function(e) {
	e.preventDefault();
	$(this).toggleClass('open');
	$('.navigation-primary').slideToggle();
});


/*------------------------------------------------------------------
# [Camp list]
------------------------------------------------------------------*/
$('.CampList select').change(function(e) {
	this.form.submit();
});


/*------------------------------------------------------------------
# [Inquiry form]
------------------------------------------------------------------*/

if (!deviceMobile) {

	var inquiryForm = $('.inquiryform');
	var inquiryFrom = $('.arrival[type="text"]', inquiryForm);
	var inquiryTo = $('.departure[type="text"]', inquiryForm);

	inquiryFrom.datepicker({
		showOn: "both",
		buttonImage: "mysite/images/calendar.png",
		buttonImageOnly: true,
		defaultDate: '+1w',
		changeMonth: true,
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			inquiryTo.datepicker('option', 'minDate', selectedDate);
		}
	});

	inquiryTo.datepicker({
		showOn: "both",
		buttonImage: "mysite/images/calendar.png",
		buttonImageOnly: true,
		defaultDate: '+1w',
		changeMonth: true,
		numberOfMonths: 1,
		onClose: function(selectedDate) {
			inquiryFrom.datepicker('option', 'maxDate', selectedDate);
		}
	});

}

/*------------------------------------------------------------------
# [Slideshow]
------------------------------------------------------------------*/

function slideshow() {
	var activeID = 0;
	var depthIndex = 0;
	var delay = 5000;
	var totalItems = $('.slideshow .slides ul li').length;
	var autoplayInterval = setInterval(autoplay, delay, activeID);

	if (totalItems == 1) {
		$('.slides-navigation').hide();
		clearInterval(autoplayInterval);
	} else {
		$('.slides-navigation').show();
		//$('.slides-current').html('1');
		//$('.slides-all').html(totalItems);
	}

	$('.slides-navigation .next').click(function() {
		id = activeID;
		showItem(id, 'next');
	});

	$('.slides-navigation .prev').click(function() {
		id = activeID;
		showItem(id, 'prev');
	});

	function showItem(id, direction, autoplay) {

		if (direction == 'prev') {
			if (activeID <= 0) {
				id = totalItems - 1;
			} else {
				id = activeID - 1;
			}
		}

		if (direction == 'next') {
			if (activeID >= totalItems - 1) {
				id = 0;
			} else {
				id = activeID + 1;
			}
		}

		if (activeID != id) {
			$('.slideshow .slides ul li').eq(id).css({'z-index' : depthIndex++, 'display' :'none'});
			$('.slideshow .slides ul li').eq(id).stop().fadeIn(300);
			activeID = id;
		}

		if (!autoplay) {
			clearInterval(autoplayInterval);
		}

		$('.slides-current').html(activeID + 1);
	}

	function autoplay(id) {
		if (id >= totalItems) {
			id = 0;
		} else {
			id = activeID + 1;
		}

		//autoplay
		//showItem(id, 'next', true);
	}
}


/*------------------------------------------------------------------
# [Inquiry Resize]
------------------------------------------------------------------*/

function resizeInquiry() {
	windowWidth = $(window).width();

	if (windowWidth <= 440) {
		$('.featured-offer .responsive-container').height($('.featured-offer .responsive-container').width());
	} else {
		$('.featured-offer .responsive-container').height('auto');
	}
}


/*------------------------------------------------------------------
# [Inquiry Form Resize]
------------------------------------------------------------------*/

function resizeInquiryForm() {
	windowWidth = $(window).width();

	if (windowWidth > 900) {
		$('.puzzle .col-right').height($('.puzzle .col-left').height());
		$('.puzzle .col-right .inquiry-form').css('margin-top', -($('.puzzle .col-right .inquiry-form').height()/2));
	} else {
		$('.puzzle .col-right').height('auto');
		$('.puzzle .col-right .inquiry-form').css('margin-top', 0);
	}
}


/* mobile date fields */

var mobileDateFields = {
	init: function(){
		var now = new Date(),
				day = ("0" + now.getDate()).slice(-2),
				month = ("0" + (now.getMonth() + 1)).slice(-2),
				today = now.getFullYear()+"-"+(month)+"-"+(day);
		$('input.departure, input.arrival').attr('type','date').val(today);
	}
};


/* placeholder remove on focus */

$('input,textarea').focus(function(){
	$(this).data('placeholder', $(this).attr('placeholder'));
	$(this).attr('placeholder','');
}).blur(function(){
	$(this).attr('placeholder',$(this).data('placeholder'));
});


/* responsive price table */

if ($('table.prices').length) {
	$('div.description>table.prices').each(function(){
		var t = $(this),
				title = t.find('th.col-1').html(),
				pricelist = t.html();

		t.after('<div class="prices-mobile"><h3>' + title +'</h3><table class="prices">' + pricelist + '</table></div>');
		$('div.prices-mobile').find('th.col-1').remove();
	});
}


function play_sky() {
	var sky = $('div.sky');
	if (sky.length) {

		var ml = ($(window).width() - $('div.page').width()) / 2;

		sky.css({
			marginLeft: -ml,
			marginRight: -ml,
			width: $(window).width()
		});
	}
}



/*------------------------------------------------------------------
# [Document.ready ]
------------------------------------------------------------------*/

$(document).ready(function() {
	
	$('.mobile-trigger').click(function(e){
		e.preventDefault();
		var t = $(this);
		if(t.hasClass('is-open')) {
			t.removeClass('is-open')
		} else {
			t.addClass('is-open')
		}

	});
	

	if (!deviceMobile) {
		//select2
		$('form.inquiryform').find('select').select2();
	}
	// nam mobilnih devicih pokazi date field
	else {
		//

		mobileDateFields.init();
	}

	$('div.mobile-tabs>select').on('change', function () {
		window.location.href = $('div.mobile-tabs>select').find(':selected').val();
	});

	//select2
	if (!deviceMobile) {
		$('div.mobile-tabs>select, #destination, #persons').select2({
			theme: 'mobile-menu'
		});
	}

	//responsive sliders
	var slider = $('div.slideshow ul'),
		moreThanOne = slider.find('li').length,
		prevArrow = '<div class="arrow-left"></div>',
		nextArrow = '<div class="arrow-right"></div>';
	if (slider.length && moreThanOne > 1) {
		slider.slick({
			infinite: true,
			speed: 300,
			prevArrow: prevArrow,
			nextArrow: nextArrow,
			dots: true,
			dotsClass: 'slick-total',
			customPaging: function (slider, i) {
				return '<div class="total"><span class="slides-current">' + (i + 1) + '</span> / <span class="slides-all">' + slider.slideCount + '</span></div>';
			}
		});

		setTimeout(function () {
			$('li.slick-cloned').find('a').removeAttr('rel');
		}, 100);


		if ($('ul.connected-slides').length) {
			$('ul.connected-slides').slick({
				infinite: true,
				speed: 300,
				prevArrow: false,
				nextArrow: false,
				dots: false,
				draggable: false
			});

			$('div.slideshow ul').on('afterChange', function () {
				var isCurrentSlide = $(this).find('li.slick-active').data('slick-index');
				//alert(isCurrentSlide);
				$('ul.connected-slides').slick('slickGoTo', isCurrentSlide, true);
			});

		}

	}

	//top smooth scroll
	$('a.top-button').on('click', function (e) {
		e.preventDefault();


		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html,body').animate({
				scrollTop: target.offset().top
			}, 800);
			return false;
		}

	});


	if ($('.featured-offer .responsive-container').length) {
		resizeInquiry();
	}

	if ($('.puzzle .col-right .inquiry-form').length) {
		resizeInquiryForm();
	}

	// init paginated list (if any)
	var News = pl.init($('.addMore_News,.CampNewsList'), $('.loadMore_News'));
	var NewsFromWorld = pl.init($('.addMore_NewsFromWorld'), $('.loadMore_NewsFromWorld'));
	var NewsBigBarry = pl.init($('.addMore_NewsBigBarry'), $('.loadMore_NewsBigBarry'));
	var Stories = pl.init($('.addMore_Stories'), $('.loadMore_Stories'));
    var SocialPosts = pl.init($('.addMore_SocialPost'), $('.loadMore_SocialPost'));
	
	
	// news
	var newsForm = $('form.tags');
	$('input', newsForm).click(function () {
		$(this).parents('form').submit();
	});
	 
	if (window.innerWidth < 1025) {
		$("[rel='fancybox-1']").each(function(){
			var t= $(this);
			if(!t.hasClass('fancybox.iframe')) {
				t.on('click', function (e) {
						e.preventDefault();
						$(this).css('cursor', 'default');
					})
					.removeAttr('rel');
			}
		})
	}
	$('a[rel="fancybox-1"]').fancybox({
		padding:0,
		topRatio:window.innerWidth < 1025 ? 0 :.5,
		margin:window.innerWidth < 1025 ? 20 : 5,
		openEffect: 	window.innerWidth < 1025 ? 'none' : 'elastic',
		closeEffect:	window.innerWidth < 1025 ? 'none' : 'elastic',
		nextEffect:		window.innerWidth < 1025 ? 'none' : 'elastic',
		prevEffect:		window.innerWidth < 1025 ? 'none' : 'elastic',
		width:1280, //for iframe only
		height:720, //for iframe only
		iframe: {
			preload:false
		}
	});

	play_sky();

});


$(window).on('load', function(){

	$('div.grid').equalheights({
		debounceResize:true,
		updateOnResize:true
	});

});

var el = document.getElementById('bigCookie'); 
if(el != null) {
	if (el.fireEvent) { 
		el.fireEvent('onclick'); 
	} 
	else 
	{ 
		var evObj = document.createEvent('Events'); 
		evObj.initEvent('click', true, false); 
		el.dispatchEvent(evObj); 
	}
}


/*------------------------------------------------------------------
# [Window.resize ]
------------------------------------------------------------------*/

$(window).resize(function () {

	device.calculate();

	play_sky();
	
	if ($('.featured-offer .responsive-container').length) {
		resizeInquiry();
	}

	if ($('.puzzle .col-right .inquiry-form').length) {
		resizeInquiryForm();
	}

	if (window.innerWidth < 1025) {
		$("[rel='fancybox-1']").each(function(){
			var t= $(this);
			if(!t.hasClass('fancybox.iframe')) {
				t.on('click', function (e) {
						e.preventDefault();
						$(this).css('cursor', 'default');
					})
					.removeAttr('rel');
			}
		})

	}
});

/*------------------------------------------------------------------
# [Window.scroll ]
------------------------------------------------------------------*/

//$(window).on('scroll', function() {});

/**********************
 * pl - Paginated List
 */
var pl = {
	button: '',
	list: '',
	listFilter: '',
	useval: true,
	
	init: function(pageClass, buttonClass) {

		if((list = pageClass).length > 0) {
			this.list = list;
			// Load more
			if((this.button = buttonClass).length > 0) {
				this.enableNext();
			}
		} else
			return;		
		
		return;
	},
	
	/**
	 * Hide more button
	 */
	hideButton: function() {
		if(pl.button.length > 0 && pl.button.is(':visible'))
			pl.button.hide();
	},
	
	/**
	 * Show more button
	 */
	showButton: function() {
		if(pl.button.length > 0 && !pl.button.is(':visible'))
			pl.button.show();
	},
	
	removeClassDisabled: function(className) {
		$(".loadMore_"+className).removeClass('disabled');
	},
	
	loadData: function(data) {
		$(".addMore_"+data.className).append(data.data);
	},
	
	changeHref: function(className, href) {
		$(".loadMore_"+className).attr('href', href);
	},
	
	showButtonNew: function(classname) {
		var btn = $(".loadMore_"+classname);
		if(btn.length > 0 && !btn.is(':visible'))
			btn.show();
	},
	
	hideButtonNew: function(classname) {
		var btn = $(".loadMore_"+classname);
		if(btn.length > 0 && btn.is(':visible'))
			btn.hide();
	},
	
	/**
	 * Enable next button handler
	 */
	enableNext: function() {
		this.button.click(function(e) {
			e.preventDefault();
			
			if($(this).hasClass('disabled'))
				return;
			
			$(this).addClass('disabled');

			$.ajax({
				url: $(this).attr('href'),
				dataType: 'json',
				data: 'class=' + $(this).attr('data-class') ,
				success: function(response) {
					//pl.list.append(response.data);
					pl.loadData(response);
					//pl.button.removeClass('disabled');
					pl.removeClassDisabled(response.className);

					/*
					if (response.more) {
						pl.button.attr('href', response.more)
						pl.showButton();
					}
					else pl.hideButton();
					*/
					if(response.more) {
						pl.changeHref(response.className, response.more);
						pl.showButtonNew(response.className);
					}
					else {
						pl.hideButtonNew(response.className);
					}
				}
			});
		});
	}
};

(function ( $ ) {
    $.fn.IssuuPopup = function() {
        $(this).on('click', function (e) {
            var embed = $(this).parents('.homepage-elements-item').find('.hidden').html();
            $("body").append('<div class="YouTubePopUp-Wrap YouTubePopUp-animation IssuuPopup"><div class="YouTubePopUp-Content"><span class="YouTubePopUp-Close"></span>' + embed + '</div></div>');
            if( $('.YouTubePopUp-Wrap').hasClass('YouTubePopUp-animation') ){
                setTimeout(function() {
                    $('.YouTubePopUp-Wrap').removeClass("YouTubePopUp-animation");
                }, 600);
            }
    
            $(".YouTubePopUp-Wrap, .YouTubePopUp-Close").click(function(){
                $(".YouTubePopUp-Wrap").addClass("YouTubePopUp-Hide").delay(515).queue(function() { $(this).remove(); });
            });
    
            e.preventDefault();
            return false;
        });
    }
}( jQuery ));

$(document).ready(function() {
	$('[href^="https://www.youtube"]').YouTubePopUp();
    $('a.issuu-popup').IssuuPopup();
    
});
	//jQuery RWD Image Maps
$(document).ready(function(e) {
	$('img[usemap]').rwdImageMaps();
});


 // When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function() {scrollFunction()};

	function scrollFunction() {
		myBtn = document.getElementById("myBtn")
		if ((document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) && myBtn) {
			//document.getElementById("myBtn").style.display = "block";	
		} else {
		//	document.getElementById("myBtn").style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0; // For Chrome, Safari and Opera 
		document.documentElement.scrollTop = 0; // For IE and Firefox
	}


// search bar
$(document).ready(function() {
			var searchTiggerButton = $(".search-trigger");
			var searchBar = $('.search-box');
	
			/* toggle navigation and search in mobile view */
			searchTiggerButton.click(function(e) {				
				e.preventDefault();
				if($(this).html()=='L')
					$(this).html('\'');
				else
					$(this).html('L');
					
				searchBar.toggle('fast');
				$('#gsc-i-id1').focus();
				return false;
				
			});
            var langlist = $('div.languages ul.langlist');
            $('a.current-lang').click(function(){
                if(langlist.is(":visible") == true){
                    langlist.fadeOut();
                }else{
                    langlist.fadeIn();
                }
                return false; 
            });
	
});

/*
$('.shareBtn').click( function() {
			
  FB.ui({
    method: 'feed',
    display: 'popup',
    link: 'https://developers.facebook.com/docs/',
    source: 'https://developers.facebook.com/docs/',
  }, function(response){});
	
	return false;
});
*/
$('.shareBtn').click( function() {  
  FB.ui({
    method: 'share',
    display: 'popup',
	href: this.getAttribute('data-href'),    
    picture: this.getAttribute('data-media'),
  }, function(response){
	});
    
  return false;
});


