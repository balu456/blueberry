<?php
/**
 * Use this configuration file for all project settings, that are not server (live, dev, stage) specific.
 * For server specific settings, use regular _config.php
 * 
 * This file is included at the end of _config.php
 * 
 */

// Override default language to english
Translatable::set_default_locale('en_US');

// translatable
Translatable::set_allowed_locales(array('sl_SI', 'en_US', 'it_IT', 'hr_HR', 'de_DE'));

Designer::add_extension('Translatable');
News::add_extension('Translatable');
NewsFromWorld::add_extension('Translatable');
NewsBigBarry::add_extension('Translatable');
Stories::add_extension('Translatable');


// make dev/build faster
Config::inst()->update('SS_Database', 'check_and_repair_on_build', false);

// JS always before body
Requirements::set_force_js_to_bottom(true);

/**
 * COOKIES
 */
CookiesDirective_ControllerExtension::$cdDefaultUse = true;
CookiesDirective_ControllerExtension::$cdShowOnlyOnce = true;
CookiesDirective_ControllerExtension::$cdType = 'C';

// sitemap.xml
GoogleSitemap::register_dataobject('DataObjectAsPage', 'always');