<?php

class HouseBanner extends DataObject {
	
	private static $singular_name = 'Banner hiše';
	private static $plural_name = 'Bannerji hiše';	
	
	private static $db = array(
		'Title' 		=> 'Varchar(255)',
		'Content'		=> 'HTMLText',
		'CustomLink'	=> 'Varchar(255)',
		//'Color1'		=> 'Varchar(255)',
		//'Color2'		=> 'Varchar(255)'
	);
	
	private static $has_one = array(
		'Image'		=> 'Image',
		'LogoImage'	=> 'Image',
		'PageLink'	=> 'Page',
	);
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldsToTab('Root.Main', array(
			TextField::create('Title')->setTitle('Naslov'),
			HtmlEditorField::create('Content')->setTitle('Vsebina'),
			UploadField::create('Image')->setTitle('Slika'),
			UploadField::create('LogoImage')->setTitle('Slika logotip'),
			TreeDropdownField::create('PageLinkID')->setTitle('Izberite povezavo na notranjo stran')->setSourceObject('SiteTree'),
			TextField::create('CustomLink')->setTitle('Custom link (ki ne obstaja v drevesni strukturi)'),
			//TextField::create('Color1')->setTitle('Barva ozadja (hex koda brez #)'),
			//TextField::create('Color2')->setTitle('Barva besedila (hex koda brez #)')
		));
		
		return $fields;
	}

	/**
	 * Return link
	 * @return boolean
	 */
	function getLinkURL() {
		if($this->PageLinkID > 0) {
			return $this->PageLink()->Link();
		} else if($this->CustomLink) {
			return $this->CustomLink;
		}
		return false;
	}
}