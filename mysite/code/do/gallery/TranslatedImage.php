<?php

class TranslatedImage extends DataObject {
	
	private static $singular_name = 'Slika galerije';
	
	private static $db = array(
		'Youtube'	=> 'HTMLText',
	);
	
	private static $has_one = array(
		'Image' => 'Image',
	);
	
	private static $many_many = array(
		'Tags' => 'TagImage',
	);
	
	private static $belongs_many_many = array(
		'Galleries' => 'ImageGallery',
	);

	static $_locale_DB_fields = array(
		'Title' => 'Varchar(255)',
	);
	
	public static $_locale_DB_tabs = false;
	
	static $_locale_DB_summary_fields = array(
		'Title:sl_SI',
		'Image.CMSThumbnail',
	);
	
	private static $field_labels = array(
		'Title' => 'Naziv',
		'Image' => 'Slika',
		'Image.CMSThumbnail' => 'Slika',
		'Tags' => 'Značke',
		'Youtube' => 'Youtube ID',
	);
	
	private static $searchable_fields = array(
		'Title-sl_SI',
		'Title-en_US',
	);
	
	function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName(array('Galleries'));
		$fields->addFieldsToTab('Root.Main', array(
			TextField::create('Youtube')->setTitle('Youtube ID'),
		));
		return $fields;
	}
	
	function getTitle() {
		return $this->LocaleField('Title', 'sl_SI');
	}

	/**
	 * Gets the relative URL accessible through the web.
	 *
	 * @uses Director::baseURL()
	 * @return string
	 */
	public function getYoutubeURL() {
		return '//www.youtube.com/embed/' . $this->owner->Youtube . '?autoplay=1&amp;controls=0&amp;showinfo=0';
	}
	
	/*
	 * Nebi blo slabo preverit, če je user res vnesel samo ID
	 * in odstranit nepotrebn del
	 */
	public function onBeforeWrite() {
		parent::onBeforeWrite();
		$this->owner->Youtube = self::getYoutubeID($this->owner->Youtube);
	}
	
	public static function getYoutubeID($url)
	{
		if(stristr($url,'youtu.be/'))
		{
			preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID);
			return $final_ID[4];
		}
		elseif(stristr($url,'youtube'))
		{
			@preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD);
			return $IDD[5];
		}
		else
		{
			return $url;
		}
	}
	
}