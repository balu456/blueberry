<?php

class ImageGallery extends DataObject {
	
	private static $singular_name = 'Galerija';
	
	private static $db = array(
		'EditorialTitle'	=> 'Varchar(255)',
// 		'Title'				=> 'Varchar(255)',
		'Description'		=> 'HTMLText',
		'ShowClouds'		=> 'Boolean',
	);
	
	static $_locale_DB_tabs = false;
	static $_locale_DB_fields = array(
		'Title'				=> 'Varchar(255)',
	);
	
	private static $belongs_many_many = array(
		'HousePages' => 'HousePage',
	);
	
	private static $many_many = array(
		'Images'	=> 'TranslatedImage',
		'Files'		=> 'File',
	);
	
	private static $many_many_extraFields = array(
		'Images'	=> array(
			'SortOrder' => 'Int',
		),
		'Files'	=> array(
			'SortOrder' => 'Int',
		),
	);
	
	private static $searchable_fields = array(
		'EditorialTitle' => array(
	 		"field" => "TextField", 
	 		"filter" => "PartialMatchFilter",
	 	),
		'Title-sl_SI' => array(
	 		"field" => "TextField", 
	 		"filter" => "PartialMatchFilter",
	 	),
		'Title-en_US' => array(
	 		"field" => "TextField", 
	 		"filter" => "PartialMatchFilter",
	 	),
		'Description' => array(
	 		"field" => "TextField", 
	 		"filter" => "PartialMatchFilter",
	 	),
	);
	
	private static $summary_fields = array(
		'EditorialTitle',
		'Title',
	);
	
	private static $field_labels = array(
		'Title'				=> 'Naziv galerije',
		'EditorialTitle'	=> 'Uredniški naziv galerije',
		'Images'			=> 'Slike galerije',
		'Files'				=> 'Datoteke galerije',
		'Description'		=> 'Opis',
		'ShowClouds'		=> 'Prikaži oblake okoli galerije',
	);
	
	function getCMSFields() {
		$fields = parent::getCMSFields();
		$galleryConf = new GridFieldConfig_RelationEditor();
		$galleryConf->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->replaceField('Images', new GridField('Images', 'Slike galerije', $this->Images()->sort('SortOrder'), $galleryConf));
		
		$fields->replaceField('Files', SortableUploadField::create('Files')->setTitle('Datoteke galerije')->setFolderName('Uploads/datoteke'));
		
		return $fields;
	}
	
	function getTitle() {
		return $this->LocaleField('Title');
	}
	
}