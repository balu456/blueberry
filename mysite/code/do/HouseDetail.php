<?php

class HouseDetail extends DataObject {
	
	private static $singular_name = 'Detajl hiše';
	private static $plural_name = 'Detajli hiše';	
	
	private static $db = array(
		'Title' 	=> 'Varchar(50)',
		'Label'		=> 'Varchar(255)',
		'Content'	=> 'HTMLText',
		'PageTitle'	=> 'Varchar(255)'
	);
	
	private static $has_one = array(
		'Image' => 'Image',
		'PageLink'	=> 'Page'
	);
	
	private static $belongs_many_many = array(
		'HouseVariationPages'	=> 'HouseVariationPage'
	);
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
	
		$fields->addFieldsToTab('Root.Main', array(
			TextField::create('Title')->setTitle('Naslov'),
			TextField::create('Label')->setTitle('Uredniška oznaka'),
			HtmlEditorField::create('Content')->setTitle('Vsebina'),
			UploadField::create('Image')->setTitle('Slika'),
			TextField::create('PageTitle')->setTitle('Naslov linka (gumba)'),
			TreeDropdownField::create('PageLinkID')->setTitle('Izberite povezavo na notranjo stran')->setSourceObject('SiteTree'),
		));
		
		return $fields;
	}
	
	/**
	 * Return title if entered, otherwise look for linked page title
	 * @return string
	 */
	public function getTitleLink() {
		$title = $this->PageTitle;
		if (isset($title) && !empty($title))
			return $title;
		if ($this->PageLinkID > 0)
			return $this->PageLink()->Title;
	
		return '';
	}

}