<?php

class CampIcon extends DataObject {
	
	private static $singular_name = 'Ikona kampa';
	private static $plural_name = 'Ikone kampa';	
	
	private static $db = array(
		'Title' 		=> 'Varchar(255)',
	);
	
	private static $has_one = array(
		'Image'	=> 'Image'
	);
	
	/**
	 * Gets fields used in the cms 
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
	
		$fields->addFieldsToTab('Root.Main', array(
			TextField::create('Title')->setTitle('Naslov'),
			UploadField::create('Image')->setTitle('Slika'),
		));
		
		return $fields;
	}

}