<?php

class TagNews extends DataObject {
	
	private static $singular_name = 'Značka';
	private static $plural_name = 'Značke';	

	private static $db = array(
		'Title'		=> 'Varchar(50)',
		'Locale'	=> 'Varchar(255)',
	);
	
	private static $belongs_many_many = array(
		'CampAttractions'	=> 'CampAttraction'
	);	
	
	private static $summary_fields = array(
		'Title',
		'Locale'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldsToTab('Root.Main', array(
			DropdownField::create('Locale')->setSource($this->setSourceLang())
		));
		
		return $fields;
	}
	
	public function setSourceLang() {
		$tmp = array();
		foreach(Translatable::get_allowed_locales() as $lang) {
			$tmp[$lang]	= $lang;
		}
		return $tmp;
	}
	
	public function Link() {	
		return '?tag[]=' . $this->ID;
	}	

}