<?php

class SubTagStories extends DataObject {
	
	private static $singular_name = 'Subtag';
	private static $plural_name = 'Subtags';	

	private static $db = array(
		'Title'	=> 'Varchar(50)',
		'Locale'	=> 'Varchar(255)',
	);
    
    static $has_one = array(
        'TagStories' => 'TagStories',
    );
	
	private static $summary_fields = array(
		'Title',
		'Locale'
	);
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldsToTab('Root.Main', array(
			DropdownField::create('Locale')->setSource($this->setSourceLang())
		));
		
		return $fields;
	}
	
	public function setSourceLang() {
		$tmp = array();
		foreach(Translatable::get_allowed_locales() as $lang) {
			$tmp[$lang]	= $lang;
		}
		return $tmp;
	}
	
	public function Link() {
		$news = StoriesList::get()->filter('ClassName', 'NewsList')->first();
		if (!$news) return false;
	
		return $news->Link() . '?tagS[]=' . $this->ID;
	}
    
    function TitleFirstWord(){
        $titleArr = explode(' ', $this->Title);
        return $titleArr[0];
    }	
    
    function CustomTitle(){
        return ($this->TagStories()->Title && $this->Title) ? ($this->TagStories()->Title . '->' . $this->Title) : '';
    }
}