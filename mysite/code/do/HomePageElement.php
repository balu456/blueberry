<?php

class HomePageElement extends DataObject {

	private static $singular_name = 'Element naslovne strani';
	private static $plural_name = 'Elementi naslovne strani';

	private static $db = array(
		'Title' 		  => 'Varchar(50)',
		'Content'		  => 'HTMLText',
		'Label'			  => 'Varchar(255)',
		'Telephone'		  => 'Varchar(255)',
		'ExternalLink'	  => 'Text',
		'OpenInNewWindow' => 'Boolean',
		'SortOrder'		  => 'Int',
        'IssuuPopup' => 'Boolean',
        'IssuuEmbed' => 'Text',
	);

	private static $has_one = array(
		'Image' 	=> 'Image',
		'PageLink'	=> 'Page',
		'HomePage'	=> 'HomePage'
	);

	private static $summary_fields = array(
		'Label'	=> 'Label',
	);

	public static $default_sort = 'SortOrder';

	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->removeByName(array('SortOrder', 'HomePageID'));

		$fields->addFieldsToTab('Root.Main', array(
			TextField::create('Title')->setTitle('Title'),
			HtmlEditorField::create('Content')->setTitle('Content'),
            CheckboxField::create('IssuuPopup', 'Issuu popup?'),
            TextareaField::create('IssuuEmbed', 'Issuu embed code'),
			TextField::create('Label')->setTitle('Label'),
			TextField::create('Telephone')->setTitle('Telephone'),
			UploadField::create('Image')->setTitle('Image'),
			TreeDropdownField::create('PageLinkID')->setTitle('Povezava na notranjo stran')->setSourceObject('SiteTree'),
			TextField::create('ExternalLink')->setTitle('External Link'),
			CheckboxField::create('OpenInNewWindow')->setTitle('Open in new window')
		));

		return $fields;
	}

}