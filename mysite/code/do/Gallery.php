<?php
class Gallery extends DataObject {

    private static $singular_name = 'Galerija';
    private static $plural_name = 'Galerija';

    private static $db = array(
        'Title'     	=> 'Varchar(255)',
    	'ShowClouds'	=> 'Boolean',
        'Description'   => 'HTMLText',
    );

    private static $many_many = array(
        'Images'	=> 'Image',
    	'Files'		=> 'File'
    );
    
    private static $many_many_extraFields = array(
    	'Images' => array(
    		'SortOrder' => 'Int'
    	),
    	'Files' => array(
    		'SortOrder' => 'Int'
    	),
    );
    
	/**
	 * Gets fields used in the cms
	 * @return {FieldSet} Fields to be used
	 */
	public function getCMSFields() {
	    $fields=parent::getCMSFields();
	    
	    $fields->addFieldsToTab('Root.Main', array(
	    	TextField::create('Title'),
	    	CheckboxField::create('ShowClouds')->setTitle('Prikaži oblake okoli galerije'),
	    	HtmlEditorField::create('Description'),
	    	SortableUploadField::create('Images'),
	    	SortableUploadField::create('Files')
	    ));
	    
	    return $fields;
	}

}