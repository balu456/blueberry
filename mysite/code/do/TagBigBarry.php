<?php

class TagBigBarry extends DataObject {
	
	private static $singular_name = 'Značka';
	private static $plural_name = 'Značke';	

	private static $db = array(
		'Title'	=> 'Varchar(50)',
		'Locale'	=> 'Varchar(255)',
	);
	
	private static $belongs_many_many = array(
		'News' => 'NewsBigBarry'
	);	
	
	private static $summary_fields = array(
		'Title',
		'Locale'
	);
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldsToTab('Root.Main', array(
			DropdownField::create('Locale')->setSource($this->setSourceLang())
		));
		
		return $fields;
	}
	
	public function setSourceLang() {
		$tmp = array();
		foreach(Translatable::get_allowed_locales() as $lang) {
			$tmp[$lang]	= $lang;
		}
		return $tmp;
	}
	
	public function Link() {
		$news = NewsBigBarryList::get()->filter('ClassName', 'NewsList')->first();
		if (!$news) return false;
	
		return $news->Link() . '?tagB[]=' . $this->ID;
	}	

}