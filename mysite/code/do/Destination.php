<?php

class Destination extends DataObject {
	
	private static $singular_name = 'Kategorija kampa';
	private static $plural_name = 'Kategorije kampa';	
	
	private static $db = array(
		'Title' => 'Varchar(50)'
	);
	
	private static $has_one = array(
	);
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
	
		return $fields;
	}

}