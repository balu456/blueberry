<?php

class Inquiry extends DataObject {
	
	private static $singular_name = 'Inquiry';
	private static $plural_name = 'Inquiries';	
	
	private static $db = array(
		'Arrival'	=> 'Date',
		'Departure'	=> 'Date',
		'Guests'	=> 'Int',
		'FirstName'	=> 'Varchar(40)',
		'Surname'	=> 'Varchar(40)',
		'Email'		=> 'Varchar(255)',
		'Phone'		=> 'Varchar(20)',
		'Message'	=> 'Text'
	);
	
	private static $has_one = array(
		'Camp'	=> 'Camp',
		'House'	=> 'CampHouse'
	);
	
	private static $summary_fields = array(
		'Created',
		'FirstName',
		'Surname',
		'Arrival', 
		'Departure',
		'Guests',
	);
	
	private static $field_labels = array(
		'Arrival'	=> 'Prihod',
		'Departure'	=> 'Odhod',
		'Guests'	=> 'Število gostov',
		'FirstName'	=> 'Ime',
		'Surname'	=> 'Priimek',
		'Email'		=> 'Elektronski naslov',
		'Phone'		=> 'Telefon',
		'Message'	=> 'Sporočilo',
		'Created'	=> 'Ustvarjeno',
	);
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		Translatable::disable_locale_filter();
		$fields = parent::getCMSFields();
		
		
		return $fields;
	}

}