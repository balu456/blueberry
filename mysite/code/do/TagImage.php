<?php
class TagImage extends DataObject {
	
	private static $singular_name = 'Image Tag';
	private static $plural_name = 'Image Tags';
	
	private static $db = array(
		'Title'		=> 'Varchar(255)',
		'Locale'	=> 'Varchar(255)',
		'SortOrder'	=> 'Int'
	);
	
	private static $belongs_many_many = array(
		//'Images'			=> 'Image',
		'TranslatedImages'	=> 'TranslatedImage',
	);
	
	private static $summary_fields = array(
		'Title',
		'Locale'
	);
	
	private static $searchable_fields = array(
		'Title',
		'Locale'
	);
	private static $field_labels = array(
		'Title' => 'Naziv',
		'Locale' => 'Jezik'
	);
	
	/**
	 * Gets fields used in the cms
	 * @return {FieldSet} Fields to be used
	 */
	public function getCMSFields() {
	    $fields=parent::getCMSFields();
	    
	    $fields->removeByName('SortOrder');
	    
	    $fields->addFieldsToTab('Root.Main', array(
	    	DropdownField::create('Locale')->setSource($this->setSourceLang())
	    ));
	    
	    return $fields;
	}
	
	public function setSourceLang() {
		$tmp = array();
		foreach(Translatable::get_allowed_locales() as $lang) {
			$tmp[$lang]	= $lang;
		}
		return $tmp;
	}
	
	function getCMSTitle() {
		return sprintf("%s (%s)", $this->Title, $this->Locale);
	}
	
}