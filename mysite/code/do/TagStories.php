<?php

class TagStories extends DataObject {
	
	private static $singular_name = 'Tag';
	private static $plural_name = 'Tags';	

	private static $db = array(
		'Title'	=> 'Varchar(255)',
        'URLSegment' => 'Varchar(255)',
		'Locale'	=> 'Varchar(255)',
	);
    
    private static $searchable_fields = array(
      'Title',
      'Locale' => array(
        'title' => 'Locale',
	    'field' => 'TextField',
	    'filter' => 'PartialMatchFilter'
      )
   );
	
    static $has_many = array(
        'SubTags' => 'SubTagStories',
    );
	
	private static $summary_fields = array(
		'Title',
		'Locale'
	);
    
    public function localeField() {
      return DropdownField::create('Locale')->setSource($this->setSourceLang());
    }
    
    function onBeforeWrite(){
        parent::onBeforeWrite();
        $this->URLSegment = singleton('SiteTree')->generateURLSegment($this->Title);
    }
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('URLSegment');
		$fields->addFieldsToTab('Root.Main', array(
			DropdownField::create('Locale')->setSource($this->setSourceLang())
		));
		
        $config = new GridFieldConfig_RecordEditor();
        $SubTagsGrid = new GridField('SubTags', 'Subtags', $this->SubTags(), $config);
        $fields->addFieldToTab('Root.SubTags', $SubTagsGrid);
        
		return $fields;
	}
	
	public function setSourceLang() {
		$tmp = array();
		foreach(Translatable::get_allowed_locales() as $lang) {
			$tmp[$lang]	= $lang;
		}
		return $tmp;
	}
	
	public function Link() {
		$holder = Controller::curr();
	
		return $holder->Link('tag/' . $this->URLSegment);
	}
    
    function TitleFirstWord(){
        $titleArr = explode(' ', $this->Title);
        return $titleArr[0];
    }	
    
    function isCurrent(){
        $tagSegment = Controller::curr()->request->param('OtherID');
        return $tagSegment == $this->URLSegment;
    }

}