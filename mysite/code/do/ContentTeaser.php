<?php

class ContentTeaser extends DataObject {
	
	private static $singular_name = 'Vsebinski teaser';
	private static $plural_name = 'Vsebinski teaserji';	
	
	private static $db = array(
		'Title' 		=> 'Varchar(255)',
		'Description'	=> 'Text',
		'CustomLink'	=> 'Varchar(255)',
	);
	
	private static $has_one = array(
		'Image'		=> 'Image',
		'PageLink'	=> 'Page',
	);
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldsToTab('Root.Main', array(
			TextField::create('Title')->setTitle('Naslov'),
			TextareaField::create('Description')->setTitle('Opis'),
			UploadField::create('Image')->setTitle('Slika'),
			TreeDropdownField::create('PageLinkID')->setTitle('Izberite povezavo na notranjo stran')->setSourceObject('SiteTree'),
			TextField::create('CustomLink')->setTitle('Custom link (ki ne obstaja v drevesni strukturi)')
		));
		
		return $fields;
	}

	/**
	 * Return link
	 * @return boolean
	 */
	function getLinkURL() {
		if($this->PageLinkID > 0) {
			return $this->PageLink()->Link();
		} else if($this->CustomLink) {
			return $this->CustomLink;
		}
		return false;
	}
}