<?php

/**
 * This decorator takes care of creating translated fields: Title-sl_SI,Title-en_US,Intro-sl_SI,Intro-en_US,...
 * 
 * To use it on a class you need to:
 * 1. define all available languages:
 * 		Translatable::set_allowed_locales(array('sl_SI', 'en_US', 'hr_HR', 'bs_BA', 'sr_RS', 'mk_MK', 'it_IT'));
 * 2. decorate that class:
 * 		Product::add_extension('LocaleDOFieldsDecorator');
 * 3. list the fields you wish to translate
 * 		class ShopProduct extends DataObject{
			public static $_locale_DB_fields = array(
				'Title' => 'Varchar(255)',
				'Variant' => 'Varchar(255)',
				'Desc' => 'HTMLText'
			);
			public static $_locale_has_one = array(
				'File' => 'File'
			);
			public static $_locale_DB_tabs = false;
			public static $_locale_DB_summary_fields = array(
				'Title:sl_SI','Desc'
			);
			private static $_locale_DB_searchable_fields = array(
				'Title:sl_SI','Desc:sl_SI','Content'
			);
		}
 * 4. run dev/buil
 * 
 * Optionally you can also set indexes:
 * 		public static $_locale_indexes = array(
  			"URLSegment" => array(
   				'type' => 'unique',
   				'value' => 'URLSegment'
  			)
 		);
 		
 * And also you can specify whether you want the getCMSFields to create separate Tabs for each Locale
 * where is puts all the translated fields for that locale:
 * 	 public static $_locale_DB_tabs = false;
 * 
 * ALSO: very IMPORTANT: this Decorator expects all fields to be scaffolded in the hierarchy: TabSet Root > Tab Main.
 * If $_locale_DB_tabs != false then it will create locale tabs: Root > sl_SI, Root > en_US, ... to put the translated fields to
 * the other fields will stay on the Main tab or wherever else they have been put.
 * 
 * @author ines.panker@innovatif.com
 *
 */
class LocaleDOFieldsDecorator extends DataExtension  {
	
	//IMPORTANT!!! DO NOT EVER AGAIN USE '__' AS THE SEPARATOR!!!!
	public static $name_separator = '-';
	
	private static $extraFieldsCache = array();
	private static $extraHasOneFields = array();
	private static $extraIndexes = array();
	private static $extraSummaryFields = array();
	private static $extrSearchableFields = array();
	public static $localesCache = null;
	public static $locale_names = array();
	
	/**
	 * Enable or disable field creation
	 * @var string
	 */
	public static $enable_field_creation = true;
	
	/**
	 * Returns a list of new DB fields (i.e. Title_sl_SI, Title_sn_US, ....)
	 */
	public static function get_extra_config($class, $extension, $args) {

		if(!isset(self::$extraFieldsCache[$class])){
		
			$fields = array();
			Config::inst()->get($class, '_locale_DB_fields', Config::EXCLUDE_EXTRA_SOURCES | Config::UNINHERITED, $fields);
			
			$locales = self::getLocales();
			
			$newFields = array();
			if($fields){
				foreach($fields as $fname => $ftype){
					foreach($locales as $locale){
						$newFields[$fname.self::$name_separator.$locale] = $ftype;
					}
				}
			}

			self::$extraFieldsCache[$class] = $newFields;
		}
		
		if(!isset(self::$extraHasOneFields[$class])){
		
			$fields = array();
			Config::inst()->get($class, '_locale_has_one', Config::EXCLUDE_EXTRA_SOURCES | Config::UNINHERITED, $fields);

			$newFields = array();
			if($fields){
				$locales = self::getLocales();
				foreach($fields as $fname => $ftype){
					foreach($locales as $locale){
						$newFields[$fname.self::$name_separator.$locale] = $ftype;
					}
				}
			}
		
			self::$extraHasOneFields[$class] = $newFields;
		}
		
		if(!isset(self::$extraIndexes[$class])){
			$fields = array();
			$newFields = array();
			Config::inst()->get($class, '_locale_indexes', Config::EXCLUDE_EXTRA_SOURCES | Config::UNINHERITED, $fields);
			
			if(count($fields) > 0){
				$locales = self::getLocales();
					
				foreach($fields as $fname => $fdata){
					foreach($locales as $locale){
						$data = $fdata;
						if(isset($data['value']))
							$data['value'] = $fdata['value'].self::$name_separator.$locale;
						$newFields[$fname.self::$name_separator.$locale] = $data;
					}
				}
			}
			
			self::$extraIndexes[$class] = $newFields;
		}
		
		if(!isset(self::$extraSummaryFields[$class])){
			$fields = array();
			$newFields = array();
			Config::inst()->get($class, '_locale_DB_summary_fields', Config::EXCLUDE_EXTRA_SOURCES | Config::UNINHERITED, $fields);
				
			if(count($fields) > 0){
				$locales = self::getLocales();
					
				foreach($fields as $value){
					$parts = explode(':', $value);
					if(count($parts) > 1)
						$newFields[] = $parts[0].self::$name_separator.$parts[1];
					else
						$newFields[] = $parts[0];
				}
			}
				
			self::$extraSummaryFields[$class] = $newFields;
		}

		if(!isset(self::$extrSearchableFields[$class])){
			$fields = array();
			$newFields = array();
			Config::inst()->get($class, '_locale_DB_searchable_fields', Config::EXCLUDE_EXTRA_SOURCES | Config::UNINHERITED, $fields);
		
			if(count($fields) > 0){
				$locales = self::getLocales();
					
				foreach($fields as $value){
					$parts = explode(':', $value);
					if(count($parts) > 1)
						$newFields[] = $parts[0].self::$name_separator.$parts[1];
					else
						$newFields[] = $parts[0];
				}
			}
		
			self::$extrSearchableFields[$class] = $newFields;
		}
		
		$return = array (
			'db' => self::$extraFieldsCache[$class],
			'has_one' => self::$extraHasOneFields[$class],
			'indexes' => self::$extraIndexes[$class],
			'summary_fields' => self::$extraSummaryFields[$class],
			'searchable_fields' => self::$extrSearchableFields[$class]
		);
		return $return;
	}
	
	public function _locale_DB_fields(){
		$classes = ClassInfo::ancestry($this->owner);
		$good = false;
		$items = array();
		
		foreach($classes as $class) {
			// Wait until after we reach DataObject
			if(!$good) {
				if($class == 'DataObject') {
					$good = true;
				}
				continue;
			}
		
			if(isset(self::$extraFieldsCache[$class])){
				$dbItems = self::$extraFieldsCache[$class];
				$items = array_merge((array) $items, $dbItems);
			}
		}
		return $items;
	}
	
	public function _locale_has_one(){
		$classes = ClassInfo::ancestry($this->owner);
		$good = false;
		$items = array();
		
		foreach($classes as $class) {
			// Wait until after we reach DataObject
			if(!$good) {
				if($class == 'DataObject') {
					$good = true;
				}
				continue;
			}
		
			if(isset(self::$extraHasOneFields[$class])){
				$dbItems = self::$extraHasOneFields[$class];
				$items = array_merge((array) $items, $dbItems);
			}
		}
		return $items;
	}
	
	public static function getLocales(){
		if(!self::$localesCache){
			$list = Translatable::get_allowed_locales();
			if(!$list || count($list) < 1){
				$list = array(Translatable::default_locale());
			}
			self::$localesCache = $list;
		}
		
		return self::$localesCache;
	}
	
	public function updateFieldLabels(&$labels) {
		parent::updateFieldLabels($labels);
	
		$statics = self::$extraFieldsCache[$this->ownerBaseClass];
		foreach($statics as $field => $type){
			$parts = explode(self::$name_separator, $field);
			//nice name for first part of the field
			$niceName = isset($labels[$parts[0]]) ? $labels[$parts[0]] : $parts[0];
			$labels[$field] = $niceName . ' (' . $parts[1] . ')';
		}
	}
	
	public function updateCMSFields(FieldList $fields) {
		parent::updateCMSFields($fields);
	
		if(!self::$enable_field_creation)
			return;
		
		//TODO: this is wrong, there should be a for(this owner class and all parent classes up to DataObject){}
// 		if(!isset(self::$extraFieldsCache[$this->owner->class])){
// 			return;
// 		}

		$useLocaleTabs = Config::inst()->get($this->owner->ClassName, '_locale_DB_tabs') !== false;
		
		//create separate tabs
		$locales = self::getLocales();
		
		if($useLocaleTabs){
			//for estetic reasons it is better if the tabs follow each other in this way: Main, locales, all relations
			$insertAfterTab = 'Main';
			foreach($locales as $l){
				$locName = isset(self::$locale_names[$l]) ? self::$locale_names[$l] : $l;  
				$fields->insertAfter(new Tab($l, $locName), $insertAfterTab);
				$insertAfterTab = $l;
			}
		}
	
		// put fields to tabs:
		$allFieldsSorted = $this->owner->_locale_DB_fields();
		$hasOneFields = $this->owner->_locale_has_one();
		$allTitles = $this->owner->fieldLabels();		
		
		foreach($allFieldsSorted as $translatableField => $type){			
			$this->replaceField($fields, $allTitles, $useLocaleTabs, $translatableField, false);
		}

		if(count($hasOneFields) > 0){
			foreach($hasOneFields as $translatableField => $type){
				$this->replaceField($fields, $allTitles, $useLocaleTabs, $translatableField, true, $type);
			}
		}
		
		
		//is the main tab now empty?
		$mainTab = $fields->fieldByName('Root.Main');
		$num = $mainTab->getChildren()->count();
		if($num < 1){
			$fields->removeByName('Main');
		}
		
		return;
	}
	
	protected function replaceField(&$fields, &$allTitles, $useLocaleTabs, $translatableField, $relationship=false, $relationshipClass = ''){
		list($basename, $l) = explode(self::$name_separator, $translatableField);
		$locName = isset(self::$locale_names[$l]) ? self::$locale_names[$l] : $l;
		
		$fieldName = $relationship ? "{$translatableField}ID" : $translatableField;
		
		$field = null;
		//if the relationship is a SiteTree, then create a dropdown with different locales for every lang
		if($relationship && is_a(singleton($relationshipClass), 'SiteTree')){
			Translatable::disable_locale_filter();
			$field = new DropdownField($fieldName, $this->owner->fieldLabels($translatableField));
			$field->setEmptyString(' ');
			$field->setSource(SiteTree::get()->filter('Locale', $l)->map('ID','Title'));
			Translatable::enable_locale_filter();
		}
		else if($relationship && is_a(singleton($relationshipClass), 'File')){
			//upload fields have the field id withouth the "-ID" suffix
			$field = $fields->fieldByName('Root.Main.'.$translatableField);
		}
		else{
			$field = $fields->fieldByName('Root.Main.'.$fieldName);
		}
		
		if($field){
			//remove all localized fields from the list (generated through scaffolding)
			$fields->removeByName($translatableField);
		}
		else{
		
			if(!$relationship)
				$field = $this->owner->dbObject($fieldName)->scaffoldFormField(null);
			else
				$field = $this->owner->relObject($fieldName)->scaffoldFormField(null);
				
		}
		//set title:
		if(isset($allTitles[$basename]))
			$field->setTitle($allTitles[$basename].' ('.$locName.')');
			
		$tabName = $useLocaleTabs ? $l : 'Main';
		$fields->addFieldToTab('Root.'.$tabName, $field);
	}
	
	protected function getBasename($field) {
		$retVal = explode(self::$name_separator, $field);
		return reset($retVal);
	}
	/** Use to call localised db fields.
	 * 
	 * @param unknown $name
	 * @param string $locale
	 */
	public function LocaleField($name, $locale = null){
		$locale = $locale ? $locale : Translatable::get_current_locale();
		$fieldName = $name . self::$name_separator . $locale;
		return $this->owner->$fieldName;
	}
	
	/** Use to call localised has_one fields.
	 * @param unknown $name i.e. Page
	 * @param string $locale i.e. sl_SI
	 * @param string $returnObj true/false => if true, then we return the object, else we return the ID of the object
	 */
	public function LocaleRel($name, $returnObj = true, $locale = null){
		$locale = $locale ? $locale : Translatable::get_current_locale();
		$fieldName = $name . self::$name_separator . $locale;
		if($returnObj)
			return $this->owner->{$fieldName}();
		else
			return $this->owner->{$fieldName.'ID'};
	}
	
	public function SetLocaleField($name, $value, $locale = null){
		$locale = $locale ? $locale : Translatable::get_current_locale();
		$fieldName = $name . self::$name_separator . $locale;
		$this->owner->$fieldName = $value;
	}
	
	public function LocaleTitle(){
		$loc = i18n::get_locale();
		$fieldName = 'Title'.self::$name_separator.$loc;
		return $this->owner->$fieldName;
	}
	public function LocaleTitleFieldName(){
		$loc = i18n::get_locale();
		return 'Title'.self::$name_separator.$loc;
	}
	
	public function GetLocaleFieldNameFor($basename, $locale = null){
		$locale = $locale ? $locale : Translatable::get_current_locale();
		$fieldName = $basename . self::$name_separator . $locale;
		return $fieldName;
	}
	
	public function GetAllLocaleFieldNamesFor($basename){
		$locales = self::$localesCache;
		$return = array();
		foreach ($locales as $l){
			$return[$l] = $basename . self::$name_separator . $l;
		}
		return $return;
	}
}