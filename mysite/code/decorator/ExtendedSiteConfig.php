<?php

/**
 * Extra SiteConfig options
 */
class ExtendedSiteConfig extends DataExtension {     

	private static $db = array(
		'MainMail'				=> 'Varchar(255)',
		'MainPhone'				=> 'Varchar(255)',
		'PDFTitle'				=> 'Varchar(255)',
		'GoogleAnalyticsCode'	=> 'Text',
			
		'Twitter'				=> 'Varchar(255)',
		'Facebook'				=> 'Varchar(255)',
		'Pinterest'				=> 'Varchar(255)',
		'Youtube'				=> 'Varchar(255)',
		'GooglePlus'			=> 'Varchar(255)',
		'Instagram'				=> 'Varchar(255)',
		//'Flickr'				=> 'Varchar(255)',
		'Medium'				=> 'Varchar(255)',
		'TripAdviser'			=> 'Varchar(255)',
		'Issuu'					=> 'Varchar(255)',
		'Tumblr'			    => 'Varchar(255)',
		'LinkedIn'			    => 'Varchar(255)',
		'Vkontakte'			    => 'Varchar(255)',
		'Airbnb'			    => 'Varchar(255)',
		'Booking'				=> 'Varchar(255)',	
	);
 
	public function updateCMSFields(FieldList $fields) {
		
		$fields->addFieldsToTab('Root.Social',
			array(
				TextField::create('Twitter'),
				TextField::create('Facebook'),
				TextField::create('Pinterest'),
				TextField::create('Youtube'),
				TextField::create('GooglePlus'),
				TextField::create('Instagram'),				
				TextField::create('TripAdviser'),
				TextField::create('Medium'),
				TextField::create('Issuu'),
				TextField::create('Tumblr'),
				TextField::create('LinkedIn'),
				TextField::create('Vkontakte'),
				TextField::create('Airbnb'),
				TextField::create('Booking'),
			)
		);

		$fields->addFieldsToTab('Root.Main', 
			array(
				TextareaField::create('GoogleAnalyticsCode')->setTitle('Koda za Google Analytics'),
				TextField::create('MainMail')->setTitle('Privzeti elektronski naslov'),
				TextField::create('MainPhone')->setTitle('Privzeta telefonska številka'),
				TextField::create('PDFTitle')->setTitle('Privzeta naslov v PDF dokumentu'),
			)
		);
	}
	
}
