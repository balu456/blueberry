<?php
class ImageDecorator extends DataExtension {
	
	private static $db = array(
		'Youtube'	=> 'HTMLText',
	);
	
	private static $many_many = array(
		'Tags'		=> 'TagImage'
	);
	
	/**
	 * update cmsfields
	 */
	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldsToTab('Root.Main', array(
			TextField::create('Youtube')->setTitle('Youtube ID'),
			CheckboxSetField::create('Tags', 'Tags', TagImage::get()->filter('Locale', Translatable::get_current_locale())->map('ID', 'getCMSTitle'))
		));
	}
	
	/**
	 * Gets the relative URL accessible through the web.
	 *
	 * @uses Director::baseURL()
	 * @return string
	 */
	public function getYoutubeURL() {
		return '//www.youtube.com/embed/' . $this->owner->Youtube . '?autoplay=1&amp;controls=0&amp;showinfo=0';
	}
	
	/*
	 * Nebi blo slabo preverit, če je user res vnesel samo ID
	 * in odstranit nepotrebn del
	 */
	public function onBeforeWrite() {
		$this->owner->Youtube = self::getYoutubeID($this->owner->Youtube);
	}
	
	public static function getYoutubeID($url)
	{
		if(stristr($url,'youtu.be/'))
		{
			preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID); 
			return $final_ID[4]; 
		}
		elseif(stristr($url,'youtube'))
		{
			@preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD); 
			return $IDD[5]; 
		}
		else 
		{
			return $url;
		}
	}
	
	
}