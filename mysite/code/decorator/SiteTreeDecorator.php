<?php

class SiteTreeDecorator extends DataExtension {

	private static $db = array(
		'MenuFooterLeft' 	=> 'Boolean',
		'MenuFooterRight'	=> 'Boolean',
	);

	public function updateSettingsFields(FieldList $fields) {
		
		$fields->addFieldsToTab('Root.Settings', array(
			CheckboxField::create('MenuFooterLeft', 'Prikaži v nogi levo'),
			CheckboxField::create('MenuFooterRight', 'Prikaži v nogi desno'),
		), 'ShowInSearch');
	 
	 }
}
