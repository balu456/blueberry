<?php
/**
 * Extension that provides additional permissions for user groups (like Content Authors) and enables them to edit/delete/view/create DataObjects.
 * 
 * How to use it?
 * Add this to your config.yml:
 * 
 * MyDataObjectClass:
 *   extensions:
 *     - DataObjectPermissionsDecorator
 * 
 * 
 * @author klemend
 */
class DataObjectPermissionsDecorator extends DataExtension implements PermissionProvider {

	public function providePermissions() {
		$perms = array(
			"DataObject_CREATE"	=>	array(
				'name'		=> _t('DataObjectWithPermissions.EnableCreate', 'Dovoli ustvarjanje objektov'),
				'category'	=> _t('DataObjectWithPermissions.DataObjectCategory', 'Objekti'),
				'help'		=> _t('DataObjectWithPermissions.EnableCreateHelp', 'Omogoča ustvarjanje objektov.'),
				'sort'		=> -100
			),
			"DataObject_VIEW"	=>	array(
				'name'		=> _t('DataObjectWithPermissions.EnableView', 'Dovoli pregled objektov'),
				'category'	=> _t('DataObjectWithPermissions.DataObjectCategory', 'Objekti'),
				'help'		=> _t('DataObjectWithPermissions.EnableViewHelp', 'Omogoča pregled objektov.'),
				'sort'		=> -100
			),
			"DataObject_EDIT"	=>	array(
				'name'		=> _t('DataObjectWithPermissions.EnableEdit', 'Dovoli urejanje objektov'),
				'category'	=> _t('DataObjectWithPermissions.DataObjectCategory', 'Objekti'),
				'help'		=> _t('DataObjectWithPermissions.EnableEditHelp', 'Omogoča urejanje objektov.'),
				'sort'		=> -100
			),
			"DataObject_DELETE"	=>	array(
				'name'		=> _t('DataObjectWithPermissions.EnableDelete', 'Dovoli brisanje objektov'),
				'category'	=> _t('DataObjectWithPermissions.DataObjectCategory', 'Objekti'),
				'help'		=> _t('DataObjectWithPermissions.EnableDeleteHelp', 'Omogoča brisanje objektov.'),
				'sort'		=> -100
			),
		);
		
		return $perms;
	}
	
	public function canCreate($member = null) {
		// !$member - site view
		// Permission::check("DataObject_CREATE") - added permissions
		// else check default SS permissions
		return !$member || Permission::check("DataObject_CREATE") || (is_callable('parent::canCreate') ? parent::canCreate($member) : false);
	}
	
	public function canView($member = null) {
		return !$member || Permission::check("DataObject_VIEW") || (is_callable('parent::canView') ? parent::canView($member) : false);
	}
	
	public function canEdit($member = null) {
		return !$member || Permission::check("DataObject_EDIT") || (is_callable('parent::canEdit') ? parent::canEdit($member) : false);
	}
	
	public function canDelete($member = null) {
		return !$member || Permission::check("DataObject_DELETE") || (is_callable('parent::canDelete') ? parent::canDelete($member) : false);
	}
}
