<?php

class SideContentAdmin extends ModelAdmin {
	
	private static $menu_title = 'Šifrant';
	
	private static $url_segment = 'sifrant';
	
	public $showImportForm = false;
	
	private static $managed_models = array('Destination', 'Inquiry', 'CampIcon', 'HouseBanner', 'TagImage', 'ImageGallery');
	
	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id, $fields);
	
		// enable sorting
		$sortables = array('TagImage');
	
		if (in_array($this->modelClass, $sortables) && $gridField = $form->Fields()->dataFieldByName($this->sanitiseClassName($this->modelClass))) {
			if ($gridField instanceof GridField) $gridField->getConfig()->addComponent(new GridFieldSortableRows('SortOrder'));
		}
	
		return $form;
	}

	public function getExportFields() {
		if($this->modelClass == 'Inquiry')
			return array(
				'Arrival'	=> 'Prihod',
				'Departure'	=> 'Odhod',
				'Guests'	=> 'Število gostov',
				'FirstName'	=> 'Ime',
				'Surname'	=> 'Priimek',
				'Email'		=> 'Elektronski naslov',
				'Phone'		=> 'Telefonska številka',
				'Message'	=> 'Sporočilo',
			);
		return parent::getExportFields();
	}
	
}