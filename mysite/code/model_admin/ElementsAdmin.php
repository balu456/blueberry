<?php

class ElementsAdmin extends DataObjectAsPageAdmin {
	
	private static $menu_title = 'Elementi';
	
	private static $url_segment = 'elements';
	
	public $showImportForm = false;
	
	private static $managed_models = array('News', 'SocialPost', 'NewsBigBarry', 'Designer', 'Stories','Tag', 'TagStories');
	
	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id, $fields);
	
		// enable sorting
		$sortables = array('Designer');
	
		if (in_array($this->modelClass, $sortables) && $gridField = $form->Fields()->dataFieldByName($this->sanitiseClassName($this->modelClass))) {
			if ($gridField instanceof GridField) $gridField->getConfig()->addComponent(new GridFieldSortableRows('SortOrder'));
		}
	
		return $form;
	}
}