<?php

class Images2Gallery extends BuildTask {
	

	function init() {
		parent::init();
	
		$canAccess = (Director::isDev() || Director::is_cli() || Permission::check('ADMIN'));
		if (!$canAccess) return Security::permissionFailure($this);
	}
	
	private $old = array();
	
	function run($request) {
		
		// old task
// 		$this->task1();

		// 2016
		$this->updateGallery();
	}
	
	private function updateGallery() {
		$list_galleries = ImageGallery::get();
		
		$list_trans = Translatable::get_allowed_locales();
		
		foreach ($list_galleries as $gal) {
			$title = $gal->dbObject('Title')->getValue();
			if(!$title) {
				$title = $gal->dbObject('EditorialTitle')->getValue();
			}
			
			foreach($list_trans as $locale) {
				$gal->SetLocaleField('Title', $title, $locale);
			}
			
			$gal->write();
		}
	}
	
	/** ******* **/
	private function task1() {
		$enabled = Translatable::disable_locale_filter();
		$this->purge();
		
		$list_sites = SiteTree::get();
		
		foreach($list_sites as $site) {
				
			echo "<p>".$site->ID." ";
				
				
			$this->handle_Images($site);
			$this->handle_Galleries($site);
				
			echo "</p>";
		}
		
		Translatable::enable_locale_filter($enabled);
	}
	
	private function purge() {
		TranslatedImage::get()->removeAll();
		ImageGallery::get()->removeAll();
	}
	
	private function handle_Images($site) {
		if(!method_exists($site, 'Images'))
			return false;
		$list_images = $site->Images()->sort('SortOrder');
		if($list_images && $list_images->count()) {
			$gallery_title = sprintf("%2d %s", $site->ID, $site->Title);
		
			$current_gallery = $this->_findOrCreateGallery($gallery_title);
	
			$site->ImageGalleryID = $current_gallery->ID;
			$site->write();
			$site->doPublish();
	
			echo " added new images gallery '$gallery_title' ";
	
			$this->_appendImagesToGallery($current_gallery, $list_images);
		} else {
			echo " skipped images ";
		}
	}
	
	private function handle_Galleries($site) {
		if(!($site instanceof HousePage))
			return false;
		
		$list_galleries = $site->Galleries()->sort('SortOrder');
		
		if($list_galleries && $list_galleries->count()) {
			
			foreach($list_galleries as $gallery) {
				$gallery_title = sprintf("gal %2d %s", $site->ID, $gallery->Title);
				
				$current_gallery = $this->_findOrCreateGallery($gallery_title, $gallery->Title);
				$current_gallery->ShowClouds = $gallery->ShowClouds;
				$current_gallery->Description = $gallery->Description;
				$current_gallery->write();
				
				$site->ImageGalleries()->add($current_gallery);
				echo " added new gallery '$gallery_title' ";
				
				$this->_appendImagesToGallery($current_gallery, $gallery->Images()->sort('SortOrder'));
			}
		} else {
			echo " skipped gallery ";
		}
	}
	
	private function _findOrCreateGallery($gallery_title, $title = false) {
		$current_gallery = ImageGallery::get()->filter(array('EditorialTitle' => $gallery_title))->first();
		if(!$current_gallery)
			$current_gallery = new ImageGallery();
		
		if(!$current_gallery->Title) $current_gallery->Title = $title ? $title : $gallery_title;
		$current_gallery->EditorialTitle = $gallery_title;
		$current_gallery->write();
		
		return $current_gallery;
	}
	
	private function _appendImagesToGallery($current_gallery, $list_images) {
		$sortOrder = 0;
		foreach($list_images as $original_image) {
		
			$new_img_object = $current_gallery->Images()->filter(array('ImageID' => $original_image->ID))->first();
			if(!$new_img_object) {
				$new_img_object = new TranslatedImage();
				$new_img_object->ImageID = $original_image->ID;
			}
			$new_img_object->Youtube = $original_image->Youtube;
			$new_img_object->{'Title-sl_SI'} = $original_image->Title;
			$new_img_object->write();
		
			$list_tags = $original_image->Tags();
			if($list_tags && $list_tags->count()) {
				foreach($list_tags as $tag) {
					$new_img_object->Tags()->add($tag->ID);
				}
			}
		
			$current_gallery->Images()->add($new_img_object, array('SortOrder' => $sortOrder++));
		}
	}
}