<?php

class SplitPage extends Page {
	
	/**
	 * Disable creation of basic split page
	 * {@inheritDoc}
	 * @see SiteTree::canCreate()
	 */
	function canCreate($member = null) {
		return $this->ClassName != 'SplitPage' && parent::canCreate($member);
	}
	
}

class SplitPage_Controller extends Page_Controller {
	
	function getSplitPageMenu() {
		$parent = $this->getSplitParent($this);
		
		return $this->getSplitChildren($parent);
	}
	
	private function getSplitChildren(SiteTree $start) {
		$list_childrens = $start->Children();
		$list_out = ArrayList::create();
		
		foreach($list_childrens as $children) {
			
			//if(!($children instanceof SplitPage) || !$children->isPublished()) continue;
			if(!$children->isPublished()) continue;
			
			$list_out->add($children);
			$list_out->merge( $this->getSplitChildren($children) );
		}
		return $list_out;
	}
	
	private function getSplitParent(SiteTree $start) {
		$parent = $start->Parent();
		
		if($parent instanceof SplitPage)
			return $this->getSplitParent($parent);
		else
			return $start;
	}
	
}