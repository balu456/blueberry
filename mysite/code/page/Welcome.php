<?php

class Welcome extends Page {

	private static $icon = 'mysite/images/cms_icons/welcome.png';	
	private static $description = 'Dobrodošli';
	private static $singular_name = 'Dobrodošli';

	private static $db = array(
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		return $fields;
	}	

}

class Welcome_Controller extends Page_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}

}
