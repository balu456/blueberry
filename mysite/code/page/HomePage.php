<?php

class HomePage extends Page {

	private static $icon = 'mysite/images/cms_icons/home_page.gif';	
	private static $description = 'Home page';

	private static $db = array(
		'HouseDescription'	=> 'HTMLText',
	);
	
	/*
	private static $has_one = array(
		'ExposedCamp' => 'Camp'
	);
	*/
	
	private static $has_one = array(
		'ExposedImage' => 'Image',
	);

	private static $has_many = array(
		'HomePageElements' => 'HomePageElement'
	);
	
	private static $many_many = array(
		'News' 			=> 'News',
		'NewsFromWorld'	=> 'NewsFromWorld',
		'NewsBigBarry'	=> 'NewsBigBarry',
		'Stories'		=> 'Stories',
	);
	
	private static $many_many_extraFields = array(
		'News' => array(
			'SortOrder' => 'Int'
		),
		'NewsFromWorld' => array(
			'SortOrder' => 'Int'
		),
		'NewsBigBarry' => array(
			'SortOrder' => 'Int'
		),
		'Stories' => array(
			'SortOrder' => 'Int'
		),
	);

	/**
	 * Gets fields used in the cms
	 * @return {FieldSet} Fields to be used
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		 
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		$newsConf = new GridFieldConfig_RelationEditor();
		$newsConf->addComponent(new GridFieldSortableRows('SortOrder'));
		
		// hide "Add new" button
		$newsConf->removeComponentsByType('GridFieldAddNewButton');
	
		$fields->addFieldToTab('Root.Članki', new GridField('News', 'News', $this->SortedNews(), $newsConf));
		$fields->addFieldToTab('Root.ČlankiIzSveta', new GridField('NewsFromWorld', 'NewsFromWorld', $this->SortedNews('NewsFromWorld'), $newsConf));
		$fields->addFieldToTab('Root.ČlankiBigBarry', new GridField('NewsBigBarry', 'NewsBigBarry', $this->SortedNews('NewsBigBarry'), $newsConf));
		$fields->addFieldToTab('Root.Stories', new GridField('Stories', 'Stories', $this->SortedNews('Stories'), $newsConf));


		$elemConf = new GridFieldConfig_RelationEditor();
		$elemConf->addComponent(new GridFieldSortableRows('SortOrder'));

		$fields->addFieldToTab('Root.Elementi', new GridField('HomePageElements', 'HomePageElements', $this->HomePageElements(), $elemConf));


		$fields->addFieldsToTab('Root.Main',
			array(
				HtmlEditorField::create('HouseDescription')->setTitle('Opis hiš'),
				UploadField::create('ExposedImage')->setTitle('Izpostavljena slika')
				//DropdownField::create('ExposedCampID', 'ExposedCamp', Camp::get()->map())->setEmptyString('-')
			)
		);		
		
		return $fields;
	}
	
	public function SortedNews($class = 'News') {
		$limit = 4;
		$now = date('Y-m-d H:i:s');
		
		$news = NewsList::NewsWhere($this->$class())->sort('SortOrder');//->limit($limit); // limit tukaj predstavlja MySQL težavo, ker ne pozna LIMIT v subquery-ju
		
		return $news;
	}
	
	public function SortedNewsForPage($class = 'News') { 
		$limit = 4;
		$now = date('Y-m-d H:i:s');
		
		$news = NewsList::NewsWhere($this->$class())->sort('SortOrder')->limit($limit)->toArray();
		$return = new ArrayList($news);
		
		if($return->Count() < $limit) {
			$newLimit = $limit - $return->Count();
				
			$excludeIDs = array();
			foreach($return as $row) {
				$excludeIDs[] = $row->ID;
			}
			
			$addNews = $class::get()->limit($newLimit)->sort('Created DESC')->filter(array(
				'Locale'	=> Translatable::get_current_locale()
			));
			
			if(count($excludeIDs))
				$addNews = $addNews->filter(array('ID:not'	=> $excludeIDs));
			
			if($addNews->count()) {
				$return->merge($addNews->toArray());
			}
		}
		
		return $return;
	}
	
	public function HousePages() {
		return HousePage::get()->filter(array('ShowInMenus' => 1))->sort('Sort');
	}
}

class HomePage_Controller extends Page_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}
	
	function HomePageCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			//$this->ExposedCampID,
			$this->ExposedImageID,
			HousePage::get()->max('LastEdited'),
			Page::ManyManyTableCacheKey('HomePage_News'),
			News::get()->max('LastEdited')
		);
	
		return implode('_', $params);
	}
}
