<?php

class HouseVariationPage extends Page {

	private static $icon = 'mysite/images/cms_icons/house.png';
	private static $description = 'Stran z različicami hiše';
	private static $singular_name = 'Stran z različicami hiše';
	
	private static $db = array(
		'Persons'		=> 'Int',
		'Dimensions'	=> 'Int',
		'Rooms'			=> 'Varchar(255)',
		'InMaterials'	=> 'Varchar(255)',
		'OutMaterials'	=> 'Varchar(255)',
	);
	
	private static $has_one = array(
		'Tloris' 			=> 'Image',
		//'DescriptionFile'	=> 'File'
	);
	
	private static $many_many = array(
		'HouseDetails' 		=> 'HouseDetail',
	);
	
	private static $many_many_extraFields = array(
		'HouseDetails' => array(
			'SortOrder' => 'Int'
		),
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
	    $fields = parent::getCMSFields();
	    
	    $fields->removeByName(array(
    		'YoutubeLink',
    		'YoutubeTitle',
	    	'Images'
	    ));
	    
	    $persons = array(
    		'2' => '2',
    		'4' => '4',
    		'6' => '6',
			'10' => '10'
	    );
	    
	    $detailsConf = new GridFieldConfig_RelationEditor();
	    $detailsConf->addComponent(new GridFieldSortableRows('SortOrder'));
	    $detailsConf->getComponentByType('GridFieldAddExistingAutocompleter')->setResultsFormat('$Title $Label');
	    
	    $fields->addFieldToTab('Root.HouseDetails', new GridField('HouseDetails', 'HouseDetails', $this->SortedHouseDetails(), $detailsConf));
	    
	    $fields->addFieldsToTab('Root.Main', array(
	    	UploadField::create('Tloris'),
	    	DropdownField::create('Persons')->setSource($persons)->setEmptyString('-')->setTitle('Oseb'),
	    	NumericField::create('Dimensions')->setTitle('Dimenzije'),
    		TextareaField::create('Rooms')->setTitle('Sobe'),
    		TextareaField::create('InMaterials')->setTitle('Materiali notri'),
    		TextareaField::create('OutMaterials')->setTitle('Materiali zunaj'),
	    	//UploadField::create('DescriptionFile')->setTitle('Datoteka s podrobnim opisom'),
	    ), 'Metadata');
	    
	    return $fields;
	}
	
	public function SortedHouseDetails() {
		return $this->HouseDetails()->Sort('SortOrder');
	}
	
}

class HouseVariationPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}

	function HouseVariationPageCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			$this->TlorisID,
			$this->Parent()->BoxImageID,
			$this->Parent()->BrochureFileID,
			Page::ManyManyTableCacheKey('HouseVariationPage_HouseDetails'),
			Page::ManyManyTableCacheKey('HousePage_Designers'),
			Page::ManyManyTableCacheKey('HousePage_HouseBanners'),
			//Page::ManyManyTableCacheKey('HousePage_DimensionImages'),
			Page::ManyManyTableCacheKey('HousePage_Galleries'),
			Page::ManyManyTableCacheKey('Page_Images'),
			Page::ManyManyTableCacheKey('Page_Files')
		);
	
		return implode('_', $params);
	}
}
