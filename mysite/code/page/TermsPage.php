<?php

class TermsPage extends Page {

	private static $icon = 'mysite/images/cms_icons/terms.png';	
	private static $description = 'Pravno obvestilo';
	private static $singular_name = 'Pravno obvestilo';

	private static $db = array(
		'LeftColumnText' => 'HTMLText'
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		$fields->addFieldsToTab('Root.Main', array(
			HtmlEditorField::create('LeftColumnText')->setTitle('Vsebina v levem stolpcu')
		), 'Content');
		
		return $fields;
	}	

}

class TermsPage_Controller extends Page_Controller {
	
}
