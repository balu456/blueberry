<?php

class TerracePage extends DataObjectAsPageHolder {

	private static $icon = 'mysite/images/cms_icons/terrace.gif';	
	private static $description = 'Terase';
	private static $singular_name = 'Terase';

	private static $db = array(
	);
	
	private static $has_one = array(
		'BoxImage' => 'Image',
	);
	
	private static $has_many = array(
		'TerraceAdditionals' => 'TerraceAdditional'
	);
	
	private static $many_many = array(
		'SmallImages'	=> 'Image',
	);
	
	private static $many_many_extraFields = array(
		'SmallImages'	=> array(
			'SortOrder'	=> 'Int',
		),
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldsToTab('Root.Main', array(
			UploadField::create('BoxImage')->setTitle('Slika nad naslovom'),
		));
		
		
		$terraceConf = new GridFieldConfig_RelationEditor();
		$terraceConf->addComponent(new GridFieldSortableRows('SortOrder'));
		
		$fields->addFieldToTab('Root.TerraceAdditionals', new GridField('TerraceAdditionals', 'TerraceAdditionals', $this->TerraceAdditionals(), $terraceConf));
		
		$fields->addFieldsToTab('Root.SmallImages', array(
			new SortableUploadField('SmallImages'),
		));
		
		return $fields;
	}	

}

class TerracePage_Controller extends DataObjectAsPageHolder_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}
	
	/**
	 * Function to call in DOAPHolder for template rendering.
	 * @return string
	 */
	function getItemClass() {
		return 'TerraceAdditional';
	}
	
	function TerracePageCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			$this->BoxImageID,
			Page::ManyManyTableCacheKey('TerraceAdditional_Images'),
			Page::ManyManyTableCacheKey('TerracePage_SmallImages'),
			TerraceAdditional::get()->max('LastEdited'),
		);
	
		return implode('_', $params);
	}

}
