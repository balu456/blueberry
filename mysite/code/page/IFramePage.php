<?php
class IFramePage extends Page{
    
    static $singular_name = 'IFrame Page';
    
    static $db = array(
        'EmbedCode' => 'HTMLText'
    );
    
    function getCMSFields(){
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Embed', TextareaField::create('EmbedCode', 'Embed code'));
        return $fields;
    }
}
class IFramePage_Controller extends Page_Controller{
    
}