<?php

class ContactPage extends Page {

	private static $icon = 'mysite/images/cms_icons/phone.png';	
	private static $description = 'Kontakt';
	private static $singular_name = 'Kontakt';

	private static $db = array(
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		return $fields;
	}	

}

class ContactPage_Controller extends Page_Controller {
	
}
