<?php

class NewsListPage extends SplitPage {
	
	private static $icon = 'mysite/images/cms_icons/news_list_page.png';
	
	//private static $description = 'Seznam novic';
	//private static $singular_name = 'Seznam novic';
	
	private static $db = array(
		'itemsPerPage'	=> 'Int',
		'AutomaticPDF'	=> 'Boolean'
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(
			array(
				'YoutubeLink',
				'YoutubeTitle'
			)
		);
		 
		$fields->addFieldToTab('Root.Main', NumericField::create('itemsPerPage')->setTitle("Število novic na strani"));
		$fields->addFieldToTab('Root.Main', CheckboxField::create('AutomaticPDF')->setTitle('Avtomatska izdelava PDF dokumentov'), 'Content');
		
		return $fields;
	}	
	
	public static function NewsWhere($list) {
		$now = date('Y-m-d H:i:s');
	
		$list = $list->where('
						"Start" IS NULL
		     				OR
		     			"Start" IS NOT NULL AND "Start" <= \'' . $now . '\'
				     		AND
				     	"End" IS NULL
				    		OR
				     	"End" IS NOT NULL AND "End" >= \'' . $now . '\'
					');
	
		return $list;
	}	
	
}

class NewsListPage_Controller extends SplitPage_Controller {

	private static $url_handlers = array(
		'$ID/$OtherID' => 'handleAction'
	);

	private static $allowed_actions = array(
		'more'
	);
	
	protected $limit;
	protected $offset;
	protected $className = 'SocialPost';
	
	function init() {
		parent::init();
	
		// Load number of items per page
		if (!$this->itemsPerPage)
			$this->limit = 8;
		else
			$this->limit = $this->itemsPerPage;
		
		// Default offset
		$this->offset = 0;
		
		$this->loadGETFilters();
	}

	/**
	 * Override handle action
	 */
	protected function handleAction($request, $action) {
		$params = $request->allParams();
	
		$id = $params['ID'];
		if (!isset($id) && $action != 'index') $id = $action;
		if (!isset($id)) return $this->index();
	
		if ($this->hasMethod($id)) return parent::handleAction($request, $id);
	
		$item = $this->getCurrentItem();
		if ($item) return $this->showDetail($item);
	
		return $this->httpError(404);
	}
	
	public function index() {		
		return $this;
	}
	
	
	/**
	 * Get current item based on url segment
	 *
	 * @return DataObjectAsPage|boolean
	 */
	public function getCurrentItem() {
		$params = $this->getRequest()->allParams();
	
		$URLSegment = $params['ID'];
		if (!isset($URLSegment)) return false;
		
		$class = $this->className; 
	
		return $class::get()->filter('URLSegment', Convert::raw2sql($URLSegment))->first();
	}
	
	
	/**
	 * Render item detail
	 *
	 * @param DataObjectAsPage $item
	 */
	public function showDetail($item) {
		if (!$item->canView()) return Security::permissionFailure($this);
	
		$data = array(
				'Item'			=> $item,
				'Breadcrumbs'	=> $item->Breadcrumbs(false, false, 'RedirectorPage', true),
				'MetaTags'		=> $item->MetaTags(false),
				'Title'			=> $item->Title,
				'Title2'		=> $this->Title
	
		);
	
		if ($this->hasMethod('updateShowDetailData')) $data = $this->updateShowDetailData($data);
	
		if ($this->getRequest()->getVar('pdf')) {
			return $this->generatePDF($data);
		}
		else {
			return $this->customise($data)->renderWith(array('News' . 'Detail', 'Page', 'ContentController'));
		}
	}
	
	/**
	 * Generate PDF
	 */
	function generatePDF($data) {
		$html = $this->customise($data)->renderWith('CampAreaPDF');
		include_once BASE_PATH . '/mysite/code/other/MPDF561/mpdf.php';
		error_reporting(0);
		//echo $html;die();
		$mpdf = new mPDF();
		$mpdf->ignore_invalid_utf8 = true;
		$mpdf->WriteHTML($html);
	
		$file = TEMP_FOLDER . '/tmp_pdf_' . time() . '.pdf';
		$mpdf->Output($file, 'F');
		$response = SS_HTTPRequest::send_file(file_get_contents($file), Convert::raw2url($data["Title"]) . '.pdf', 'application/pdf');
		unlink($file);
	
		return $response->output();
	}
	
	function getAssetsPath() {
		return ASSETS_PATH;
	}
	function getBaseFolder() {
		return Director::baseFolder();
	}
	
	/**
	 * Loads parameters from GET
	 */
	function loadGETFilters() {
		/* Load filters */
		$offset = intval($this->getRequest()->getVar('page'));
		$class	= $this->getRequest()->getVar('class');
				
		if ($offset > 0) $this->offset = $offset;
		/*if ($class::get()->first() instanceof Object) $this->className = $class;*/
		if(trim($class)) $this->className = $class;
	}
	
	public function getSelectedFilterItem($item) {
		return $this->getRequest()->getVar($item);
	}	
	
	public function Tags() {
		$news = SocialPost::get();
		$tags = TagFromWorld::get()->filter('News.ID', $news->column('ID'))->filter('Locale', Translatable::get_current_locale());
		
		if ($currentTags = $this->getSelectedFilterItem('tag')) {
			$tmp = new ArrayList();
			
			foreach ($tags as $tag) {
				if (in_array($tag->ID, $currentTags)) $tag->Selected = true;
				else $tag->Selected = false;
		
				$tmp->push($tag);
			}
			$tags = $tmp;
		}		
		
		return $tags;
	}
	
	public function TagsFromWorld() {
		$news = NewsFromWorld::get();
		$tags = TagFromWorld::get()->filter('News.ID', $news->column('ID'))->filter('Locale', Translatable::get_current_locale());
		
		if ($currentTags = $this->getSelectedFilterItem('tagW')) {
			$tmp = new ArrayList();
			
			foreach ($tags as $tag) {
				if (in_array($tag->ID, $currentTags)) $tag->Selected = true;
				else $tag->Selected = false;
		
				$tmp->push($tag);
			}
			$tags = $tmp;
		}		
		
		return $tags;
	}
	
	public function TagsBigBarry() {
		$news = NewsBigBarry::get();
		$tags = TagBigBarry::get()->filter('News.ID', $news->column('ID'))->filter('Locale', Translatable::get_current_locale());
		
		if ($currentTags = $this->getSelectedFilterItem('tagB')) {
			$tmp = new ArrayList();
			
			foreach ($tags as $tag) {
				if (in_array($tag->ID, $currentTags)) $tag->Selected = true;
				else $tag->Selected = false;
		
				$tmp->push($tag);
			}
			$tags = $tmp;
		}		
		
		return $tags;
	}
	
	public function TagsStories() {
		$news = Stories::get();
		$tags = TagStories::get()->filter('News.ID', $news->column('ID'))->filter('Locale', Translatable::get_current_locale());
		
		if ($currentTags = $this->getSelectedFilterItem('tagS')) {
			$tmp = new ArrayList();
			
			foreach ($tags as $tag) {
				if (in_array($tag->ID, $currentTags)) $tag->Selected = true;
				else $tag->Selected = false;
		
				$tmp->push($tag);
			}
			$tags = $tmp;
		}		
		
		return $tags;
	}
	
	/**
	 * List all news (limited with offset)
	 * @return DataList
	 */
	function ListNews($class = null) {
		$class = ($class == null) ? $this->className : $class ;
		$news = $this->getBasicFilteredList($class)->limit($this->limit, $this->limit * $this->offset);
		return $news;
	}
	
	/**
	 * Return news list with filters
	 * @return DataList
	 */
	private function getBasicFilteredList($class = null) {
		if($class == null) $class = $this->className;
		$news = NewsList::NewsWhere($class::get()->filter($class.'.Locale', Translatable::get_current_locale()))->sort('LastEdited DESC');
		#print_r($news->count());die;
		if ($this->TagID) $news = $news->filter('Tags.ID', $this->TagID);
		else {
			$selectedTags = $this->getSelectedFilterItem('tag');
			if ($selectedTags && $class == 'News') {
				$news = $news->filter('Tags.ID', $selectedTags);
			}

			$selectedTagsW = $this->getSelectedFilterItem('tagW');
			if ($selectedTagsW && $class == 'NewsFromWorld') {
				$news = $news->filter('Tags.ID', $selectedTagsW);
			}

			$selectedTagsB = $this->getSelectedFilterItem('tagB');
			if ($selectedTagsB && $class == 'NewsBigBarry') {
				$news = $news->filter('Tags.ID', $selectedTagsB);
			}
			
			$selectedTagsS = $this->getSelectedFilterItem('tagS');
			if ($selectedTagsS && $class == 'Stories') {
				$news = $news->filter('Tags.ID', $selectedTagsS);
			}
		}
		
		return $news;
	}
	
	/**
	 * Check if we need to display load more button
	 * @return boolean
	 */
	function getHasMorePages($class = null) {
		$list = $this->getBasicFilteredList($class);
		return $list->Count() > $this->limit + $this->limit * $this->offset;
	}
	
	/** nevem zakaj ne pobere $class iz template-a **/
	function getHasMorePagesNewsFromWorld() {
		$list = $this->getBasicFilteredList('NewsFromWorld');
		return $list->Count() > $this->limit + $this->limit * $this->offset;
	}
	function getHasMorePagesBigBarry() {
		$list = $this->getBasicFilteredList('NewsBigBarry');
		return $list->Count() > $this->limit + $this->limit * $this->offset;
	}
	
	function getHasMorePagesStories() {
		$list = $this->getBasicFilteredList('Stories');
		return $list->Count() > $this->limit + $this->limit * $this->offset;
	}
	
	/**
	 * Load more news - ajax method
	 * @return string|NewsListPage_Controller
	 */
	function more() {
		if (!Director::is_ajax()) return $this->redirect($this->Link());		

		$class = $this->className;
		$news = $this->ListNews($class);
		$out = array();
			
		// add rendered news to output
		foreach ($news as $new) $out[] = $new->renderWith('ListSocialPost');
			
		return json_encode(
			array(
				'data'	=> implode("\n", $out),
				'more'	=> $this->getHasMorePages() ? $this->MoreLink() : false,
				'className'	=> $class,
			)
		);
	}
	
	function MoreLink() {
		$getVars = $this->getRequest()->getVars();
		unset($getVars['url']);
		
		$getVars['page'] = ($this->offset + 1);
		
		return $this->Link() . 'more/?' . http_build_query($getVars);
	}
	
	/**
	 * Function to call in DOAPHolder for template rendering.
	 * @return string
	 */
	function getItemClass() {
		$ID =  $this->getRequest()->param('ID');
		echo $ID;
		if($data = News::get()->filter('URLSegment', $ID)->first()) {}
		elseif($data = NewsBigBarry::get()->filter('URLSegment', $ID)->first()) {}
		elseif($data = NewsFromWorld::get()->filter('URLSegment', $ID)->first()) {}
		elseif($data = Stories::get()->filter('URLSegment', $ID)->first()) {}
		elseif($data = SocialPost::get()->filter('URLSegment', $ID)->first()) {}
		return $data->ClassName;
		//return 'News';
	}
	
	function NewsListCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			News::get()->max('LastEdited')
		);
	
		return implode('_', $params);
	}
}