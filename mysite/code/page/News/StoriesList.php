<?php

class StoriesList extends NewsList {
	
	private static $description = 'Stories';
	private static $singular_name = 'Stories';	
	
    function getCMSFields(){
        $fields = parent::getCMSFields();
        return $fields;
    }
}

class StoriesList_Controller extends NewsList_Controller {

	private static $allowed_actions = array(
		'more', 'tag', 'updatetag',
	);
	
	function init() {
		parent::init();
	}
    
    function updatetag(){
        $stories = Stories::get()->filter('Locale', Translatable::get_current_locale());
        $kolpaTag = TagStories::get()->filter(array(
            'Title' => 'Kolpa River',
            'Locale' => 'en_US'
        ))->first();
        if($kolpaTag){
            $parentTagID = $kolpaTag->ID;
            foreach($stories as $story){
                $story->ParentTagID = $parentTagID;
                $story->write();
            }
            die('done');
        }
    }
    
    function getAllTags(){
        $tags = TagStories::get()->filter('Locale', Translatable::get_current_locale());
        return $tags;
    }
    
    function getSubTagsOfCurrentTag(){
        $currTag = $this->getCurrentTag();
        if($currTag){
            $subTags = $currTag->SubTags();
            if ($currentTags = $this->getSelectedFilterItem('tagS')) {
    			$tmp = new ArrayList();
    			foreach ($subTags as $subTag) {
    				if (in_array($subTag->ID, $currentTags)){
    				    $subTag->Selected = true;
                    }else{
                        $subTag->Selected = false;   
                    }
    				$tmp->push($subTag);
    			}
    			$subTags = $tmp;
    		}
            return $subTags;
        }
        
    }
    
    function getCurrentTag(){
        $tagSegment = $this->request->param('OtherID');
        return TagStories::get()->filter(array(
            'Locale' => Translatable::get_current_locale(),
            'URLSegment' => $tagSegment,
        ))->first();
    }
    
    function tag(){        
        return $this->renderWith(array('StoriesList_Tag', 'ContentController'));
    }
    
    //$tag = $this->request->param('OtherID');
    
    function getStoriesByFeaturedTags(){
        $results = new ArrayList();
        $featuredTags = $this->FeaturedTags();
        if($featuredTags){
            foreach($featuredTags as $tag){
                $news = $tag->News()->filter('Locale', Translatable::get_current_locale())->sort('LastEdited DESC');
                $results->merge($news);
            }
        }
        $results->removeDuplicates();
        return $results;
    }
    
    
    function ListNews($class = null) {
		$class = ($class == null) ? $this->className : $class ;
		$news = $this->getBasicFilteredList($class)->limit($this->limit, $this->limit * $this->offset);
		return $news;
	}
    
    function getHasMorePages($class = 'Stories') {
		$list = $this->getBasicFilteredList($class);
		return $list->Count() > $this->limit + $this->limit * $this->offset;
	}
    
    private function getBasicFilteredList($class = null) {
		if($class == null) $class = $this->className;
		$news = NewsList::NewsWhere($class::get()->filter('Locale', Translatable::get_current_locale()))->sort('LastEdited DESC');
		$currTag = $this->getCurrentTag();
        if($currTag){
            $news = $news->filter('ParentTagID', $currTag->ID);
            if(!$news){
                $subTags = SubTagStories::get()->filter(array(
                    'Locale' => Translatable::get_current_locale(),
                    'TagStoriesID' => $currTag->ID
                ));
                if($subTags){
                    $subTagsIDS = implode(',', $subTags->map('ID', 'ID')->toArray());
                    if($subTagsIDS != '')                
                        $news = $news->where("$class.TagID IN ($subTagsIDS)");
                }
            }
        }
		$selectedTags = $this->getSelectedFilterItem('tag');
		if ($selectedTags && $class == 'News') {
			$news = $news->filter('Tags.ID', $selectedTags);
		}

		$selectedTagsW = $this->getSelectedFilterItem('tagW');
		if ($selectedTagsW && $class == 'NewsFromWorld') {
			$news = $news->filter('Tags.ID', $selectedTagsW);
		}

		$selectedTagsB = $this->getSelectedFilterItem('tagB');
		if ($selectedTagsB && $class == 'NewsBigBarry') {
			$news = $news->filter('Tags.ID', $selectedTagsB);
		}
		
		$selectedTagsS = $this->getSelectedFilterItem('tagS');
		if ($selectedTagsS && $class == 'Stories') {
            $tagsIDS = implode(',', $selectedTagsS);  
			$news = $news->where("TagID IN ($tagsIDS)");
            
		}
		
		return $news;
	}

}