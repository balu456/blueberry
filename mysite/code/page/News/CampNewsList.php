<?php

class CampNewsList extends NewsList {
	
	private static $description = 'Seznam novic - kemp';
	private static $singular_name = 'Seznam novic - kemp';	
	
	private static $has_one = array(
		'Tag' => 'Tag'
	);
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
			
		$fields->addFieldToTab('Root.Main', DropdownField::create('TagID')->setSource(Tag::get()->filter('News.ID', News::get()->column('ID'))->map()));
	
		return $fields;
	}	
		
}

class CampNewsList_Controller extends NewsList_Controller {

	private static $allowed_actions = array(
		'more'
	);
	
	function init() {
		parent::init();
	}

}