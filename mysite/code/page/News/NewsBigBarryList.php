<?php

class NewsBigBarryList extends NewsList {
	
	private static $description = 'People';
	private static $singular_name = 'People';	
	
	private static $has_one = array(
		'Tag' => 'Tag'
	);	
}

class NewsBigBarryList_Controller extends NewsList_Controller {

	private static $allowed_actions = array(
		'more'
	);
	
	function init() {
		parent::init();
	}

}