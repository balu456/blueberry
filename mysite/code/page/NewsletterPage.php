<?php

class NewsletterPage extends Page {

	private static $icon = 'mysite/images/cms_icons/newsletter_signup.gif';
	private static $description = 'Prijava na e-novice';
	private static $singular_name = 'Prijava na e-novice';

	private static $db = array(
		'AfterContent' => 'HTMLText',
	);
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
	
		$fields->addFieldsToTab('Root.Main', array(
			HtmlEditorField::create('AfterContent')->setTitle('Besedilo, ki se izpiše po oddaji obrazca'),
		), 'Metadata');
		
		return $fields;
	}

}

class NewsletterPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
		'SignupForm'
	);

	public function init() {
		parent::init();
	}
	
	public function SignupForm() {
		$fields = new FieldList(
			TextField::create('Name')->setAttribute('placeholder', _t('NewsletterPage.NAME', 'Ime')),
			TextField::create('Surname')->setAttribute('placeholder', _t('NewsletterPage.SURNAME', 'Priimek')),
			$emailField = EmailField::create('Email')->setAttribute('placeholder', _t('NewsletterPage.EMAIL', 'Email')),
			CheckboxField::create('GeneralMailing')->setTitle(_t('NewsletterPage.GENERAL_SIGN_UP', 'Prijavite se na e-obveščanje Big Berry (splošna predstavitev mobilnih hišk, nakup mobilne hiške,...)')),
			CheckboxField::create('RentMailing')->setTitle(_t('NewsletterPage.RENT_SIGN_UP', 'Prijavite se na e-obveščanje o najemu mobilnih hišk (predstavitev kampov, akcije,...)')),
	
			// http://haacked.com/archive/2007/09/11/honeypot-captcha.aspx
			TextField::create('hc666')->setRightTitle('If you see this, leave this field empty!')->addExtraClass('hc666')
		);
		
		$email = $this->getRequest()->getVar('email');
		if (isset($email) && !empty($email)) $emailField->setValue($email);
		
		$actions = new FieldList(
			new FormAction('signup', _t('NewsletterPage.SUBMIT', 'Submit'))
		);
		
		$required = new RequiredFields(array('Email', 'Name', 'Surname'));
		
		$form = new Form($this, 'SignupForm', $fields, $actions, $required);
		
		return $form;		
	}
	
	public function signup(Array $data, Form $form) {
		
		if (!empty($data['hc666'])) return false;
		
		$submit = array();
		$fields = array('Name', 'Surname', 'Email');
		foreach($fields as $field) {
			$value = Convert::html2raw(Convert::raw2sql($data[$field]));
			$value = trim($value);
			if($field == 'Email')
				$value = strtolower($value);
				
			$submit[$field] = $value;
		}
		
		//$lists = array('sl_SI' => 'bf7bb0a8c2', 'en_US' => 'ae1ac89da7');
		
		
		
		// not sure if this is working
		$groupsarr = array();
		if (isset($submit['GeneralMailing'])) 
			$groupsarr[0] = "Test 1";
		else if (isset($submit['RentMailing'])) 
			$groupsarr[0] = "Test 2";
		else if (isset($submit['GeneralMailing']) && isset($submit['RentMailing'])) 
			$groupsarr = array_merge($groupsarr, array("Test 1","Test 2"));
		
			
		if (self::sign_up($submit['Name'], $submit['Surname'], $submit['Email'], self::get_list(), $groupsarr))
			$form->setMessage(_t('NewsletterPage.ERROR', 'There was an error, please contact administrator.'), 'bad');
		else {
			Session::set('good', 1);
		}
			
		return $this->redirect($this->Link());
	}
	
	static function get_api_key() {
		if (Director::isDev()) $apikey = '9b51d6698fd8e0b555918724681ed9e1-us10';
		else $apikey = '4120df70c3ac7c8930955dc823e6bd93-us11';
		
		return $apikey;
	}
	
	static function get_list() {
		if (Director::isDev()) $lists = array('sl_SI' => '18812ebbc3');
		else $lists = array('sl_SI' => '4768fb250a');
		
		$locale = Translatable::get_current_locale();
		
		return isset($lists[$locale]) ? $lists[$locale] : reset($lists);
	}
	
	static function sign_up($name, $surname, $mail, $list, $groups) {

		include_once BASE_PATH . '/mysite/code/other/mailchimp/MailChimp.php';
		
		$mailchimp = new \Drewm\MailChimp(self::get_api_key());
		
		$merge_vars = array(
			'FNAME' => $name,
			'LNAME' => $surname,
			'GROUPINGS' => array(
				array(
					'name' => "Test", // You can use either 'name' or 'id' to identify the group
					'groups' => $groups
				),
			)
		);
		
		$result = $mailchimp->call('lists/subscribe', array(
			'id'            => $list,
			'email'			=> array('email' => $mail),
			'merge_vars'	=> $merge_vars
		));
		
		/*
		 * List -> Groups -> Segments inside Mailchimp
		 */
		
		return !isset($result['error']);
	}
	
	
	/**
	 * Form message
	 * @return Ambigous <NULL, multitype:, unknown>
	 */
	function FormCompleted() {
		$message = Session::get('good');
		if($message) {
			Session::set('good', 0);
			Session::clear('good');
			return true;
		}
		return false;
	}
}
