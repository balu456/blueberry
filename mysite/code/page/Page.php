<?php

class Page extends SiteTree {

	private static $db = array(
		'YoutubeLink'	=> 'Varchar(255)',
		'YoutubeTitle'	=> 'Varchar(255)'
	);
	
	private static $has_one = array(
		'ImageGallery' => 'ImageGallery',
	);
	
	private static $many_many = array(
// 		'Images' 	=> 'Image',
		'Files'		=> 'File'
	);
	
	private static $many_many_extraFields = array(
// 		'Images' => array(
// 			'SortOrder' => 'Int'
// 		),
		'Files' => array(
			'SortOrder' => 'Int'
		)
	);
	
	public function Images() {
		$gallery = $this->ImageGallery();
		if(!$gallery) return false;
		return $gallery->Images()->sort('SortOrder');
// 		return $this->getManyManyComponents('Images')->sort('SortOrder');
	}
	
	public function Files() {
		return $this->getManyManyComponents('Files')->sort('SortOrder');
	}
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
	
		$fields->addFieldsToTab('Root.Main', array(
			TextareaField::create('YoutubeLink')->setTitle('Youtube celoten link'),
			TextField::create('YoutubeTitle')->setTitle('Youtube naslov na gumbu')	
		));
		
		$fields->addFieldsToTab('Root.Images', array(
			DropdownField::create('ImageGalleryID')->setTitle('Galerija')->setEmptyString('-brez-')->setSource(ImageGallery::get()->map()),
// 			new SortableUploadField('Images'),
		));
		$fields->addFieldToTab('Root.Files', new SortableUploadField('Files'));
	
		return $fields;
	}
	
	/**
	 * Get first page of type $class
	 *
	 * @param string $class
	 * @return SiteTree
	 */
	function pageByClass($class) {
		if ($page = $class::get()->First()) return $page;
	}
	
	/**
	 * Menu for header/footer on page
	 * @param int|null $level
	 * @return boolean|DataList
	 */
	function vMenu($level = false) {
		$level = intval($level);
	
		if($level === 2)
			$filter = array('MenuFooterLeft' => 1);
		else if($level === 3)
			$filter = array('MenuFooterRight' => 1);
		else
			return false;
	
		return Page::get()->filter($filter)->sort('Sort');
	}

	public static function EncodedAbsoluteLink($link=null) {
	   if(!$link){
	       $link = Director::absoluteURL(Controller::curr()->Link());
	   }
		return urlencode($link);
	}
	
	/**
	 * Used for caching - detect changes in many many tables
	 *
	 * @param string $table
	 * @return string
	 */
	public static function ManyManyTableCacheKey($table) {
		$sql = 'CHECKSUM TABLE ' . $table;
		
		$sqlResult = DB::query($sql)->map();
		
		$arrValues = array_values($sqlResult);
        return $arrValues[0];
	}
	
	function MenuCacheKey() {
		$params = array(
			$this->Link(),
			SiteTree::get()->max('LastEdited'),
		);
	
		return implode('_', $params);
	}

}

class Page_Controller extends ContentController {

	private static $allowed_actions = array(
        'test',
	);
    
    function test(){
        $url = 'http://www.belakrajina.si/en/traditional-events-in-bela-krajina/';
        $html = file_get_contents($url);
    }

	public function init() {
		parent::init();
		
		if ($this->dataRecord->hasExtension('Translatable')) {
			i18n::set_locale($this->dataRecord->Locale);
			setlocale(LC_ALL, $this->dataRecord->Locale);
		}
	}
	
	public function RequiredJS() {
		Requirements::javascript('//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js');
		Requirements::javascript('mysite/js/libs/select2.min.js');
		Requirements::javascript('mysite/js/libs/jquery-ui.min.js');
		Requirements::javascript('mysite/js/libs/datepicker-' . $this->Lang() . '.js');
		Requirements::javascript('mysite/js/libs/slick.min.js');
//		Requirements::javascript('mysite/js/libs/lightbox.min.js');
		Requirements::javascript('mysite/js/libs/jquery.fancybox.pack.js');
		Requirements::javascript('mysite/js/libs/jquery.equalHeights1337.min.js');
		Requirements::javascript('mysite/js/libs/YouTubePopUp/YouTubePopUp.jquery.js');
		Requirements::javascript('mysite/js/functions.js');
		Requirements::javascript('mysite/js/jquery.rwdImageMaps.min.js');
	}
	
	/**
	 * Return true if site is in live mode
	 * @return boolean
	 */
	public function isLive() {
		return Director::isLive();
	}
    
    function currentLang(){
        return strtoupper(Translatable::current_lang());
    }
	
	/**
	 * Language menu
	 * @return ArrayList
	 */
    function LangMenu() {
        $curr_locale = i18n::get_locale();
        $items = new ArrayList();
        foreach ($this->getTranslations() as $t) {
            if($t->Locale != $curr_locale)
                $items->push($t);
        }
        // add current
        #$items->push($this);
        
        // find missing ones and add homepages
        $allLangs = Translatable::get_existing_content_languages();
        unset($allLangs[$curr_locale]);
        $missing = array_diff(array_keys($allLangs), $items->column('Locale'));
        
        foreach ($missing as $locale) if ($homePage = HomePage::get()->filter('Locale', $locale)->first()) $items->push($homePage);
        
        // sort
        $items = $items->sort('Locale', 'DESC');
        
        return $items;
    }
	
	public function Lang() {
		$lang = explode('_', $this->Locale);
	
		return strtoupper($lang[0]);
	}
	
	public function getSelectedFilterItem($item) {
		return $this->getRequest()->getVar($item);
	}
	
	public function Camps() {
		return Camp::get()->sort('Title');
	}	

	function PageCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			Page::ManyManyTableCacheKey('Page_Images'),
			Page::ManyManyTableCacheKey('Page_Files'),
		);
	
		return implode('_', $params);
	}
	
	function SearchLink() {
		$page = SearchPage::get()->first();
		if($page)
			return $page->Link('results');
		return false;
	}
	
	public static function ButtonParser ($arguments, $mycontent, $parser, $shortcode) {
		return '<button onclick="topFunction()" id="myBtn" title="Go to top">'.$mycontent.'</button>';
	}	
	
}
