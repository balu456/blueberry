<?php

class CampInfo extends DataObjectAsPageHolder {

	private static $icon = 'mysite/images/cms_icons/camp_info.png';	
	private static $description = 'Camp info';

	private static $db = array(
	);
	
	private static $has_one = array(
		'Image' => 'Image',
	);
	
	private static $has_many = array(
		'CampOffers' 	=> 'CampOffer',
	);
	
	private static $many_many = array(
		'CampIcons' => 'CampIcon',
	);
	
	private static $many_many_extraFields = array(
		'CampIcons' => array(
			'SortOrder' => 'Int'
		),
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		$offersConf = new GridFieldConfig_RelationEditor();
		$offersConf->addComponent(new GridFieldSortableRows('SortOrder'));
		
		$iconsConf = new GridFieldConfig_RelationEditor();
		$iconsConf->addComponent(new GridFieldSortableRows('SortOrder'));
		$iconsConf->removeComponentsByType('GridFieldAddNewButton');
		
		$fields->addFieldToTab('Root.CampOffers', new GridField('CampOffers', 'CampOffers', $this->CampOffers(), $offersConf));
		//$fields->addFieldToTab('Root.CampIcons', new GridField('CampIcons', 'CampIcons', $this->CampIcons(), $iconsConf));
		$fields->addFieldsToTab('Root.Main', array(
			UploadField::create('Image')->setTitle('Slika z načrtom kampa')
		));
	
		return $fields;
	}
	
	public function CampIcons() {
		return $this->getManyManyComponents('CampIcons')->sort('SortOrder');
	}

}

class CampInfo_Controller extends DataObjectAsPageHolder_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}
	
	public function RequiredJS() {
		parent::RequiredJS();
		
		Requirements::javascript('//maps.googleapis.com/maps/api/js');
		Requirements::javascript('mysite/js/libs/maplace-0.1.3.min.js');
		
		$locs = "
			$(document).ready(function() {
				
				var locs = [{
					lat: '" . $this->Parent()->Latitude . "',
					lon: '" . $this->Parent()->Longitude . "', zoom: 16,
					icon: './mysite/images/map/map-pin.png'
				}];
						
				$('#mobile-map').attr('href', 'http://www.google.com/maps/place/' + '".$this->Parent()->Latitude.",".$this->Parent()->Longitude."');
						
				if (typeof locs !== 'undefined') {
				    new Maplace({
				        locations: locs,
						controls_on_map: false,
				    }).Load();		
				} 
				
				$('#mapform').submit(function(event) {
					event.preventDefault();
					var address = $(this).find('input[name=\"address\"]').val();
					var googleurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address;
					
					$.ajax({
				    	url: googleurl,
				        dataType: \"json\",
				        success: function(data) {
							var location = data.results[0].geometry.location;
							locs[1]={lat: location.lat, lon: location.lng};
						
							new Maplace({
								locations: locs.slice(0).reverse(),
								controls_on_map: false,
								type: 'directions',
								draggable: true,
								afterRoute: function(distance, status, result) {
									if(status == 'OK') {
										$('#distance-info').html((distance/1000).toFixed(2) + ' km');
										$('p.distance').addClass('show-it');
									}
								}
							}).Load();
							
				        }
				    });
						
				
				});
			});
		";
		
		Requirements::customScript($locs);
	}

	/**
	 * Function to call in DOAPHolder for template rendering.
	 * @return string
	 */
	function getItemClass() {
		return 'CampOffer';
	}
	
	function CampInfoCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			$this->ImageID,
			CampOffer::get()->max('LastEdited'),
			Page::ManyManyTableCacheKey('CampInfo_CampIcons'),
		);
	
		return implode('_', $params);
	}	
}
