<?php

class CampArea extends DataObjectAsPageHolder {

	private static $icon = 'mysite/images/cms_icons/camp_area.png';	
	private static $description = 'V okolici kampa';
	private static $singular_name = 'V okolici kampa';

	private static $db = array(
		'GoogleMap'		=> 'Text',
		'AutomaticPDF'	=> 'Boolean'
	);
	
	private static $has_many = array(
		'CampAttractions' => 'CampAttraction'
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		$attrConf = new GridFieldConfig_RelationEditor();
		$attrConf->addComponent(new GridFieldSortableRows('SortOrder'));
		
		$fields->addFieldToTab('Root.CampAttractions', new GridField('CampAttractions', 'CampAttractions', $this->CampAttractions(), $attrConf));

		// $fields->addFieldToTab('Root.Main', TextareaField::create('GoogleMap')->setTitle('Google map iframe (link: http://help.create.net/hc/en-us/articles/202491369-How-to-pin-point-multiple-locations-on-Google-maps-)'));
	
		$fields->addFieldToTab('Root.Main', CheckboxField::create('AutomaticPDF')->setTitle('Avtomatska izdelava PDF dokumentov'), 'Content');
		
		return $fields;
	}

}

class CampArea_Controller extends DataObjectAsPageHolder_Controller {

	private static $allowed_actions = array();

	public function init() {
		parent::init();
	}
	
	public function RequiredJS() {
		parent::RequiredJS();
	
		Requirements::javascript('//maps.googleapis.com/maps/api/js');
		Requirements::javascript('mysite/js/libs/maplace-0.1.3.min.js');
	}

	/**
	 * Function to call in DOAPHolder for template rendering.
	 * @return string
	 */
	function getItemClass() {
		return 'CampAttraction';
	}
	
	function CampAreaCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			CampAttraction::get()->max('LastEdited')
		);
	
		return implode('_', $params);
	}
	
	/**
	 * Render item detail
	 *
	 * @param DataObjectAsPage $item
	 */
	public function showDetail($item) {
		if (!$item->canView()) return Security::permissionFailure($this);
		
		$data = array(
			'Item'			=> $item,
			'Breadcrumbs'	=> $item->Breadcrumbs(false, false, 'RedirectorPage', true),
			'MetaTags'		=> $item->MetaTags(false),
			'Title'			=> $item->Title,
			'Title2'		=> $this->Title,
			'Subtitle'		=> $this->Parent()->Title,
		);
	
		if ($this->hasMethod('updateShowDetailData')) $data = $this->updateShowDetailData($data);
	
		if ($this->getRequest()->getVar('pdf')) {
			return $this->generatePDF($data);
		}
		else {
			if ($item->ShowMap()) {
				$tmpl = new SSViewer('CampAttractionMapJS');
				Requirements::customScript($tmpl->process($item));
			}
			
			return $this->customise($data)->renderWith(array($this->getItemClass() . 'Detail', 'Page', 'ContentController'));
		}
	}

	
	/**
	 * Generate PDF
	 */
	function generatePDF($data) {
		$html = $this->customise($data)->renderWith('CampAreaPDF');
		include_once BASE_PATH . '/mysite/code/other/MPDF561/mpdf.php';
		error_reporting(0);
		//return $html;die();
		$mpdf = new mPDF();
		$mpdf->defaultheaderline = 0;
		$mpdf->ignore_invalid_utf8 = true;
		$mpdf->SetHTMLFooter('<p style="text-align:center;font-size:10px;">'.Director::absoluteBaseURL().'</p>');
		$mpdf->WriteHTML($html);
	
		$file = TEMP_FOLDER . '/tmp_pdf_' . time() . '.pdf';
		$mpdf->Output($file, 'F');
		$response = SS_HTTPRequest::send_file(file_get_contents($file), Convert::raw2url($data["Title"]) . '.pdf', 'application/pdf');
		unlink($file);
	
		return $response->output();
	}
	
	function getAssetsPath() {
		return ASSETS_PATH;
	}
	function getBaseFolder() {
		return Director::baseFolder();
	}
	
	/**
	 * filtered News
	 */
	public function getFilteredCampAttractions() {
		$items = $this->CampAttractions();
		$selectedTags = $this->getSelectedFilterItem('tag');
		if($selectedTags) $items = $items->filter('Tags.ID', $selectedTags);
		return $items;
	}
	
	/**
	 * Tags
	 */
	public function getAllTags() {		
		$news = $this->CampAttractions()->sort('Title');
		$tags = TagNews::get()->filter('CampAttractions.ID', $news->column('ID'))->filter('Locale', Translatable::get_current_locale());
		
		if ($currentTags = $this->getSelectedFilterItem('tag')) {
			$tmp = new ArrayList();
				
			foreach ($tags as $tag) {
				if (in_array($tag->ID, $currentTags)) $tag->Selected = true;
				else $tag->Selected = false;
		
				$tmp->push($tag);
			}
			$tags = $tmp;
		}
		
		return $tags;
	}
	
	public function getSelectedFilterItem($item) {
		return $this->getRequest()->getVar($item);
	}
	
}
