<?php

class Camp extends RedirectorPage {

	private static $icon = 'mysite/images/cms_icons/camp.gif';	
	private static $description = 'Camp';

	private static $db = array(
		'SubTitle'		=> 'Varchar(255)',
		'Description'	=> 'Varchar(150)',
		'Address'		=> 'Varchar(255)',
		'Latitude' 		=> 'Varchar(25)',
		'Longitude'		=> 'Varchar(25)'
	);
	
	private static $has_one = array(
		'ListImage'		=> 'Image',
		'Destination'	=> 'Destination'
	);
	
	private static $field_labes = array(
		'Address'		=> 'Naslov'
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
	
		$fields->addFieldsToTab('Root.Main',
			array(
				TextField::create('SubTitle')->setTitle('Podnaslov'),
				TextareaField::create('Description')->setTitle('Opis'),
				UploadField::create('ListImage')->setTitle('Slika za izpostavek, oz za listing vseh kampov'),
				DropdownField::create('DestinationID', 'Destination', Destination::get()->map())->setEmptyString('-')->setTitle('Kategorija kampa'),
				TextField::create('Address'),
				TextField::create('Latitude'),
				TextField::create('Longitude')
			)
		);

		return $fields;
	}
	
	public function BestPrice() {
		$houses = CampHouses::get()->filter('ParentID', $this->ID)->first();	
			
		if ($houses) {
			$lowest = $houses->CampHouses()->sort('Price')->first();
			
			if ($lowest) return $lowest->NicePrice();
		}
		
		return false;
	}

}

class Camp_Controller extends RedirectorPage_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}

}
