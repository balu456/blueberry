<?php
class CampGallery extends Page {
	
	private static $description = 'Camp Galerija';
	private static $singular_name = 'Camp Galerija';
	
}

class CampGallery_Controller extends Page_Controller {
	
	public function init() {
		parent::init();
	}
	
	public function getTags() {
		$tagIDs = array();
		// get all used tags
		foreach($this->Images() as $image) {
			foreach($image->Tags() as $tag) {
				$tagIDs[]	= $tag->ID;				
			}
			
		}
		$tagIDs = array_filter($tagIDs);
		$tags = TagImage::get()->filter('ID', $tagIDs)->filter('Locale', Translatable::get_current_locale());
		
		if ($currentTags = $this->getSelectedFilterItem('tag')) {
			$tmp = new ArrayList();
				
			foreach ($tags as $tag) {
				if (in_array($tag->ID, $currentTags)) $tag->Selected = true;
				else $tag->Selected = false;
		
				$tmp->push($tag);
			}
			$tags = $tmp;
		}
		
		return $tags;
	}
	
	public function getFilteredImages() {
		$images = $this->Images();
		/**
		 * Filter Tags
		 */
		$tags = $this->getRequest()->getVar('tag');
		if(!empty($tags)) {
			$images = $this->Images()->filter('Tags.ID', $tags);
		}
		
		return $images;
	}
	
	function CampAreaCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			$this->Images,
		);
	
		return implode('_', $params);
	}
	
}