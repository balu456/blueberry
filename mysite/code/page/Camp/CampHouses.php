<?php

class CampHouses extends DataObjectAsPageHolder {

	private static $icon = 'mysite/images/cms_icons/camp_houses.png';	
	private static $description = 'Camp houses';

	private static $db = array(
	);
	
	private static $has_many = array(
		'CampHouses' => 'CampHouse'
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		$housesConf = new GridFieldConfig_RelationEditor();
		$housesConf->addComponent(new GridFieldSortableRows('SortOrder'));
		
		$fields->addFieldToTab('Root.Houses', new GridField('CampHouses', 'CampHouses', $this->CampHouses(), $housesConf));
	
		return $fields;
	}	

}

class CampHouses_Controller extends DataObjectAsPageHolder_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}
	
	/**
	 * Function to call in DOAPHolder for template rendering.
	 * @return string
	 */
	function getItemClass() {
		return 'CampHouse';
	}

	function CampHousesCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			//CampHouse::get()->map("ID","SortOrder")
		);
	
		return implode('_', $params);
	}
}
