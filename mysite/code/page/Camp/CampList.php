<?php

class CampList extends Page {

	private static $icon = 'mysite/images/cms_icons/camp_list.gif';	
	private static $description = 'Camp list';

	private static $db = array(
	);
	
	private static $has_one = array(
		'ExposedCamp' => 'Camp'
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
	
		$fields->addFieldsToTab('Root.Main',
			DropdownField::create('ExposedCampID', 'ExposedCamp', Camp::get()->map())->setEmptyString('-')
		);
	
		return $fields;
	}
	
	public function ExposedImages() {
		$images = new ArrayList();
		
		if ($this->ExposedCampID) $images->push(array('Image' => $this->ExposedCamp()->ListImage()));
		
		return $images;
	}
	
	public function CarouselData() {
		if (!$this->ExposedCampID) return false;
		
		return new ArrayData(
			array(
				'Title'		=> $this->ExposedCamp()->Title,
				'SubTitle'	=> $this->ExposedCamp()->SubTitle,
				'BestPrice'	=> $this->ExposedCamp()->BestPrice()
			)
		);
	}

}

class CampList_Controller extends Page_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}
	
	public function Camps() {
		$filter = array(
			'ParentID' => $this->ID
		);
		
		if ($dest = $this->getSelectedFilterItem('dest')) $filter['DestinationID'] = $dest;
		
		$camps = Camp::get();
		
		if ($persons = $this->getSelectedFilterItem('persons')) {
			$camps = $camps
						->innerJoin('SiteTree_Live', 'st1."ParentID" = "Camp"."ID"', 'st1')
						->innerJoin('CampHouse', 'ch."CampHousesID" = st1."ID"', 'ch');
			
			$filter['ch.Persons'] = $persons;
		}
		
		return $camps->filter($filter);
	}
	
	public function Destinations() {
		$filter = array(
			'st0.ParentID' => $this->ID
		);
		
		$persons = $this->getSelectedFilterItem('persons');
		
		if ($persons) $filter['ch.Persons'] = $persons;
		
		$destinations = Destination::get()
							->innerJoin('Camp', 'c."DestinationID" = "Destination"."ID"', 'c')
							->innerJoin('SiteTree_Live', 'c."ID" = st0."ID"', 'st0');
		
		if ($persons) $destinations = $destinations		
							->innerJoin('SiteTree_Live', 'st1."ParentID" = c."ID"', 'st1')
							->innerJoin('CampHouse', 'ch."CampHousesID" = st1."ID"', 'ch');
	
		return $destinations->filter($filter)->sort('Title');
	}
	
	public function Persons() {
		$filter = array(
			'st1.ParentID' => $this->ID
		);
		
		if ($destinationID = $this->getSelectedFilterItem('dest')) $filter['Camp.DestinationID'] = $destinationID;
		
		$houses = CampHouse::get()
					->innerJoin('SiteTree_Live', '"CampHouse"."CampHousesID" = st0."ID"', 'st0')
					->innerJoin('SiteTree_Live', 'st1."ID" = st0."ParentID"', 'st1')
					->innerJoin('Camp', 'st1."ID" = "Camp"."ID"')					
					->filter($filter);
		
		$persons = $houses->column('Persons');
		$persons = array_unique($persons);
		sort($persons);
		
		$personsAL = new ArrayList();
		foreach ($persons as $p) $personsAL->push(new ArrayData(array('Value' => $p)));
		
		return $personsAL;
	}
	
	function CampListCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			$this->ExposedCampID,
			Camp::get()->max('LastEdited'),
			//CampHouse::get()->max('LastEdited'),
			Destination::get()->max('LastEdited'),
		);
	
		return implode('_', $params);
	}
	
	function IsDestinations() {
		$pageType = $this->getRequest()->getVar('page');
	
		return $pageType == "destinations";
	}
}
