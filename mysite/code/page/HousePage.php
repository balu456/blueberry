<?php

class HousePage extends RedirectorPage {

	private static $icon = 'mysite/images/cms_icons/houses.png';
	private static $description = 'Stran z vrsto hiše';
	private static $singular_name = 'Stran z vrsto hiše';
	
	private static $db = array(
		'Colour'		=> 'Varchar(20)',
		//'FashionTV'		=> 'Boolean',
		//'FashionTVText' => 'HTMLText',
	);
	
	private static $has_one = array(
		'BoxImage'			=> 'Image',
		//'FashionTVImage'	=> 'Image',
		//'FashionTVLogo'		=> 'Image',
		'BrochureFile'		=> 'File',
		//'FashionTVLink'		=> 'Page'
	);
	/*
	private static $has_one = array(
		'ImageGallery' => 'ImageGallery',
	);
	*/
	private static $many_many = array(
		'Designers' 		=> 'Designer',
		'HouseBanners'		=> 'HouseBanner',
		//'DimensionImages'	=> 'Image',
		'Galleries'			=> 'Gallery',
		'ImageGalleries' 	=> 'ImageGallery',
	);
	
	private static $many_many_extraFields = array(
		'Designers' => array(
			'SortOrderMany' => 'Int'
		),
		'HouseBanners' => array(
			'SortOrder' => 'Int'
		),
		/*
		'DimensionImages' => array(
			'SortOrder' => 'Int'
		),
		*/
		'Galleries' => array(
			'SortOrder' => 'Int'
		),
		'ImageGalleries' => array(
			'SortOrder' => 'Int'
		),
	);
	
	/**
	 * Gets fields used in the cms
	 * @return {FieldSet} Fields to be used
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$colour = array(
			'blue' 			=> 'blue',
			'brown' 		=> 'brown',
			'fashiontv'		=> 'fashiontv',
			'bloomingville' => 'bloomingville',
			'kvadrat'		=> 'kvadrat'
		);
			
		$designersConf = new GridFieldConfig_RelationEditor();
		$designersConf->addComponent(new GridFieldSortableRows('SortOrderMany'));
		// hide "Add new" button
		$designersConf->removeComponentsByType('GridFieldAddNewButton');
	
		$bannersConf = new GridFieldConfig_RelationEditor();
		$bannersConf->addComponent(new GridFieldSortableRows('SortOrder'));
		// hide "Add new" button
		$bannersConf->removeComponentsByType('GridFieldAddNewButton');
		
		$fields->addFieldToTab('Root.Designers', new GridField('Designers', 'Designers', $this->Designers(), $designersConf));		
		$fields->addFieldToTab('Root.HouseBanners', new GridField('HouseBanners', 'HouseBanners', $this->HouseBanners(), $bannersConf));
		
		$fields->addFieldsToTab('Root.Main', array(
			//CheckboxField::create('FashionTV')->setTitle('Hiša je tipa Fashion TV'),
			UploadField::create('BoxImage')->setTitle('Podporna slika nad naslovom'),
			DropdownField::create('Colour')->setSource($colour)->setEmptyString('-')->setTitle('Barva (tip hiše)'),
			UploadField::create('BrochureFile')->setTitle('Datoteka (katalog, brošura)')
		));
		
		//$fields->addFieldToTab('Root.DimensionImages', SortableUploadField::create('DimensionImages')->setTitle('Slike z merami'));
		
		$galleryConf = new GridFieldConfig_RelationEditor();
		$galleryConf->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldsToTab(
			'Root.Galleries', 
			array(
				new GridField('Galleries', 'Galleries', $this->Galleries(), new GridFieldConfig_RelationEditor()),
				new GridField('ImageGalleries', 'Galerije', $this->ImageGalleries(), $galleryConf),
			)
		);
	
		/*$fields->addFieldsToTab('Root.FashionTVBanner', array(			
			HtmlEditorField::create('FashionTVText')->setTitle('Vsebina'),
			TreeDropdownField::create('FashionTVLinkID')->setTitle('Povezava na notranjo stran')->setSourceObject('SiteTree'),
			UploadField::create('FashionTVImage')->setTitle('Slika'),
			UploadField::create('FashionTVLogo')->setTitle('FashionTV logo')
		));*/
		
		return $fields;
	}
	

	public function Designers() {
		return $this->getManyManyComponents('Designers')->sort('SortOrderMany');
	}
	
	public function HouseBanners() {
		return $this->getManyManyComponents('HouseBanners')->sort('SortOrder');
	}
	
	/*
	public function DimensionImages() {
		return $this->getManyManyComponents('DimensionImages')->sort('SortOrder');
	}
	*/
}

class HousePage_Controller extends RedirectorPage_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}

}
