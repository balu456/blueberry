<?php

class DesignerList extends DataObjectAsPageHolder {
	
	private static $icon = 'mysite/images/cms_icons/designerlist.png';
	
	private static $description = 'Seznam oblikovalcev';
	private static $singular_name = 'Seznam oblikovalcev';
	
	private static $db = array(
	);
	
	public static function Designers() {
		return Designer::get()->sort('SortOrder');
	}
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
	
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		return $fields;
	}
	
}

class DesignerList_Controller extends DataObjectAsPageHolder_Controller {
	
	/**
	 * Function to call in DOAPHolder for template rendering.
	 * @return string
	 */
	function getItemClass() {
		return 'Designer';
	}
	
	function DesignerListCacheKey() {
		$params = array(
			$this->Link(),
			$this->ID,
			Designer::get()->max('LastEdited'),
		);
	
		return implode('_', $params);
	}
	
}