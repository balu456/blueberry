<?php

class Pricing extends Page {

	private static $icon = 'mysite/images/cms_icons/price-euro-icon.png';	
	private static $description = 'Stran s cenikom';
	private static $singular_name = 'Stran s cenikom';

	private static $db = array(
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		return $fields;
	}	

}

class Pricing_Controller extends Page_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}

}
