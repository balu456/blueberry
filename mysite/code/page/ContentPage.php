<?php

class ContentPage extends Page {

	private static $icon = 'mysite/images/cms_icons/text_icon.png';	
	private static $description = 'Stran z vsebino in teaserji';
	private static $singular_name = 'Stran z vsebino in teaserji';

	private static $db = array(
		'InquiryButton'	=> 'Boolean',
		'Button'		=> 'Varchar(255)',
		'ButtonText'	=> 'Text',
		'HideSlideShow' => 'Boolean',
	);
	
	private static $many_many = array(
		'ContentTeasers' => 'ContentTeaser',
	);
	
	private static $many_many_extraFields = array(
		'ContentTeasers' => array(
			'SortOrder' => 'Int'
		),
	);
	
	/**
	 * Gets fields used in the cms
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$teasersConf = new GridFieldConfig_RelationEditor();
		$teasersConf->addComponent(new GridFieldSortableRows('SortOrder'));
		
		$fields->addFieldToTab('Root.ContentTeasers', new GridField('ContentTeasers', 'ContentTeaser', $this->ContentTeasers(), $teasersConf));
		
		$fields->addFieldsToTab('Root.Main', array(
			CheckboxField::create('InquiryButton')->setTitle('Stran vsebuje gumb, ki pelje na obrazec za povpraševanje'),
			TextField::create('Button')->setTitle('Napis na gumbu za povpraševanje'),
			TextareaField::create('ButtonText')->setTitle('Besedilo nad gumbom za povpraševanje')
		), 'Metadata');
		
		return $fields;
	}
	
	function getSettingsFields() {
		$fields = parent::getSettingsFields();
		$fields->addFieldToTab("Root.Settings", new CheckBoxField('HideSlideShow', 'Hide slide show?'), 'ShowInSearch');
		return $fields;
	}
		
	public function ContentTeasers() {
		return $this->getManyManyComponents('ContentTeasers')->sort('SortOrder');
	}
}

class ContentPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
	);

	public function init() {
		parent::init();
	}

}
