<?php

class InquiryPage extends Page {

	private static $icon = 'mysite/images/cms_icons/inquiry_page.png';	
	private static $description = 'Inquiry page';

	private static $db = array(
	);

	/**
	 * Gets fields used in the cms
	 * @return {FieldSet} Fields to be used
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();	
		
		$fields->removeByName(array(
			'YoutubeLink',
			'YoutubeTitle'
		));
		
		return $fields;
	}

}

class InquiryPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
		'InquiryForm'
	);

	public function init() {
		parent::init();
	}
	
	public function InquiryForm() {
		$requestVars = $this->getRequest()->getVars();
		
		// fields
		$fields = new FieldList();
		
		if (!isset($requestVars['buy'])) {
			$fieldsBooking = array(
				DropdownField::create('CampID')
					->setTitle(_t('InquiryPage.FORM_CAMP_TITLE', 'Camp'))
					->setSource($this->Camps()->map())
					->setEmptyString('-'),
				TextField::create('Arrival')
					->addExtraClass('arrival')
					->setAttribute('placeholder', _t('InquiryPage.FORM_ARRIVAL_TITLE', 'Arrival'))
					->setTitle(_t('InquiryPage.FORM_ARRIVAL_TITLE', 'Arrival')),
				TextField::create('Departure')
					->addExtraClass('departure')
					->setAttribute('placeholder', _t('InquiryPage.FORM_DEPARTURE_TITLE', 'Departure'))
					->setTitle(_t('InquiryPage.FORM_DEPARTURE_TITLE', 'Departure')),
				DropdownField::create('Guests')
					->setTitle(_t('InquiryPage.FORM_GUESTS_TITLE', 'Guests'))
					->setSource(array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6))
					->setEmptyString('-'),
					
				HiddenField::create('HouseID')
			);
			
			$fields->merge($fieldsBooking);
		};
		
		$fieldsGeneral = array(
			TextField::create('FirstName')
				->setAttribute('placeholder', _t('InquiryPage.FORM_FIRSTNAME_TITLE', 'Firstname'))
				->setTitle(_t('InquiryPage.FORM_FIRSTNAME_TITLE', 'Firstname')),
			TextField::create('Surname')
				->setAttribute('placeholder', _t('InquiryPage.FORM_SURNAME_TITLE', 'Surname'))
				->setTitle(_t('InquiryPage.FORM_SURNAME_TITLE', 'Surname')),
			EmailField::create('Email')
				->setAttribute('placeholder', _t('InquiryPage.FORM_EMAIL_TITLE', 'Email'))
				->setTitle(_t('InquiryPage.FORM_EMAIL_TITLE', 'Email')),
			TextField::create('Phone')
				->setAttribute('placeholder', _t('InquiryPage.FORM_PHONE_TITLE', 'Phone'))
				->setTitle(_t('InquiryPage.FORM_PHONE_TITLE', 'Phone')),
			TextareaField::create('Message')
				->setAttribute('placeholder', _t('InquiryPage.FORM_MESSAGE_TITLE', 'Message'))
				->setTitle(_t('InquiryPage.FORM_MESSAGE_TITLE', 'Message'))
		);
		
		$fields->merge($fieldsGeneral);
	
		// actions
		$actions = new FieldList(FormAction::create('submit')->setTitle(_t('InquriyForm.SUBMIT', 'Preveri')));
	
		$required = new RequiredFields(array('Email'));
		// form
		$form = new Form($this, 'InquiryForm', $fields, $actions, $required);
		$form->addExtraClass('inquiryform');
		
		$form->loadDataFrom($requestVars);
		
		return $form;
	}
	
	public function submit(Array $data, Form $form) {
		$inquiry = new Inquiry();	
			
		$form->saveInto($inquiry);
		
		//dates 
		$arr = strtotime($data['Arrival']);
		$dep = strtotime($data['Departure']);
		
		$inquiry->Arrival = date('Y-m-d', $arr);
		$inquiry->Departure = date('Y-m-d', $dep);
		
		$inquiry->write();
		
		$conf = singleton('SiteTree')->getSiteConfig();
		//send main email to main mail, define from, to and subject
		$mailMain = new Email();
		$mailMain->setFrom($conf->MainMail);
		//$mail->setTo($inquiry->Email);
		$mailMain->setSubject('Povpraševanje');
		$mailMain->setTo($conf->MainMail);
		$mailMain->setTemplate('InquiryMainEmail');
		
		$tmplData = new ArrayData(
			array(
				'Inquiry' => $inquiry,
			)
		);
		
		$mailMain->populateTemplate($tmplData);
		
		$mailMain->send();
		
		//send email to user email, define from, to and subject
		$mailUser = new Email();
		$mailUser->setFrom($conf->MainMail);
		$mailUser->setTo($inquiry->Email);
		$mailUser->setSubject('Big Berry');
		$mailUser->setTemplate('InquiryUserEmail');
		
		$mailUser->populateTemplate($tmplData);
		
		$mailUser->send();
		
		Session::set('Success', true);
		
		// prijava na newsletter
		NewsletterPage_Controller::sign_up($data['FirstName'], $data['Surname'], $data['Email'], NewsletterPage_Controller::get_list(), array("Test 1","Test 2"));
		
		return $this->redirect($this->Link());
				
	}
	
	public function Success() {
		$success = Session::get('Success');
		Session::clear('Success');
		
		if ($success) return true;
		
		return false;
	}

}
