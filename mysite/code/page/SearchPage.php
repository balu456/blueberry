<?php

class SearchPage extends Page {
	
	private static $icon = 'mysite/images/cms_icons/search.png';
	
	private static $description = 'Search';

	private static $db = array(
		'GoogleCSECode' => 'Varchar(100)',
		'itemsPerPage' => 'Int',
		'domainFilter' => 'Varchar(255)'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldsToTab('Root.Main',
			array(
				HeaderField::create('srch')->setTitle('Nastavitve iskalnika'),
				TextField::create('GoogleCSECode')->setTitle('CSE ključ za iskalnik'),
				TextField::create('domainFilter')->setTitle('Filter domen'),
				NumericField::create('itemsPerPage')->setTitle('Število zadetkov na stran'),
			),
			'Metadata'
		);
		
		$fields->removeByName(array('Content'));
		
		return $fields;
	}
	
}


class SearchPage_Controller extends Page_Controller {
	
	private static $allowed_actions = array(
		'SearchForm',
		'results',
	);
	
	public function init() {
		parent::init();
	}
	
	/**
	 * Search form
	 * @return Form
	 */
	function SearchForm() {
		$fields = new FieldList(
			TextField::create('q')->setValue(urldecode($this->request->getVar('q'))),
			HiddenField::create('p')->setValue(1),

			// http://haacked.com/archive/2007/09/11/honeypot-captcha.aspx
			TextField::create('hc666')->setRightTitle('If you see this, leave this field empty!')->addExtraClass('hc666')
		);
		
		$actions = new FieldList(
			new FormAction('search', _t('Search.SUBMIT', 'Search'))
		);
		
		$form = new Form($this, 'SearchForm', $fields, $actions);
		$form->disableSecurityToken();

		return $form;
	}
	
	/**
	 * Build parameter string from array of google cse attributes
	 * @param array $parameters
	 * @return string|boolean
	 */
	function buildParamString($parameters) {
		$urlString = '';
	
		foreach ($parameters as $key => $value) {
			$urlString .= urlencode($key) . '=' . urlencode($value) . '&';
		}
	
		if (trim($urlString) != '') {
			$urlString = preg_replace('/&$/', '', $urlString);
			return $urlString;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Build results paging
	 * @param int $totalResults
	 * @param string $query
	 * @param int $page
	 * @return ArrayList
	 */
	function getPagingForResults($totalResults, $query, $page) {
		$pagingNum = 10; // number of pages in pagination
		
		$paging = new ArrayList();
		if ($totalResults > 1) {
			$maxPage = ceil($totalResults / $this->itemsPerPage);
			
			$startPage = $page;
			$endPage = $page;
			
			if ($pagingNum > $maxPage) {
				$pagingNum = $maxPage;
			}
			
			$canExpand = true;
			while ($canExpand) {
				$canExpand = false;
				
				if ($startPage > 1) {
					$startPage--;
					$canExpand = true;
				}				
				if ($endPage - $startPage == $pagingNum - 1) break;
				
				if ($endPage < $maxPage) {
					$endPage++;
					$canExpand = true;
				}
				if ($endPage - $startPage == $pagingNum - 1) break;
			}
			
			if ($page > 1) {
				$item = array(
					'Link' => $this->AbsoluteLink() . 'results?q=' . $query . '&p=' . ($page - 1),
					'PageNum' => 'Prev',
					'Current' => false,
					'Firsty' => false
				);
				$paging->push(new ArrayData($item));
			}
			
			for ($pageNum = $startPage; $pageNum <= $endPage; $pageNum++) {
				$item = array(
					'Link' => $this->AbsoluteLink() . 'results?q=' . $query . '&p=' . $pageNum,
					'PageNum' => $pageNum,
					'Current' => ($pageNum == $page) ? true : false,
					'Firsty' => ($pageNum == $startPage) ? true : false
				);
				$paging->push(new ArrayData($item));
			}
			
			if ($page < $maxPage) {
				$item = array(
					'Link' => $this->AbsoluteLink() . 'results?q=' . $query . '&p=' . ($page + 1),
					'PageNum' => 'Next',
					'Current' => false,
					'Firsty' => false								
				);
				$paging->push(new ArrayData($item));
			}			
		}

		return $paging;
	}	

	/**
	 * Parse XML results into ArrayList
	 * @param SimpleXMLElement $xml
	 * @return ArrayList
	 */
	function parseGoogleSearchResults($xml) {
		$items = array();
	
		$attr['Title'] = $xml->xpath('/GSP/RES/R/T');
		$attr['Url'] = $xml->xpath('/GSP/RES/R/U');
		$attr['Desc'] = $xml->xpath('/GSP/RES/R/S');
	
		foreach ($attr as $key => $attribute) {
			$i = 0;
			foreach ($attribute as $element) {
				$items[$i][$key] = (string)$element;
				$i++;
			}
		}
		
		$results = new ArrayList();
		foreach ($items as $item) {
			$results->push(new ArrayData($item));
		}
	
		return $results;
	}
	
	/**
	 * Get result count
	 * @param SimpleXMLElement $xml
	 * @return int
	 */
	function getResultCount($xml) {
		$totalResults = 0;
		$count = $xml->xpath('/GSP/RES/M');
		
		if (isset($count[0])) {
			$totalResults = (int)$count[0];
		}
		
		return $totalResults;
	}	
	
	/**
	 * Search and show results
	 * @param SS_HTTPRequest $request
	 */
	function results($request) {		
		// if search is empty or shorter than 2 chars, we return false and skip actual search
		if (strlen(trim($request->getVar('q'))) <= 2) {
			$data = array(
				'SearchResults' => false,
				'Paging' => false,
				'SearchCount' => false,
				'SearchError' => true
			);
			return $this->Customise($data)->renderWith(array('Search', 'Page', 'ContentController'));
		}
		
		if (!$this->itemsPerPage) return $this->httpError(500, 'Please set itemsPerPage');
		if (!$this->GoogleCSECode) return $this->httpError(500, 'Please set GoogleCSECode');
		
		$start = ($request->getVar('p') && is_numeric($request->getVar('p'))) ? $request->getVar('p') : '1';
		
		$searchParams = array();
		$searchParams['client'] = 'google-csbe';
		$searchParams['output'] = 'xml_no_dtd';
		$searchParams['num'] = $this->itemsPerPage;
		$searchParams['cx'] = $this->GoogleCSECode;
		$searchParams['start'] = $start - 1;
		$searchParams['q'] = urldecode($request->getVar('q'));
		
		if ($this->domainFilter) $searchParams['as_sitesearch'] = $this->domainFilter;
		
		$results = new ArrayList();
		$paging = new ArrayList();
		$count = 0;
		
		if (!empty($searchParams['q']) && $paramString = $this->buildParamString($searchParams)) {
			if ($xml = file_get_contents('http://www.google.com/cse?' . $paramString)) {
				if ($simpleXml = simplexml_load_string($xml)) {
					$results = $this->parseGoogleSearchResults($simpleXml);
					$count = $this->getResultCount($simpleXml);
					$paging = $this->getPagingForResults($count, urlencode($searchParams['q']), $start);	
				}
			}		
		}
		
		$data = array(
			'SearchResults' => $results,
			'Paging' => $paging,
			'SearchCount' => ($count > 0) ? true : false,
			'SearchError' => false
		);
		
		return $this->Customise($data)->renderWith(array('SearchPage', 'Page', 'ContentController'));	
	}
	
	/**
	* Process form and redirect to results
	* @param array $data
	* @param Form $form
	*/
	function search($data, $form) {
		if (!empty($data['hc666'])) return false;
		
		return $this->redirect($this->Link('results') . '?p=' . $data['p'] . '&q=' . urlencode($data['q']));
	}
	
}