<?php

class Designer extends DataObjectAsPage {
	
	private static $singular_name = 'Oblikovalec';
	private static $plural_name = 'Oblikovalci';
	
	private static $listing_page_class = 'DesignerList';
	
	public static $modelAdminClass = 'ElementsAdmin';
	
	private static $db = array(
		'Description'	=> 'HTMLText',
		'SortOrder'		=> 'Int'
	);
	
	private static $has_one = array(
		'Image' => 'Image',
	);
	
	private static $default_sort = 'SortOrder';
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName('SortOrder');
		
		return $fields;
	}
	

	function validate() {
		$result = parent::validate();
	
		if($this->exists()) {
			// Check if SKU exists
			$url_segment_filter = array(
					'URLSegment'		=> $this->URLSegment,
			);
			$url_segment_filter['ID:not'] = $this->ID;
				
			$existing = DataObjectAsPage::get()->filter($url_segment_filter)->first();
				
			if($existing) {
				$result->error(_t('DataObjectAsPage.Validate_URLSegment', 'Vnos s tem URL naslovom že obstaja!'), 'URLSegment');
			}
		}
		return $result;
	}
	
}