<?php

class SocialPost extends DataObjectAsPage {
	
	//private static $singular_name = 'Aktualno iz sveta';
	//private static $plural_name = 'Aktualno iz sveta';
	private static $singular_name = 'Social Post';
	private static $plural_name = 'Social Posts';
	
	private static $listing_page_class = 'NewsListPage';
	
	public static $modelAdminClass = 'ElementsAdmin';
	
	private static $db = array(
		//'Start'			=> 'SS_DateTime',
		//'End'			=> 'SS_DateTime',
		//'Intro'			=> 'HTMLText',
		//'YoutubeLink'	=> 'Varchar(255)',
		//'YoutubeTitle'	=> 'Varchar(255)'
		'Source'	=> 'Varchar(255)'
	);
	
	private static $many_many = array(
		'Images'	=> 'Image',
		'Files'		=> 'File',
		'Tags'		=> 'TagFromSocialPost'
	);
	
	private static $many_many_extraFields = array(
		'Images' => array(
			'SortOrder' => 'Int'
		),
		'Files' => array(
			'SortOrder' => 'Int'
		),
	);
	
	private static $default_sort = 'LastEdited DESC';
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName(array(
			'From',
			'To'
		));
		
		$fields->addFieldToTab('Root.Main', TextField::create('Source')->setTitle('Source ie FB, TW'));
		/*
		$fields->addFieldsToTab('Root.Main', array(
			TextareaField::create('YoutubeLink')->setTitle('Youtube celoten link'),
			TextField::create('YoutubeTitle')->setTitle('Youtube naslov na gumbu')
		));
		
		$fields->replaceField('Images', SortableUploadField::create('Images'));
		$fields->replaceField('Files', SortableUploadField::create('Files'));
		
		$dateFieldStart = DatetimeField::create('Start');
		$dateFieldStart->getDateField()->setConfig('showcalendar', true)->setTitle('Datum od'); // datepicker
		$dateFieldStart->setTimeField(TimePickerField::create('Start[time]', 'Ura od')); // timepicker for time field in Start
		 
		$dateFieldEnd = DatetimeField::create('End');
		$dateFieldEnd->getDateField()->setConfig('showcalendar', true)->setTitle('Datum do'); // datepicker
		$dateFieldEnd->setTimeField(TimePickerField::create('End[time]', 'Ura do')); // timepicker for time field in End
		 
		$fields->addFieldsToTab('Root.Main', array(
			$dateFieldStart,
			$dateFieldEnd
		));
		*/
		
		$fields->replaceField('Tags', new GridField('Tags', 'Tags', $this->Tags(), new GridFieldConfig_RelationEditor()));		
		
		return $fields;
	}
	
	public function Images() {
		return $this->getManyManyComponents('Images')->sort('SortOrder');
	}
	
	public function Files() {
		return $this->getManyManyComponents('Files')->sort('SortOrder');
	}
	
	/**
	 * Get the listing page to view this Event on (used in Link functions below)
	 */
	public function getListingPage(){
	
		$listingClass = $this->stat('listing_page_class');
		$controllerClass =  $listingClass . "_Controller";

		$listingPage = $listingClass::get()->filter('ClassName', $listingClass)->First();
	
		return $listingPage;
	}
	

	function validate() {
		$result = parent::validate();
	
		if($this->exists()) {
			// Check if SKU exists
			$url_segment_filter = array(
					'URLSegment'		=> $this->URLSegment,
			);
			$url_segment_filter['ID:not'] = $this->ID;
				
			$existing = DataObjectAsPage::get()->filter($url_segment_filter)->first();
				
			if($existing) {
				$result->error(_t('DataObjectAsPage.Validate_URLSegment', 'Vnos s tem URL naslovom že obstaja!'), 'URLSegment');
			}
		}
		return $result;
	}
	
}