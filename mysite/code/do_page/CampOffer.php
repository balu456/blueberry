<?php

class CampOffer extends DataObjectAsPage {
	
	private static $singular_name = 'Camp offer';
	private static $plural_name = 'Camp offers';	
	
	private static $db = array(
		'Description'	=> 'Varchar(150)',
		'YoutubeLink'	=> 'Varchar(255)',
		'YoutubeTitle'	=> 'Varchar(255)',
		'ExternalLink'	=> 'Varchar(255)',
		'ExternalLinkTitle'	=> 'Varchar(255)',
		'SortOrder'		=> 'Int',
	);
	
	private static $has_one = array(
		'CampInfo'	=> 'CampInfo',
	);
	
	private static $many_many = array(
		'Images' 	=> 'Image',
		'Files'		=> 'File'
	);
	
	private static $many_many_extraFields = array(
		'Images' => array(
			'SortOrder' => 'Int'
		),
		'Files' => array(
			'SortOrder' => 'Int'
		),
	);
	
	public function Images() {
		return $this->getManyManyComponents('Images')->sort('SortOrder');
	}
	
	public function Files() {
		return $this->getManyManyComponents('Files')->sort('SortOrder');
	}
	
	private static $default_sort = 'SortOrder';
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName('SortOrder');
		$fields->removeByName('CampInfoID');
		$fields->removeByName('Translations');
		
		$fields->replaceField('Description', TextareaField::create('Description'));
		$fields->replaceField('Images', SortableUploadField::create('Images'));
		$fields->replaceField('Files', SortableUploadField::create('Files'));
		
		$fields->addFieldsToTab('Root.Main', array(
			TextareaField::create('YoutubeLink')->setTitle('Youtube celoten link'),
			TextField::create('YoutubeTitle')->setTitle('Youtube naslov na gumbu'),
			TextField::create('ExternalLink')->setTitle('External link'),
			TextField::create('ExternalLinkTitle')->setTitle('External link title')
		));
	
		return $fields;
	}
	
	/**
	 * Generate the link to this DataObject Item page
	 */
	public function Link($action = null) {
		//Hack for search results
		if($item = DataObjectAsPage::get()->byID($this->ID))
		{ 
			return Controller::join_links(Controller::curr()->Link(), /*'show', */$item->URLSegment, $action) . '/';
		}
	}


	function validate() {
		$result = parent::validate();
	
		if($this->exists()) {
			// Check if SKU exists
			$url_segment_filter = array(
					'URLSegment'		=> $this->URLSegment,
			);
			$url_segment_filter['ID:not'] = $this->ID;
				
			$existing = DataObjectAsPage::get()->filter($url_segment_filter)->first();
				
			if($existing) {
				$result->error(_t('DataObjectAsPage.Validate_URLSegment', 'Vnos s tem URL naslovom že obstaja!'), 'URLSegment');
			}
		}
		return $result;
	}
}