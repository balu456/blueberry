<?php

class CampHouse extends DataObjectAsPage {
	
	private static $singular_name = 'Camp house';
	private static $plural_name = 'Camp houses';	
	
	private static $db = array(
		'Persons'		=> 'Int',
		'Dimensions'	=> 'Int',
		'Rooms'			=> 'Varchar(255)',
		'Description'	=> 'Varchar(255)',
		'Price'			=> 'Decimal',
		'Position'		=> 'Varchar(30)',
		'InMaterials'	=> 'Varchar(255)',
		'OutMaterials'	=> 'Varchar(255)',
		'PriceDetails'	=> 'HTMLText',
		'SortOrder'		=> 'Int',
	);
	
	private static $has_one = array(
		'CampHouses'	=> 'CampHouses',
		'Image'			=> 'Image',
		'PositionImage'	=> 'Image',
		'Tloris'			=> 'Image',
	);
	
	private static $many_many = array(
		'Images' 			=> 'Image',
		'Files'				=> 'File',
		'DescriptionFiles'	=> 'File'
	);
	
	private static $many_many_extraFields = array(
		'Images' => array(
			'SortOrder' => 'Int'
		),
		'Files' => array(
			'SortOrder' => 'Int'
		),
		'DescriptionFiles' => array(
			'SortOrder' => 'Int'
		),
	);
	
	private static $summary_fields = array(
		'Persons'		=> 'Persons',
		'Dimensions'	=> 'Dimensions',
		'Price'			=> 'Price',
		'Position'		=> 'Position'
	);
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeByName('CampHousesID');
		$fields->removeByName('Translations');
		
		$persons = array(
			'2' => '2',
			'4' => '4',
			'6' => '6',
			'10' => '10'
		);
		$fields->replaceField('Persons', DropdownField::create('Persons')->setSource($persons)->setEmptyString('-'));
		
		$fields->replaceField('Rooms', TextareaField::create('Rooms'));
		$fields->replaceField('Description', TextareaField::create('Description'));
		$fields->replaceField('InMaterials', TextareaField::create('InMaterials'));
		$fields->replaceField('OutMaterials', TextareaField::create('OutMaterials'));
		$fields->replaceField('Images', SortableUploadField::create('Images'));
		$fields->replaceField('Files', SortableUploadField::create('Files'));
		$fields->replaceField('DescriptionFiles', SortableUploadField::create('DescriptionFiles'));
	
		return $fields;
	}
	
	
	public function Images() {
		return $this->getManyManyComponents('Images')->sort('SortOrder');
	}
	
	public function Files() {
		return $this->getManyManyComponents('Files')->sort('SortOrder');
	}
	
	public function DescriptionFiles() {
		return $this->getManyManyComponents('DescriptionFiles')->sort('SortOrder');
	}
	
	public function NicePrice() {
		if (!$this->Price) return false;
		
		$locale = localeconv();
		
		if (fmod($this->Price, 1) == 0) return intval($this->Price);
		
		return number_format($this->Price, 2, $locale['decimal_point'], $locale['thousands_sep']);
	}
	
	/**
	 * Generate the link to this DataObject Item page
	 */
	public function Link($action = null) {
		//Hack for search results
		if($item = DataObjectAsPage::get()->byID($this->ID))
		{
			return Controller::join_links(Controller::curr()->Link(), /*'show', */$item->URLSegment, $action) . '/';
		}
	}

	/**
	 * Return parent of current controller if it is Camp
	 */
	public function CampParent() {
		//Hack for search results
		if($item = DataObjectAsPage::get()->byID($this->ID))
		{
			$controller = Controller::curr()->Parent();
			if ($controller->ClassName == 'Camp')
				return $controller;
			
			return false;
		}
	}
	
	public function CountImages() {
		$countimgs = 0;
		
		if ($this->ImageID > 0 && $this->PositionImageID > 0) {
			$countimgs = 2 + $this->Images()->count();
			
			return $countimgs;
		}
		
		if ($this->ImageID > 0 || $this->PositionImageID > 0) {
			$countimgs = 1 + $this->Images()->count();
			
			return $countimgs;
		}
		
		return false;
	}
	

	function validate() {
		$result = parent::validate();
	
		if($this->exists()) {
			// Check if SKU exists
			$url_segment_filter = array(
					'URLSegment'		=> $this->URLSegment,
			);
			$url_segment_filter['ID:not'] = $this->ID;
				
			$existing = DataObjectAsPage::get()->filter($url_segment_filter)->first();
				
			if($existing) {
				$result->error(_t('DataObjectAsPage.Validate_URLSegment', 'Vnos s tem URL naslovom že obstaja!'), 'URLSegment');
			}
		}
		return $result;
	}
	
}