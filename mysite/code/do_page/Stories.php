<?php

class Stories extends DataObjectAsPage {
	
	private static $singular_name = 'Story';
	private static $plural_name = 'Stories';
	
	private static $listing_page_class = 'NewsList';
	
	public static $modelAdminClass = 'ElementsAdmin';
	
	private static $db = array(
		'Start'			=> 'SS_DateTime',
		'End'			=> 'SS_DateTime',
		'Intro'			=> 'HTMLText',
		'YoutubeLink'	=> 'Varchar(255)',
		'YoutubeTitle'	=> 'Varchar(255)'
	);
    
    private static $has_one = array(
        'ParentTag' => 'TagStories',
        'Tag' => 'SubTagStories',
    );
	
	private static $many_many = array(
		'Images'	=> 'Image',
		'Files'		=> 'File',
	);
	
	private static $many_many_extraFields = array(
		'Images' => array(
			'SortOrder' => 'Int'
		),
		'Files' => array(
			'SortOrder' => 'Int'
		),
	);
	
	private static $default_sort = 'LastEdited DESC';
	
	/**
	 * Gets fields used in the cms
	 *
	 * @return FieldList
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$Tags = TagStories::get();
        $TagField = DropdownField::create('ParentTagID', 'Choose a Parent Tag', $Tags->map('ID', 'Title')->toArray());
        $fields->addFieldsToTab('Root.Main', array(
            $TagField
        ), 'Intro');
        if($this->ParentTagID != 0){
            $subTags = SubTagStories::get()->filter('TagStoriesID', $this->ParentTagID);               
            $subTagField = DropdownField::create('TagID', 'Choose a Sub Tag', $subTags->map('ID', 'CustomTitle')->toArray()); 
            $fields->addFieldsToTab('Root.Main', array(
                $subTagField
            ), 'Intro');
        }
                        
		$fields->removeByName(array(
			'From',
			'To'
		));
		
		$fields->addFieldsToTab('Root.Main', array(
			TextareaField::create('YoutubeLink')->setTitle('Youtube celoten link'),
			TextField::create('YoutubeTitle')->setTitle('Youtube naslov na gumbu')
		));
		
		$fields->replaceField('Images', SortableUploadField::create('Images'));
		$fields->replaceField('Files', SortableUploadField::create('Files'));
		
		$dateFieldStart = DatetimeField::create('Start');
		$dateFieldStart->getDateField()->setConfig('showcalendar', true)->setTitle('Datum od'); // datepicker
		$dateFieldStart->setTimeField(TimePickerField::create('Start[time]', 'Ura od')); // timepicker for time field in Start
		 
		$dateFieldEnd = DatetimeField::create('End');
		$dateFieldEnd->getDateField()->setConfig('showcalendar', true)->setTitle('Datum do'); // datepicker
		$dateFieldEnd->setTimeField(TimePickerField::create('End[time]', 'Ura do')); // timepicker for time field in End
		 
		$fields->addFieldsToTab('Root.Main', array(
			$dateFieldStart,
			$dateFieldEnd
		));
		
		
		
		return $fields;
	}
	
	public function Images() {
		return $this->getManyManyComponents('Images')->sort('SortOrder');
	}
	
	public function Files() {
		return $this->getManyManyComponents('Files')->sort('SortOrder');
	}
	
	/**
	 * Get the listing page to view this Event on (used in Link functions below)
	 */
	public function getListingPage(){
	
		$listingClass = $this->stat('listing_page_class');
		$controllerClass =  $listingClass . "_Controller";

		$listingPage = $listingClass::get()->filter('ClassName', $listingClass)->First();
	
		return $listingPage;
	}
	

	function validate() {
		$result = parent::validate();

		if($this->exists()) {
			// Check if SKU exists
			$url_segment_filter = array(
				'URLSegment'		=> $this->URLSegment,
			);
			$url_segment_filter['ID:not'] = $this->ID;
			
			$existing = DataObjectAsPage::get()->filter($url_segment_filter)->first();
			
			if($existing) {
				$result->error(_t('DataObjectAsPage.Validate_URLSegment', 'Vnos s tem URL naslovom že obstaja!'), 'URLSegment');
			}
		}
		return $result;
	}
    
    function summaryFields(){
        return array(
            'Title' => 'Title',
            'URLSegment' => 'URL',
            'Locale' => 'Locale',
            'Tag.CustomTitle' => 'Tag'
        );
    }
	
}