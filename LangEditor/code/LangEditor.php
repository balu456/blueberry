<?php

class LangEditor extends LeftAndMain {

	static $menu_title = "I18n Editor";
	static $menu_icon = "LangEditor/images/edit-language.png";
	
	static $url_segment = "langeditor";
	
	static $menu_priority = -0.6;
	
	static $allowed_actions = array (
		'TranslationForm',
		'CreateTranslationForm',
		'CopyVariablesForm',
		'show',
		'updatemodules',
		'updatelanguages',
		'updatecreateform',
		'doCreate',
		'doSave',
		
		'cleari18nCache',
		
		'translateAndUpdateAllLocales',
	);
	
	static $exclude_modules = array();
	static $exclude_locales = array();
	
	static $currentLocale = "";
	static $currentModule = "";
	static $currentIsSelected = "";
	
	/**
	 * Subsite addon
	 * @return boolean
	 */
	public function subsiteCMSShowInMenu() {
		return true;
	}
	
	/**
	 * Get modules lanugage directory
	 * @param unknown $module
	 * @return string
	 */
	public static function get_lang_dir($module) {
		return BASE_PATH.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR."lang";
	}
	
	/**
	 * Get language file from $module for $lang 
	 * @param unknown $module
	 * @param unknown $lang
	 * @return string
	 */
	public static function get_lang_file($module, $lang) {
		$file = self::get_lang_dir($module).DIRECTORY_SEPARATOR."{$lang}.yml";
		return $file;
	}
	
	/**
	 * Load existing languages from module
	 */
	public static function check_module_existing_lang() {
		if (!is_file(self::get_lang_file(self::$currentModule, self::$currentLocale))) {
			if( ($langs = self::getLanguages()) && $langs->Count() ) {
				self::$currentLocale = $langs->First()->Locale;
			}
		}
	}
	
	/**
	 * Returns the "short" language name from a locale,
	 * e.g. "en_US" would return "en".
	 *
	 * @param string $locale E.g. "en_US"
	 * @return string Short language code, e.g. "en"
	 */
	public static function get_lang_from_locale($locale) {
		if($str = i18n::get_lang_from_locale($locale)) {
			if(stristr($str,"_") !== false) {
				$parts = explode("_", $str);
				return reset($parts);
			}
			return $str;
		}
		return $locale;
	}
	
	
	public function init() {
		parent::init();
		
		$r = $this->request; //Controller::curr()->getRequest();
		self::$currentLocale = i18n::get_locale();
		self::$currentModule = project();
		if($r && $r->param('ID') && $r->param('OtherID')) {
			self::$currentLocale = $r->param('ID');
			self::$currentModule = str_replace('$', DIRECTORY_SEPARATOR, $r->param('OtherID'));
		}
		self::check_module_existing_lang();
		
	}
	
	
	public function TranslationForm($id = null, $fields = null) {
		$form = new Form(
			$this, 
			"TranslationForm", 
			new FieldList(), 
			new FieldList(
				new FormAction('doSave', _t('LangEditor.SAVE','Save'))
			)
		);
		$form->addExtraClass('cms-edit-form');
		$form->addExtraClass('center ' . $this->BaseCSSClasses());
		$form->setAttribute('data-language', self::$currentLocale);
		$form->setTemplate($this->getTemplatesWithSuffix('_TranslationForm'));
		$form->setAttribute('data-pjax-fragment', 'CurrentForm');
		$form->Namespaces = $this->Namespaces;
		$form->SelectedModule = self::$currentModule;
		$form->unsetValidator();
		return $form;
	}
	
	
	public function loadTranslationData() {
		$namespaces = new ArrayList();
		$lang_file = self::get_lang_file(self::$currentModule, self::$currentLocale);
				
		// Use the Zend copy of this script to prevent class conflicts when RailsYaml is included
		require_once 'thirdparty/zend_translate_railsyaml/library/Translate/Adapter/thirdparty/sfYaml/lib/sfYaml.php';
		
		$temp_lang = sfYaml::load($lang_file);
		
		$map = array();
		if(is_array($temp_lang) && isset($temp_lang[self::$currentLocale])) {
			foreach($temp_lang[self::$currentLocale] as $namespace => $array_of_entities) {
				$map[$namespace] = $namespace;
				$entities = new ArrayList();
				if(is_array($array_of_entities)) {
					foreach($array_of_entities as $entity => $str) {
						if (is_array($str)) {
							$str = $str[0];
						}
						$entities->push(new ArrayData(array(
							'Entity' => $entity,
							'IsCMS' => in_array(strtolower($entity), array('pluralname', 'singularname', 'description')),
							'EntityField' => new TextField("t[".self::$currentLocale."][$namespace][$entity]","",stripslashes($str)),
							'Namespace' => $namespace
						)));
					}
				}
				
				$namespaces->push(new ArrayData(array(
					'Namespace' => $namespace,
					'Entities' => $entities
				)));
			}
		}
		
		$this->Namespaces = $namespaces;
		$this->NamespaceDropdownOptions = $map;
		$this->DownloadFile = self::$currentModule."/lang/".self::$currentLocale.".yml";//;
	}
	
	
	public function Breadcrumbs($unlinked = false) {
		$items = parent::Breadcrumbs(true);
		$items->push(
			new ArrayData(array(
				'Title' => _t('LangEditor.EDITING','Editing').": ".self::$currentModule.", ".self::$currentLocale,
				'Link' => false
			))
		);
		return $items;
	}
	
	
	public static function getLanguages() {
		$langs = new ArrayList();
		if($files = glob(self::get_lang_dir(self::$currentModule).DIRECTORY_SEPARATOR."*.yml")) {
			foreach($files as $file) {
				$label = basename($file,".yml");
				if (!in_array($label, self::$exclude_locales)) {
					$langs->push(new ArrayData(array(
						'Link' => Director::baseURL().'admin/'.self::$url_segment.'/show/'.$label.'/'.str_replace(DIRECTORY_SEPARATOR, '$', self::$currentModule),
						'Locale' => $label,
						'Name' => i18n::get_language_name(self::get_lang_from_locale($label)),
						'Current' => self::$currentIsSelected && $label == self::$currentLocale ? true : false
					)));
				}
			}
		}
		return $langs;
	}
	
	
	public function getModules() {
		$modules = new ArrayList();
		
		$folders = scandir(BASE_PATH);
		$themeFolders = array();
		
		foreach($folders as $index => $folder){
			if($folder != 'themes') continue;
			else {
				$themes = scandir(BASE_PATH.DIRECTORY_SEPARATOR."themes");
				if(count($themes)){
					foreach($themes as $theme) {
						if(is_dir(BASE_PATH.DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$theme) && substr($theme,0,1) != '.' && is_dir(BASE_PATH.DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$theme.DIRECTORY_SEPARATOR."templates")){
							$themeFolders[] = 'themes'.DIRECTORY_SEPARATOR.$theme;
						}
					}
				}
				$themesInd = $index;
			}
		}
		if(isset($themesInd)) {
			unset($folders[$themesInd]);
		}
		$folders = array_merge($folders, $themeFolders);
		natcasesort($folders);
				
		foreach($folders as $folder) {
			// Only search for calls in folder with a _config.php file  
			$isValidModuleFolder = (
				!in_array($folder, self::$exclude_modules)
				&& substr($folder,0,1) != '.'
				&& is_dir(BASE_PATH.DIRECTORY_SEPARATOR."$folder")
				&& (
					(
					is_file(BASE_PATH.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR."_config.php") 
					|| 
					is_file(BASE_PATH.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR."_config".DIRECTORY_SEPARATOR."config.yml")
				)
					&& file_exists(BASE_PATH.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR."lang")
				) || (
					substr($folder,0,7) == ('themes'.DIRECTORY_SEPARATOR)
					&& file_exists(BASE_PATH.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR."lang")
				)
			);
			if(!$isValidModuleFolder) continue;
			
			$modules->push(new ArrayData(array(
				'Link' => $this->Link("show/".self::$currentLocale."/".str_replace(DIRECTORY_SEPARATOR, '$', $folder)),
				'Name' => $folder,
				'Current' => self::$currentIsSelected && $folder == self::$currentModule ? true : false
			)));

		}
		return $modules;
	}
	
	
	public function NamespaceDropdown() {
		if(!$this->NamespaceDropdownOptions) {
			$this->loadTranslationData();
		}
		if($this->NamespaceDropdownOptions) {
			$dropdown = new DropdownField('Namespace', _t('LangEditor.NAMESPACE','Namespace'), $this->NamespaceDropdownOptions);
			$dropdown->setEmptyString('-- '._t('LangEditor.SHOWALLNAMESPACES','Show all namespaces').' --');
			return $dropdown->forTemplate();
		}
		return null;
	}
	
	
	public function CreateTranslationForm() {
		$from_languages = array();
		$to_languages = i18n::get_common_languages();
		if (dataObject::has_extension('SiteTree', 'Translatable')) {
			$common_languages = $to_languages;
			$to_languages = array();
			foreach(Translatable::get_allowed_locales() as $locale) {
				if (!in_array($locale, self::$exclude_locales)) {
					$language = self::get_lang_from_locale($locale);
					$to_languages[$language] = $common_languages[$language];
				}
			}
		}
		if($languages = $this->getLanguages()) {
			foreach($languages as $l) {
				$from_languages[$l->Locale] = $l->Name;
			}
		}
		$f = new Form(
			$this,
			"CreateTranslationForm",
			new FieldList (
				new DropdownField('LanguageFrom',_t('LangEditor.TRANSLATEFROM','From'), $from_languages, self::$currentLocale),
				$d = new DropdownField('LanguageTo',_t('LangEditor.TRANSLATETO','To'),$to_languages),
				new HiddenField('Module', 'Module', self::$currentModule)
			),
			new FieldList (
				new FormAction('doCreate',_t('LangEditor.CREATE','Create'))
			)
		);
		$d->setEmptyString('-- '._t('LangEditor.PLEASESELECT','Please select').' --');
		return $f;
	}
	
	
	public function index($request) {
		self::$currentLocale = i18n::get_locale();
		self::$currentIsSelected = false;
		self::$currentModule = project();
		self::check_module_existing_lang();
		return parent::index($request);
	}
	
	
	public function show($request) {
		self::$currentIsSelected = true;
		return $this;
	}
	
	
	public function doSave($data, $form) {
		if(isset($data['t']) && is_array($data['t'])) {
			
			// Use the Zend copy of this script to prevent class conflicts when RailsYaml is included
			require_once 'thirdparty/zend_translate_railsyaml/library/Translate/Adapter/thirdparty/sfYaml/lib/sfYaml.php';
		
			$new_content = sfYaml::dump($data['t'], 99);
			
			$lang = array_keys($data['t']);
			if ($lang) $lang = $lang[0];
			
			$new_file = self::get_lang_file($data['Module'], $lang);
			if($fh = fopen($new_file, "w")) {
				fwrite($fh, $new_content);
				fclose($fh);
				$message = _t('LangEditor.SAVED','Saved');
			} else {
				$message = "Cannot write language file!";
			}			
			
			return new SS_HTTPResponse($message,200);
		}
	}
	
	
	public function doCreate($data, $form) {
		$message = $this->saveFromTo($data['Module'], $data['LanguageFrom'], $data['LanguageTo'], false);
		
		return new SS_HTTPResponse($message,200);
	}
	
	
	public function cleari18nCache() {
		$res = Zend_Translate::getCache()->clean(Zend_Cache::CLEANING_MODE_ALL);
		
		if($res) return new SS_HTTPResponse(_t('LangEditor.ClearCache_ok', 'i18n cache cleared!'),200);
		return new SS_HTTPResponse(_t('LangEditor.ClearCache_false', 'i18n cache NOT cleared!'),500);
	}
	
	/**
	 * Translate and create/merge to all available
	 * @return SS_HTTPResponse
	 */
	public function translateAndUpdateAllLocales() {
		$module 	= $_GET['module'];
		$srclocale 	= $_GET['locale'];
		
		$all = Translatable::get_allowed_locales();
		$countall = count($all);
		$i = 1;
		foreach($all as $locale) {
			$delete = false;
			if($i == $countall)
				$delete = true;
			
			$toLocale = self::get_lang_from_locale($locale);
			
			$message = $this->saveFromTo($module, $srclocale, $toLocale, $delete);
			
			$i++;
		}
		
		return new SS_HTTPResponse($message,200);
	}
	
	
	private function saveFromTo($module, $fromLocale, $toLocale, $deleteSource = false) {
		$message = _t('LangEditor.CREATED','Created');
		
		self::$currentLocale = $toLocale;
		self::$currentModule = $module;
		
		$to = self::get_lang_file($module, $toLocale);
		$from = self::get_lang_file($module, $fromLocale);
		
		// sanity check
		if(!file_exists($from)) {
			return new SS_HTTPResponse(_t('LangEditor.TOFILEMISSING','The from language does not exist!'),500);
		}
		
		// Use the Zend copy of this script to prevent class conflicts when RailsYaml is included
		require_once 'thirdparty/zend_translate_railsyaml/library/Translate/Adapter/thirdparty/sfYaml/lib/sfYaml.php';
		
		$old_data = sfYaml::load($from);
		
		// get content of source file and replace language
		$new_data = array();
		$new_data[$toLocale] = $old_data[$fromLocale];
		
		// merge data if target file exists
		if (file_exists($to)) {
				
			$message = _t('LangEditor.MERGED','Merged');
				
			$existing_data = sfYaml::load($to);
				
			if(is_array($existing_data) && isset($existing_data[self::$currentLocale])
			&& is_array($new_data) && isset($new_data[self::$currentLocale]))
			{
				$new_data = array_replace_recursive($new_data, $existing_data);
			}
		}
		
		// write new content into target file
		$new_content = sfYaml::dump($new_data, 99);
		$new_file = self::get_lang_file(self::$currentModule, self::$currentLocale);
		if($fh = fopen($new_file, "w")) {
			fwrite($fh, $new_content);
			fclose($fh);
			
			if($deleteSource) {
				if( unlink($from) ) 
					$message .= '<br>Source deleted';
				else
					$message .= '<br>Source not deleted';
			}
		} else {
			$message = "Cannot write language file!";
		}
		
		return $message;
	}
	
	/**
	 * Get first available locale
	 * @return string
	 */
	public function getFirstFreeLocale() {
		$candidates = array(
			'war_PH',
			'wbq_IN',
			'wbr_IN',
			'wo_MR',
			'wo_SN',
			'wtm_IN',
			'xh_ZA',
			'xnr_IN',
			'xog_UG',
			'yap_FM',
			'yo_NG',
			'za_CN',
			'zh_CN',
			'zh_HK',
			'zh_MO',
			'zh_SG',
			'zh_TW',
			'zh_US',
			'zh_cmn',
			'zh_yue',
			'zu_ZA'
		);
		$allowed = Translatable::get_allowed_locales();
		
		foreach($candidates as $locale) {
			if(!in_array($locale, $allowed))
				return $locale;
		}
	}
	
	
	public function getCurrentModule() {
		if(self::$currentIsSelected)
			return self::$currentModule;
		return false;
	}
	
	
	public function getCurrentLocale() {
		if(self::$currentIsSelected)
			return self::$currentLocale;
		return false;
	}
	
	
	/************************ COPY VARIABLES ***********************/
	/**
	 * Copy variables form
	 * @return Form
	 */
	public function CopyVariablesForm() {
	
		$toModules = $this->getModules();
		$toModulesArr = array();
		foreach ($toModules as $m){
			$toModulesArr[$m->Name] = $m->Name;
		}
	
		$AllLocales = Translatable::get_allowed_locales();
		$locales = array_combine($AllLocales,$AllLocales);
	
		$fields = new FieldList(
			LiteralField::create('info', 'Copy variables<br>from <strong>source</strong> module<br>to <strong>destination</strong> module'),
			DropdownField::create('fromModule', 'From module')->setSource($toModulesArr),
			DropdownField::create('defaultLocale', 'Default Locale')->setDescription('for when there is no yml file for every locale')->setSource($locales),
			DropdownField::create('toModule', 'To module')->setSource($toModulesArr)
		);
	
		$actions = new FieldList (
			new FormAction('doCopyVariables',_t('LangEditor.Merge','Merge'))
		);
	
		$f = new Form($this, 'CopyVariablesForm', $fields, $actions);
		return $f;
	}
	
	/**
	 * Copy variables action
	 * @param unknown $data
	 * @param unknown $form
	 * @return SS_HTTPResponse
	 */
	public function doCopyVariables($data, $form) {
		$fromModule = $data['fromModule'];
		$toModule = $data['toModule'];
		$defaultLocale = self::get_lang_from_locale($data['defaultLocale']);
	
		$all = Translatable::get_allowed_locales();
		foreach($all as $locale) {
			$lang = self::get_lang_from_locale($locale);
			$message = $this->saveFromModuleTomodule($fromModule, $lang,$toModule, $lang, $defaultLocale);
		}
	
		return new SS_HTTPResponse($message,200);
	}
	
	/**
	 * Save from one module to another module
	 * @param unknown $fromModule
	 * @param unknown $fromLocale
	 * @param unknown $toModule
	 * @param unknown $toLocale
	 * @param unknown $defaultLocale
	 * @return SS_HTTPResponse|Ambigous <string, mixed, string, unknown>
	 */
	private function saveFromModuleTomodule($fromModule, $fromLocale, $toModule, $toLocale, $defaultLocale) {
		$message = _t('LangEditor.CREATED','Created');
	
		self::$currentLocale = $toLocale;
		self::$currentModule = $toModule;
	
		$to = self::get_lang_file($toModule, $toLocale);
		$from = self::get_lang_file($fromModule, $fromLocale);
	
		if(!file_exists($from)) {
			$from = self::get_lang_file($fromModule, $defaultLocale);
			$fromLocale = $defaultLocale;
			if(!file_exists($from)) {
				return new SS_HTTPResponse(_t('LangEditor.TOFILEMISSING','The from language does not exist!'),500);
			}
		}
	
		// Use the Zend copy of this script to prevent class conflicts when RailsYaml is included
		require_once 'thirdparty/zend_translate_railsyaml/library/Translate/Adapter/thirdparty/sfYaml/lib/sfYaml.php';
	
		$old_data = sfYaml::load($from);
	
		// get content of source file and replace language
		$new_data = array();
		$new_data[$toLocale] = $old_data[$fromLocale];
	
		// merge data if target file exists
		if (file_exists($to)) {
	
			$message = _t('LangEditor.MERGED','Merged');
	
			$existing_data = sfYaml::load($to);
	
			if(is_array($existing_data) && isset($existing_data[self::$currentLocale])
					&& is_array($new_data) && isset($new_data[self::$currentLocale]))
			{
				$new_data = array_replace_recursive($new_data, $existing_data);
			}
	
		}
	
		// write new content into target file
		$new_content = sfYaml::dump($new_data, 99);
		$new_file = self::get_lang_file(self::$currentModule, self::$currentLocale);
		if($fh = fopen($new_file, "w")) {
			fwrite($fh, $new_content);
			fclose($fh);
		} else {
			$message = "Cannot write language file!";
		}
	
		return $message;
	}
	
}