(function($) {
	$.entwine.warningLevel = $.entwine.WARN_LEVEL_BESTPRACTISE;
	$.entwine('ss', function($) {
	
		$('#Form_CreateTranslationForm').entwine({
			onsubmit: function(){
				var $t = $(this);
				var old_lang = $('#Form_CreateTranslationForm_LanguageFrom').val();
				var new_lang = $('#Form_CreateTranslationForm_LanguageTo').val();
				$.post(
					$t.attr('action'),
					$t.serialize(),
					function(data) {
						// display message
						statusMessage(decodeURIComponent(data), 'good');
						// reload language list
						var url = $('#'+old_lang).attr('href').replace('/'+old_lang+'/', '/'+new_lang+'/');
						window.location = $.path.makeUrlAbsolute(url, $('base').attr('href'));
					}
				);
				return false;
			}
		});
		
		$('#Form_TranslationForm').entwine({
			onsubmit: function(e) {
				$('.cms-content').addClass('loading');
				$t = $(this);
				$(this).removeClass('changed');
				$.post(
					$t.attr('action'),
					$t.serialize(),
					function(data) {
						$('.cms-content').removeClass('loading');
						statusMessage(data, 'good');
						var url = $('#available_languages a.current').first().attr('href');
						window.location = $.path.makeUrlAbsolute(url, $('base').attr('href'));
					}
				);
				return false;
			}
		});
		
		$('#Namespace').entwine({
			onchange: function(){
				if($(this).val().length > 0) {
					$('.namespace').hide();
					$('#namespace-'+$(this).val()).show();
				} else {
					$('.namespace').show();
				}
			}
		});
	
		var timeout = false;
	
		$('#translationsearch input').entwine({
			onkeyup: function(){
				if(timeout) window.clearTimeout(timeout);
				s = $(this).val();		
				timeout = window.setTimeout(function() {
					$('.entity').hide();
					$('.entity').each(function() {
						reg = new RegExp(s,"i");
						if($(this).find('.entity_label').text().match(reg) || $(this).find('.entity_field input').val().match(reg)) {
							$(this).show();
						}
					});		
				},150);
			}					
		});
		
		$('#Form_CopyVariablesForm').entwine({
			onsubmit: function(){
				var $t = $(this);
//				var fromM = $('#Form_CopyVariablesForm_fromModule').val();
//				var toM = $('#Form_CopyVariablesForm_toModule').val();
//				var dLocale = $('#Form_CopyVariablesForm_defaultLocale').val();
				
				//disable button:
				$('#Form_CopyVariablesForm_action_doCopyVariables').prop('disabled', true);
				
				//show loader:
				//../../images/network-save-constructive.gif
				$('#Form_CopyVariablesForm_action_doCopyVariables').addClass('loader-input');
				
				$.post(
					$t.attr('action'),
					$t.serialize(),
					function(data) {
						// display message
						statusMessage(decodeURIComponent(data), 'good');
						// reload language list
						location.reload(true);
					}
				);
				return false;
			}
		});
		
		/*************************
		 ******** ADDONS ********* 
		 *************************/
		
		/**
		 * Toggle left filter menus
		 */
		$(".getShowHideAction").entwine({
			onclick: function(e) {
				e.preventDefault();
				
				attr = $(this).attr('data-hide');
				if( attr.length <= 0)
					return false;
				
				field = $('.'+attr);
				if( field.length <= 0)
					return false;
				
				if( field.css('display') == 'none' ) {
					$(this).addClass('opened');
					$(this).removeClass('closed');
					field.slideDown(200);
				} else {
					$(this).removeClass('opened');
					$(this).addClass('closed');
					field.slideUp(100);
				}
			}
		});
		
		
		/**
		 * Toggle system variables
		 */
		$("#ShowSystemVars").entwine({
			onclick: function(e) {
				e.preventDefault();
				mode = $(this).attr('data-mode');
				if(mode == 'show') {
					$('div.entity.systemField').show();
					$(this).attr('data-mode', 'hide');
				} else {
					$('div.entity.systemField').hide();
					$(this).attr('data-mode', 'show');
				}
			}
		});
		
		/**
		 * Add new element to list
		 */
		$("#add_feature_form").entwine({
			onsubmit: function(e) {
				e.preventDefault();
				
				currLan = $('form#Form_TranslationForm').attr('data-language');
				
				namespace = $('input[name=namespace]', $(this)).val();
				name = $('input[name=name]', $(this)).val();
				
				if(namespace.length <= 0 || name.length <= 0) {
					alert('Vpisati je potrebno vsaj NAMESPACE in NAME');
					return;
				}
				
				val = $('input[name=value]', $(this)).val();
				
				target = $('div#namespace-'+namespace);
				// append new namespace if it doesn't exist
				if( target.length <= 0) {
					$("#namespaces").append('<div class="namespace" id="namespace-'+namespace+'"></div>');
					target = $('div#namespace-'+namespace);
				}
				
				newID = 't-'+currLan+'-'+namespace+'-'+name;
				
				if($("#"+newID).length > 0) {
					
					alert('Element že obstaja!');
					
				} else {
				
					// append row
					target.append(
						'<div class="entity">'
							+'<div class="entity_label">'+namespace+'.'+name+'</div>'
							+'<div class="entity_field">'
							+'<input type="text" name="t['+currLan+']['+namespace+']['+name+']" value="'+val+'" class="text nolabel" id="'+newID+'"><span class="delete"></span>'
						+'</div>'
					);
					
					if($("input[name=resetNamespace]:checked").length <= 0)
						$('input[name=namespace]', $(this)).val('');
					
					$('input[name=value]', $(this)).val('');
					$('input[name=name]', $(this)).val('');
				}
				
			}
		});
		
		/**
		 * Delete row
		 */
		$(".delete").entwine({
			onclick: function(e) {
				e.preventDefault();
				$(this).parent().parent().remove();
			}
		});
		
		/**
		 * Clear cahe
		 */
		$("#cleari18nCache").entwine({
			onclick: function(e) {
				e.preventDefault();
				
				var link = $(this).attr('data-link');
				
				$.ajax({
					type: 'post',
					url: link,
					success: function(data) {
						// display message
						statusMessage(data, 'good');
					}
				});
			}
		});
		
		/**
		 * Translate to all
		 */
		$("#translateToAll").entwine({
			onclick: function(e) {
				e.preventDefault();
				
				if($(this).hasClass('disabled'))
					return false;
				
				this.disableLink($(this));
				
				baseHref = $("base").attr('href');
				tmpLocale = $(this).attr('data-tmplocale');
				module = $(this).attr('data-module');
				translateLink = $(this).attr('data-link');
				collector = '/dev/tasks/i18nTextCollectorTask?';
				
				translateLink = $("#translateToAll").entwine().fixLink(translateLink);
				
				if(tmpLocale.length <= 0) {
					alert('No available locales for translation');
					return;
				} else if(baseHref.length <= 0) {
					alert('No base href!');
					return;
				} else if(module.length <= 0) {
					alert('No selected module!');
					return;
				} else if(translateLink.length <= 0) {
					alert('No link for translation');
					return;
				}
				
				//numberOfSteps = $('select[name=LanguageTo] option').size();
				numberOfSteps = 2;
				
				
				this.stagePrep();
				
				this.increaseProgress(1);
				
				this.stageOne(baseHref, collector, module, tmpLocale, translateLink);
				
			},
			
			enableLink: function(element) {
				element.addClass('disabled');
			},

			disableLink: function(element) {
				element.removeClass('disabled');
			},
			stagePrep: function() {
				
				$('#other_translation').append('<div class="progressWrap">'
						+'<div class="progress"></div>'
					+'</div>');
			},
			
			removeProgress: function() {
				setTimeout( function() {
					$('.progressWrap').fadeOut(200, function() {
						$(this).remove();
						$("#translateToAll").entwine().disableLink($("#translateToAll"));
						location.reload();
					});
				}, 2000);
			},
			
			increaseProgress: function(to) {
				toWP = to + '%';
				$(".progress").html( ((to == 100)?'done' : toWP) ).css('width', toWP);
			},
			
			/**
			 * Stage one, create iframe and translate page into temporary locale
			 */
			stageOne: function(base, task, module, locale, translateLink) {
				
				iframeURL = base +''+ task + 'module=' + module +'&locale=' + locale; 
				
				var iframe = document.createElement('iframe');
					iframe.style.width = 1;
					iframe.style.height = 1;
					iframe.style.display = "none";
					iframe.src = iframeURL;
					iframe.onload = function() {
						$("#translateToAll").entwine().removeFrame();
						$("#translateToAll").entwine().increaseProgress( (1 / numberOfSteps) * 100 );
						$("#translateToAll").entwine().stageTwo(base, translateLink, module, locale);
					};
				document.getElementById("other_translation").appendChild(iframe);
			},
			
			removeFrame: function() {
				$("#other_translation").find('iframe').remove();
			},
			
			stageTwo: function(base, translateLink, module, localeFrom) {
				iframeURL = base +''+ translateLink + '?module=' + module + '&locale=' + localeFrom;
				
				var iframe = document.createElement('iframe');
					iframe.style.width = 1;
					iframe.style.height = 1;
					iframe.style.display = "none";
					iframe.src = iframeURL;
					iframe.onload = function() {
						$("#translateToAll").entwine().removeFrame();
						$("#translateToAll").entwine().increaseProgress( (2 / numberOfSteps) * 100 );
						$("#translateToAll").entwine().removeProgress();
					};
				document.getElementById("other_translation").appendChild(iframe);
			},
			
			fixLink: function(link) {
				n = link.indexOf('?');
				if(n > 0)
					return link.substring(0, n);
				return link;
			}
		});
		
    });
})(jQuery);