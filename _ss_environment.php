<?php
/**
 * Innovatif SilverStripe environment setup. 
 * This is a common environment setup for all projects on a server,
 * so it has to be placed above all projects on a server.
 * 
 * You should define
 * 	- default project url
 * 	- assets remap UNC & assets remap blacklist
 *  - secure URL
 *  - ldap settings
 * 
 * At the end, we try to include innovatif/Environment and initialize it.
 * Initialization will:
 * 	- try to resolve/find project name (unless manually specified!)
 * 	- try to set File To Url Mapping (if Env_DefProjectURL)
 *  - try to remap assets (if Env_AssetsRemapUNC is set and not on Env_AssetsRemapBlackList)
 * 
 * By default, class Environment relies on location of this file,
 * to determine ProjectName - resolved as the first directory under directory 
 * where environment (this) file resides.
 * 
 * Project name can also be manually set (ie with some other helper function), 
 * when initializing the environment for the first time! See bellow.
 * 
 * @see 	\innovatif\Environment
 * @author  Blaz 
 */
namespace innovatif;

/**
 * Default project URL
 *
 * Used by CLI scripts, can be reset later...
 * %s will be replaced with ProjectName
 *
 * On dev machines usually: 'http://devmachine.innovatif.lan:port/%s/'
 * On stage and live machines 'http://%s/'
 */
const ENV_DEFPROJECTURL = 'http://%s/';

/**
 * Assets remaping UNC to project root.
 * 
 * Default assets remaping. 
 * %s will be replaced with ProjectName
 * On dev machines usually: '//rozle.innovatif.lan/virtual/%s/webdir'
 * On stage and live machines DO NOT SET / COMMENT OUT!!!
 *
 * If assets are being remaped, MAKE SURE that also URL is beiing remaped (via virtual folder!)
 */
//const ENV_ASSETSREMAPUNC = '//pehta.innovatif.com/virtual/%s/webdir';

/**
 * Exclude from assets remaping.
 *
 * Comma separated list of project names, which to exclude from assets remaping.
 * If you don't wan't to use remaping for any project, don't define INN_Env_AssetsRemapUNC
 *
 * Do not include any spaces!
 *
 * Examle: 'www.project1.lan,www.project2.lan'
 */
const ENV_ASSETSREMAPBLACKLIST = '';

/**
 * Default secure URL
 *
 * %s will be replaced with ProjectName
 *
 * On dev and stage machines usually 'https://rozle.innovatif.lan/%s/' 
 * On live machines usually 'https://pehta.innovatif.com/%s/' 
 *
 * You can enable/disable usage of secure URL in mysite/_config_common.php
 */
#const ENV_SECUREURL = 'https://pehta.sites.innovatif.com/%s/';


/*************************************************************************************************
 * END OF CONFIGURATIONS
 ************************************************************************************************/

if (class_exists('\\'.__NAMESPACE__.'\\Environment')) {
	/*
	 * Initialize the environment
	 * and remap assets. This is the only place, where assets remaping can be done!
	 * 
	 * You can initialize environment with custom project name!
	 * 
	 * example:  
	 * $INN_env = Environment::get($projectName);
	 */	
	$INN_env = Environment::get();
	$INN_env->remapAssets();
}

/**
 * Adding default email logging
 * We have to set include path and include some files, because autoloader doesn't work yet.
 */
/*
$F_NAME = (defined('\\'.__NAMESPACE__.'\Environment::IS_SS3') && Environment::IS_SS3) ? 'framework' : 'sapphire';
set_include_path(BASE_PATH . '/' . $F_NAME . PATH_SEPARATOR
	. BASE_PATH . '/' . $F_NAME . '/parsers' . PATH_SEPARATOR
	. BASE_PATH . '/' . $F_NAME . '/thirdparty' . PATH_SEPARATOR
	. get_include_path());

require_once 'dev/LogEmailWriter.php';
require_once 'dev/Log.php';
require_once 'dev/ZendLog.php';
$EmailWriter = new \SS_LogEmailWriter('ss-errors@innovatif.com');
$EmailWriter->set_send_from($INN_env->projectName . '@pehta.innovatif.com');
\SS_Log::add_writer($EmailWriter, \SS_Log::NOTICE, '<=');
 */
$SS3 = (defined('\\'.__NAMESPACE__.'\Environment::IS_SS3') && Environment::IS_SS3);
$F_NAME = $SS3 ? 'framework' : 'sapphire';
set_include_path(BASE_PATH . '/' . $F_NAME . PATH_SEPARATOR
	. BASE_PATH . '/' . $F_NAME . '/parsers' . PATH_SEPARATOR
	. BASE_PATH . '/' . $F_NAME . '/thirdparty' . PATH_SEPARATOR
	. get_include_path());
 
require_once 'dev/LogEmailWriter.php';
require_once 'dev/Log.php';
require_once 'dev/ZendLog.php';
if ($SS3) {
	require_once 'core/Config.php';
	require_once 'core/Object.php';
}

