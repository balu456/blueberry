<?php
namespace innovatif;
/************************************************************************************
 ************************************************************************************
 **                                                                                **
 **  If you can read this text in your browser then you don't have PHP installed.  **
 **  Please install PHP 5.0 or higher, preferably PHP 5.2.                         **
 **                                                                                **
 ************************************************************************************
 ************************************************************************************/

/**
 * Innovatif SilverStripe CMS Installer
 * This installer doesn't use any of the fancy Sapphire stuff in case it's unsupported.
 * It's also PHP4 syntax compatable
 */

// go to root
chdir('../../');

/**
 * Innovatif hook - add autoloader.
 */
require_once 'innovatif/ns_code/Environment.php';


// and find base href
$base_href = 'http' .
	((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') .
	'://' . $_SERVER['SERVER_NAME'] .
	(($_SERVER['SERVER_PORT'] != '80') ? ':'.$_SERVER['SERVER_PORT'] : '') .
	'/' . implode('/',array_slice(explode('/',trim($_SERVER['SCRIPT_NAME'],'/ ')),0,-3));
$base_href .= (substr($base_href, -1) != '/') ? '/' : '';

$alreadyInstalled = false;
if(file_exists('../mysite/_config.php')) {
	// don't do nothing, if config already exists!
	$alreadyInstalled = true;
	include('../innovatif/installer/config-form.html');
	exit;
}
  
// speed up mysql_connect timeout if the server can't be found
ini_set('mysql.connect_timeout', 5);
// Don't die half was through installation; that does more harm than good
ini_set('max_execution_time', 0);
// Prevent a white-screen-of-death
ini_set('display_errors', 'on');

error_reporting(E_ALL | E_STRICT);

// Attempt to start a session so that the username and password can be sent back to the user.
if (function_exists('session_start')) {
	session_start();
}

// Include environment files
$envFiles = array('_ss_environment.php', '../_ss_environment.php', '../../_ss_environment.php');
foreach($envFiles as $envFile) {
	if(@file_exists($envFile)) {
		define(__NAMESPACE__.'\\SS_ENVIRONMENT_FILE',$envFile);
		include_once($envFile);
		break;
	}
}

if (class_exists(__NAMESPACE__.'\\Environment')) {
	$Environment = Environment::get();
}

// Include installer defaults, first common ones... the last one should always exist!
$defFiles = array('../../_ss_installer_defaults.php','../_ss_installer_defaults.php','innovatif/installer/_ss_installer_defaults.php');
foreach($defFiles as $defFile) {
	if(@file_exists($defFile)) {
		include_once($defFile);
		break;
	}
}

require_once FRAMEWORK_NAME.'/dev/install/DatabaseConfigurationHelper.php';
require_once FRAMEWORK_NAME.'/dev/install/DatabaseAdapterRegistry.php';

// Set default locale, but try and sniff from the user agent
$defaultLocale = DEF_LOCALE;
$locales = array(
		'af_ZA' => 'Afrikaans (South Africa)',
		'ar_EG' => 'Arabic (Egypt)',
		'hy_AM' => 'Armenian (Armenia)',
		'ast_ES' => 'Asturian (Spain)',
		'az_AZ' => 'Azerbaijani (Azerbaijan)',
		'bs_BA' => 'Bosnian (Bosnia and Herzegovina)',
		'bg_BG' => 'Bulgarian (Bulgaria)',
		'ca_ES' => 'Catalan (Spain)',
		'zh_CN' => 'Chinese (China)',
		'zh_TW' => 'Chinese (Taiwan)',
		'hr_HR' => 'Croatian (Croatia)',
		'cs_CZ' => 'Czech (Czech Republic)',
		'da_DK' => 'Danish (Denmark)',
		'nl_NL' => 'Dutch (Netherlands)',
		'en_GB' => 'English (United Kingdom)',
		'en_US' => 'English (United States)',
		'eo_XX' => 'Esperanto',
		'et_EE' => 'Estonian (Estonia)',
		'fo_FO' => 'Faroese (Faroe Islands)',
		'fi_FI' => 'Finnish (Finland)',
		'fr_FR' => 'French (France)',
		'de_DE' => 'German (Germany)',
		'el_GR' => 'Greek (Greece)',
		'he_IL' => 'Hebrew (Israel)',
		'hu_HU' => 'Hungarian (Hungary)',
		'is_IS' => 'Icelandic (Iceland)',
		'id_ID' => 'Indonesian (Indonesia)',
		'it_IT' => 'Italian (Italy)',
		'ja_JP' => 'Japanese (Japan)',
		'km_KH' => 'Khmer (Cambodia)',
		'lc_XX' => 'LOLCAT',
		'lv_LV' => 'Latvian (Latvia)',
		'lt_LT' => 'Lithuanian (Lithuania)',
		'ms_MY' => 'Malay (Malaysia)',
		'mi_NZ' => 'Maori (New Zealand)',
		'ne_NP' => 'Nepali (Nepal)',
		'nb_NO' => 'Norwegian',
		'fa_IR' => 'Persian (Iran)',
		'pl_PL' => 'Polish (Poland)',
		'pt_BR' => 'Portuguese (Brazil)',
		'pa_IN' => 'Punjabi (India)',
		'ro_RO' => 'Romanian (Romania)',
		'ru_RU' => 'Russian (Russia)',
		'sr_RS' => 'Serbian (Serbia)',
		'si_LK' => 'Sinhalese (Sri Lanka)',
		'sk_SK' => 'Slovak (Slovakia)',
		'sl_SI' => 'Slovenian (Slovenia)',
		'es_AR' => 'Spanish (Argentina)',
		'es_MX' => 'Spanish (Mexico)',
		'es_ES' => 'Spanish (Spain)',
		'sv_SE' => 'Swedish (Sweden)',
		'th_TH' => 'Thai (Thailand)',
		'tr_TR' => 'Turkish (Turkey)',
		'uk_UA' => 'Ukrainian (Ukraine)',
		'uz_UZ' => 'Uzbek (Uzbekistan)',
		'vi_VN' => 'Vietnamese (Vietnam)',
);

// Discover which databases are available
\DatabaseAdapterRegistry::autodiscover();

// Determine which external database modules are USABLE
foreach(\DatabaseAdapterRegistry::get_adapters() as $class => $details) {
	$databaseClasses[$class] = $details;
	if(file_exists($details['helperPath'])) {
		$databaseClasses[$class]['hasModule'] = true;
		include_once($details['helperPath']);
	} else {
		$databaseClasses[$class]['hasModule'] = false;
	}
}

// Load database config
if(isset($_REQUEST['db'])) {
	if(isset($_REQUEST['db']['type'])) $type = $_REQUEST['db']['type'];
	else $type = $_REQUEST['db']['type'] = DEF_DATABASECLASS;

	// Normal behaviour without the environment
	$databaseConfig = $_REQUEST['db'][$type];
	$databaseConfig['type'] = $type;
} else {
	$type = $_REQUEST['db']['type'] = DEF_DATABASECLASS;
	$_REQUEST['db'][$type] = $databaseConfig = array(
		"type" => $type,
		"server" => DEF_DATABASESERVER,
		"username" => isset($Environment) ? $Environment->projectify(DEF_DATABASEUSERNAME) : DEF_DATABASEUSERNAME,
		"password" => DEF_DATABASEPASSWORD,
		"database" => isset($_SERVER['argv'][2]) ? $_SERVER['argv'][2] : 
			(isset($Environment) ? $Environment->projectify(DEF_DATABASENAME) : DEF_DATABASENAME),
	);
}

if(isset($_REQUEST['admin'])) {
	$adminConfig = $_REQUEST['admin'];
} elseif (isset($_POST['db'])) {
	// unchecked inputs don't submit anything, so we use DB to check, if we are in a POST
	$_REQUEST['admin'] = $adminConfig = array(
		'do'		=> false,
		'username'	=> DEF_ADMINUSERNAME,
		'password'	=> DEF_ADMINPASSWORD,
	);
} else {
	$_REQUEST['admin'] = $adminConfig = array(
		'do'		=> DEF_ADMINDO,
		'username'	=> DEF_ADMINUSERNAME,
		'password'	=> DEF_ADMINPASSWORD,
	);
}

// load site modes
$allSiteModes = array('dev','test','live');
if (isset($_REQUEST['mode']) && in_array($_REQUEST['mode'],$allSiteModes)) {
	$sitemode = $_REQUEST['mode'];
} else {
	$sitemode = $_REQUEST['mode'] = DEF_SITEMODE;
}


// load force SSL
if (isset($_REQUEST['forcesecure']) && in_array($_REQUEST['forcesecure'],array('yes','no'))) {
	$forceSSL = ($_REQUEST['forcesecure'] == 'yes');
} else {
	$forceSSL = DEF_FORCESSL;
}
$_REQUEST['forcesecure'] = $forceSSL;

// load use LDAP
if (isset($_REQUEST['ldap']) && in_array($_REQUEST['ldap'],array('yes','no'))) {
	$useLDAP = ($_REQUEST['ldap'] == 'yes');
} else {
	$useLDAP = $_REQUEST['ldap'] = DEF_USELDAP;
}

// load log folder.
if (isset($_REQUEST['logdir'])) {
	$logdir = rtrim($_REQUEST['logdir'],'/\\').DIRECTORY_SEPARATOR;
} else {
	$logdir = realpath(isset($Environment) ? $Environment->projectify(DEF_LOGDIR) : DEF_LOGDIR); 
}
$_REQUEST['logdir'] = $logdir; 

// load log prefix.
if (isset($_REQUEST['logprefix'])) {
	$logprefix = $_REQUEST['logprefix'];
} else {
	$logprefix = isset($Environment) ? $Environment->projectify(DEF_LOGPREFIX) : DEF_LOGPREFIX; 
	$_REQUEST['logprefix'] = $logprefix;
}

// check log
$logCheck = new InstallRequirements();
$logCheck->checkLogdir($logdir);

// Check requirements
$req = new InstallRequirements();
$req->check();

$allThemes = $req->getInstalledThemes();
$allThemes[] = "NONE";

// Load theme
if (!isset($_REQUEST['theme']) || !in_array($_REQUEST['theme'], $allThemes)) {
	$theme = $_REQUEST['theme'] = 'NONE';
} else {
	$theme = $_POST['theme'];
}

$webserverConfigFile = '';
if($req->isIIS()) {
	$webserverConfigFile = 'web.config';
} else {
	$webserverConfigFile = '.htaccess';
}

if($req->hasErrors()) {
	$hasErrorOtherThanDatabase = true;
	$phpIniLocation = php_ini_loaded_file();
}

if($databaseConfig) {
	$dbReq = new InstallRequirements();
	$dbReq->checkDatabase($databaseConfig);
	
	if (!$dbReq->hasErrors()) {
		$databaseClasses[$databaseConfig['type']]['databases'] = $dbReq->getAllDatabases($databaseConfig,
				array(
					"Database Configuration",
					"Loading databases",
					"Can't load databases!"
				)
		 );
	}
}

if($adminConfig) {
	$adminReq = new InstallRequirements();
	$adminReq->checkAdminConfig($adminConfig);
}


if(isset($_REQUEST['go']) && !$req->hasErrors() && !$dbReq->hasErrors() && !$logCheck->hasErrors()
			&& !$adminReq->hasErrors()) {
	$inst = new Installer();
	$inst->install($_REQUEST);

// Show the config form
} else {
	include('innovatif/installer/config-form.html');	
}

/**
 * This class checks requirements
 * Each of the requireXXX functions takes an argument which gives a user description of the test.
 * It's an array of 3 parts:
 *  $description[0] - The test catetgory
 *  $description[1] - The test title
 *  $description[2] - The test error to show, if it goes wrong
 */
class InstallRequirements {
	var $errors, $warnings, $tests;
	
	function checkLogdir ($logdir) {
		if (!empty($logdir)) {
			if ($this->requireDirectory($logdir, array("Log directory", "Does log dir exit?", null))) {
				$this->requireWriteable($logdir, array("Log directory", "Is log dir writeable?", null),true);	
			}
		}
		
	}
	/**
	 * Check the database configuration. These are done one after another
	 * starting with checking the database function exists in PHP, and
	 * continuing onto more difficult checks like database permissions.
	 */
	function checkDatabase($databaseConfig) {
		if($this->requireDatabaseFunctions(
			$databaseConfig,
			array(
				"Database Configuration",
				"Database support",
				"Database support in PHP",
				$this->getDatabaseTypeNice($databaseConfig['type'])
			)
		)) {
			if($this->requireDatabaseServer(
				$databaseConfig,
				array(
					"Database Configuration",
					"Database server",
					$databaseConfig['type'] == 'SQLiteDatabase' ? "I couldn't write to path '$databaseConfig[path]'" : "I couldn't find a database server on '$databaseConfig[server]'",
					$databaseConfig['type'] == 'SQLiteDatabase' ? $databaseConfig['path'] : $databaseConfig['server']
				)
			)) {
				if($this->requireDatabaseConnection(
					$databaseConfig,
					array(
						"Database Configuration",
						"Database access credentials correct",
						"That username/password doesn't work"
					)
				)) {
					if($this->requireDatabaseVersion(
						$databaseConfig,
						array(
							"Database Configuration",
							"Database server meets required version",
							'',
							'Version ' . $this->getDatabaseConfigurationHelper($databaseConfig['type'])->getDatabaseVersion($databaseConfig)
						)
					)) {
						$this->requireDatabaseOrCreatePermissions(
							$databaseConfig,
							array(
								"Database Configuration",
								"Can I access/create the database",
								"I can't create new databases and the database '$databaseConfig[database]' doesn't exist"
							)
						);
					}
				}
			}
		}
	}
	
	function checkAdminConfig($adminConfig) {
		if (isset($adminConfig['do']) && $adminConfig['do']) {
			if(!$adminConfig['username']) {
				$this->error(array('Admin', 'username', 'Please enter a username!'));
			}
			if(!$adminConfig['password']) {
				$this->error(array('Admin', 'password', 'Please enter a password!'));
			}
		}
	}

	/**
	 * Check if the web server is IIS.
	 * @return boolean
	 */
	function isIIS($version = 7) {
		if(strpos($this->findWebserver(), 'IIS/' . $version) !== false) {
			return true;
		} else {
			return false;
		}
	}

	function isApache() {
		if(strpos($this->findWebserver(), 'Apache') !== false) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Find the webserver software running on the PHP host.
	 * @return string|boolean Server software or boolean FALSE
	 */
	function findWebserver() {
		// Try finding from SERVER_SIGNATURE or SERVER_SOFTWARE
		$webserver = @$_SERVER['SERVER_SIGNATURE'];
		if(!$webserver) $webserver = @$_SERVER['SERVER_SOFTWARE'];

		if($webserver) return strip_tags(trim($webserver));
		else return false;
	}
	
	/**
	 * Check everything except the database
	 */
	function check() {
		$this->errors = null;
		$isApache = $this->isApache();
		$isIIS = $this->isIIS(7) || $this->isIIS(8);
		$webserver = $this->findWebserver();
		
		$this->requirePHPVersion('5.3.4', '5.3.2', array("PHP Configuration", "PHP5 installed", null, "PHP version " . phpversion()));

		// Check that we can identify the root folder successfully
		$this->requireFile(FRAMEWORK_NAME . '/dev/install/config-form.html', array("File permissions", 
			"Does the webserver know where files are stored?", 
			"The webserver isn't letting me identify where files are stored.",
			$this->getBaseDir()
		));
		
		$this->requireModule('mysite', array("File permissions", "mysite/ directory exists?"));
		$this->requireModule(FRAMEWORK_NAME, array("File permissions", FRAMEWORK_NAME . "/ directory exists?"));
		
		if($isApache) {
			$this->requireWriteable('.htaccess', array("File permissions", "Is the .htaccess file writeable?", null));
		} elseif($isIIS) {
			$this->requireWriteable('web.config', array("File permissions", "Is the web.config file writeable?", null));
		}
		
		$this->requireWriteable('mysite/_config.php', array("File permissions", "Is the mysite/_config.php file writeable?", null));
		if (!$this->checkModuleExists('cms')) {
			$this->requireWriteable('mysite/code/RootURLController.php', array("File permissions", "Is the mysite/code/RootURLController.php file writeable?", null));
		}
		$this->requireWriteable('assets', array("File permissions", "Is the assets/ directory writeable?", null));
		
		$tempFolder = $this->getTempFolder();
		$this->requireTempFolder(array('File permissions', 'Is a temporary directory available?', null, $tempFolder));
		if($tempFolder) {
			// in addition to the temp folder being available, check it is writable
			$this->requireWriteable($tempFolder, array("File permissions", sprintf("Is the temporary directory writeable?", $tempFolder), null), true);
		}

		$this->isRunningWebServer(array("Webserver Configuration", "Server software", "Unknown", $webserver));
		
		if($isApache) {
			$this->requireApacheRewriteModule('mod_rewrite', array("Webserver Configuration", "URL rewriting support", "You need mod_rewrite to use friendly URLs with SilverStripe, but it is not enabled."));
		} elseif($isIIS) {
			$this->requireIISRewriteModule('IIS_UrlRewriteModule', array("Webserver Configuration", "URL rewriting support", "You need to enable the IIS URL Rewrite Module to use friendly URLs with SilverStripe, but it is not installed or enabled. Download it for IIS 7 from http://www.iis.net/expand/URLRewrite"));
		} else {
			$this->warning(array("Webserver Configuration", "URL rewriting support", "I can't tell whether any rewriting module is running.  You may need to configure a rewriting rule yourself."));
		}
		
		$this->requireServerVariables(array('SCRIPT_NAME','HTTP_HOST','SCRIPT_FILENAME'), array("Webserver config", "Recognised webserver", "You seem to be using an unsupported webserver.  The server variables SCRIPT_NAME, HTTP_HOST, SCRIPT_FILENAME need to be set."));
		
		// Check for GD support
		if(!$this->requireFunction("imagecreatetruecolor", array("PHP Configuration", "GD2 support", "PHP must have GD version 2."))) {
			$this->requireFunction("imagecreate", array("PHP Configuration", "GD2 support", "GD support for PHP not included."));
		}
		
		// Check for XML support
		$this->requireFunction('xml_set_object', array("PHP Configuration", "XML support", "XML support not included in PHP."));
		$this->requireClass('DOMDocument', array("PHP Configuration", "DOM/XML support", "DOM/XML support not included in PHP."));
		$this->requireFunction('simplexml_load_file', array('PHP Configuration', 'SimpleXML support', 'SimpleXML support not included in PHP.'));
		
		// Check for token_get_all
		$this->requireFunction('token_get_all', array("PHP Configuration", "PHP Tokenizer", "PHP tokenizer support not included in PHP."));
		
		// Check for CType support
		$this->requireFunction('ctype_digit', array('PHP Configuration', 'CType support', 'CType support not included in PHP.'));
		
		// Check for session support
		$this->requireFunction('session_start', array('PHP Configuration', 'Session support', 'Session support not included in PHP.'));
		
		// Check for iconv support
		$this->requireFunction('iconv', array('PHP Configuration', 'iconv support', 'iconv support not included in PHP.'));
		
		// Check for hash support
		$this->requireFunction('hash', array('PHP Configuration', 'hash support', 'hash support not included in PHP.'));

		// Check for mbstring support
		$this->requireFunction('mb_internal_encoding', array('PHP Configuration', 'mbstring support', 'mbstring support not included in PHP.'));
		
		// Check for Reflection support
		$this->requireClass('ReflectionClass', array('PHP Configuration', 'Reflection support', 'Reflection support not included in PHP.'));
		
		// Check for Standard PHP Library (SPL) support
		$this->requireFunction('spl_classes', array('PHP Configuration', 'SPL support', 'Standard PHP Library (SPL) not included in PHP.'));
		
		$this->requireDateTimezone(array('PHP Configuration', 'date.timezone setting and validity', 'date.timezone option in php.ini must be set correctly.', ini_get('date.timezone')));
		
		$this->suggestClass('finfo', array('PHP Configuration', 'fileinfo support', 'fileinfo should be enabled in PHP. SilverStripe uses it for MIME type detection of files. SilverStripe will still operate, but email attachments and sending files to browser (e.g. export data to CSV) may not work correctly without finfo.'));
		
		$this->suggestPHPSetting('asp_tags', array(false,0,''), array('PHP Configuration', 'asp_tags option', 'This should be turned off as it can cause issues with SilverStripe'));
		$this->suggestPHPSetting('magic_quotes_gpc', array(false,0,''), array('PHP Configuration', 'magic_quotes_gpc option', 'This should be turned off, as it can cause issues with cookies. More specifically, unserializing data stored in cookies.'));
		$this->suggestPHPSetting('display_errors', array(false,0,''), array('PHP Configuration', 'display_errors option', 'Unless you\'re in a development environment, this should be turned off, as it can expose sensitive data to website users.'));
		
		// Check memory allocation
		$this->requireMemory(32*1024*1024, 64*1024*1024, array("PHP Configuration", "Memory allocated (PHP config option 'memory_limit')", "SilverStripe needs a minimum of 32M allocated to PHP, but recommends 64M.", ini_get("memory_limit")));
			
		return $this->errors;
	}
	
	function suggestPHPSetting($settingName, $settingValues, $testDetails) {
		$this->testing($testDetails);
		
		$val = ini_get($settingName);
		if(!in_array($val, $settingValues) && $val != $settingValues) {
			$testDetails[2] = "$settingName is set to '$val' in php.ini.  $testDetails[2]";
			$this->warning($testDetails);
		}
	}

	function suggestClass($class, $testDetails) {
		$this->testing($testDetails);
		if(!class_exists($class)) {
			$this->warning($testDetails);
		}
	}
		
	function requireDateTimezone($testDetails) {
		$this->testing($testDetails);
		$result = ini_get('date.timezone') && in_array(ini_get('date.timezone'), timezone_identifiers_list());
		if(!$result) {
			$this->error($testDetails);
		}
	}

	function requireMemory($min, $recommended, $testDetails) {
		$_SESSION['forcemem'] = false;
		
		$mem = $this->getPHPMemory();
		if($mem < (64 * 1024 * 1024)) {
			ini_set('memory_limit', '64M');
			$mem = $this->getPHPMemory();
			$testDetails[3] = ini_get("memory_limit");
		}
		
		$this->testing($testDetails);

		if($mem < $min && $mem > 0) {
			$testDetails[2] .= " You only have " . ini_get("memory_limit") . " allocated";
			$this->error($testDetails);
		} else if($mem < $recommended && $mem > 0) {
			$testDetails[2] .= " You only have " . ini_get("memory_limit") . " allocated";
			$this->warning($testDetails);
		} elseif($mem == 0) {
			$testDetails[2] .= " We can't determine how much memory you have allocated. Install only if you're sure you've allocated at least 20 MB.";
			$this->warning($testDetails);
		}
	}
	
	function getPHPMemory() {
		$memString = ini_get("memory_limit");

		switch(strtolower(substr($memString,-1))) {
			case "k":
				return round(substr($memString,0,-1)*1024);

			case "m":
				return round(substr($memString,0,-1)*1024*1024);
			
			case "g":
				return round(substr($memString,0,-1)*1024*1024*1024);
			
			default:
				return round($memString);
		}
	}
	
	function listErrors() {
		if($this->errors) {
			echo "<p>The following problems are preventing me from installing SilverStripe CMS:</p>\n\n";
			foreach($this->errors as $error) {
				echo "<li>" . htmlentities(implode(", ", $error), ENT_COMPAT, 'UTF-8') . "</li>\n";
			}
		}
	}
	
	function showTable($section = null) {
		if($section) {
			$tests = $this->tests[$section];
			$id = strtolower(str_replace(' ', '_', $section));
			echo "<table id=\"{$id}_results\" class=\"testResults\" width=\"100%\">";
			if ($this->tests) foreach($tests as $test => $result) {
				echo "<tr class=\"$result[0]\"><td>$test</td><td>" . nl2br(htmlentities($result[1], ENT_COMPAT, 'UTF-8')) . "</td></tr>";
			}
			echo "</table>";
			
		} else {
			if ($this->tests) foreach($this->tests as $section => $tests) {
				$failedRequirements = 0;
				$warningRequirements = 0;
				
				$output = "";
				
				foreach($tests as $test => $result) {
					if(isset($result['0'])) {
						switch($result['0']) {
							case 'error':
								$failedRequirements++;
								break;
							case 'warning':
								$warningRequirements++;
								break;
						}
					}
					$output .= "<tr class=\"$result[0]\"><td>$test</td><td>" . nl2br(htmlentities($result[1], ENT_COMPAT, 'UTF-8')) . "</td></tr>";
				}
				$className = "good";
				$text = "All Requirements Pass";
				$pluralWarnings = ($warningRequirements == 1) ? 'Warning' : 'Warnings';
				
				if($failedRequirements > 0) {
					$className = "error";
					$pluralWarnings = ($warningRequirements == 1) ? 'Warning' : 'Warnings';
					
					$text = $failedRequirements . ' Failed and '. $warningRequirements . ' '. $pluralWarnings;
				}
				else if($warningRequirements > 0) {
					$className = "warning";
					$text = "All Requirements Pass but ". $warningRequirements . ' '. $pluralWarnings;
				}
				
				echo "<h5 class='requirement $className'><em class='inlineBarText'>$section</em> <a href='#'>See All Requirements</a> <span>$text</span></h5>";
				echo "<table class=\"testResults\">";
				echo $output;
				echo "</table>";
			}		
		}
	}
	
	function requireFunction($funcName, $testDetails) {
		$this->testing($testDetails);
		if(!function_exists($funcName)) $this->error($testDetails);
		else return true;
	}

	function requireClass($className, $testDetails) {
		$this->testing($testDetails);
		if(!class_exists($className)) $this->error($testDetails);
		else return false;
	}

	/**
	 * Require that the given class doesn't exist
	 */
	function requireNoClasses($classNames, $testDetails) {
		$this->testing($testDetails);
		$badClasses = array();
		foreach($classNames as $className) {
			if(class_exists($className)) $badClasses[] = $className;
		}
		if($badClasses) {
			$testDetails[2] .= ".  The following classes are at fault: " . implode(', ', $badClasses);
			$this->error($testDetails);
		}
		else return true;
	}
		
	function requirePHPVersion($recommendedVersion, $requiredVersion, $testDetails) {
		$this->testing($testDetails);
		
		$installedVersion = phpversion();
		
		if(version_compare($installedVersion, $requiredVersion, '<')) {
			$testDetails[2] = "SilverStripe requires PHP version $requiredVersion or later.\n
				PHP version $installedVersion is currently installed.\n
				While SilverStripe requires at least PHP version $requiredVersion, upgrading to $recommendedVersion or later is recommended.\n
				If you are installing SilverStripe on a shared web server, please ask your web hosting provider to upgrade PHP for you.";
			$this->error($testDetails);
			return;
		}
		
		if(version_compare($installedVersion, $recommendedVersion, '<')) {
			$testDetails[2] = "PHP version $installedVersion is currently installed.\n
				Upgrading to at least PHP version $recommendedVersion is recommended.\n
				SilverStripe should run, but you may run into issues. Future releases may require a later version of PHP.\n";
			$this->warning($testDetails);
			return;
		}
		
		return true;
	}

	/**
	 * Check that a module exists
	 */
	function checkModuleExists($dirname) {
		$path = $this->getBaseDir() . $dirname;
		return file_exists($path) && ($dirname == 'mysite' || file_exists($path . '/_config.php'));
	}
	
	/**
	 * The same as {@link requireFile()} but does additional checks
	 * to ensure the module directory is intact.
	 */
	function requireModule($dirname, $testDetails) {
		$this->testing($testDetails);
		$path = $this->getBaseDir() . $dirname;
		if(!file_exists($path)) {
			$testDetails[2] .= " Directory '$path' not found. Please make sure you have uploaded the SilverStripe files to your webserver correctly.";
			$this->error($testDetails);
		} elseif(!file_exists($path . '/_config.php') && $dirname != 'mysite') {
			$testDetails[2] .= " Directory '$path' exists, but is missing files. Please make sure you have uploaded the SilverStripe files to your webserver correctly.";
			$this->error($testDetails);
		}
	}

	function requireFile($filename, $testDetails) {
		$this->testing($testDetails);
		$filename = $this->getBaseDir() . $filename;
		if(!file_exists($filename)) {
			$testDetails[2] .= " (file '$filename' not found)";
			$this->error($testDetails);
		}
	}
	
	function requireDirectory($filename, $testDetails) {
		$this->testing($testDetails);
		if(!is_dir($filename)) {
			$testDetails[2] .= " (directory '$filename' not found)";
			$this->error($testDetails);
			return false;
		}
		return true;
	}
	
	function requireWriteable($filename, $testDetails, $absolute = false) {
		$this->testing($testDetails);

		if($absolute) {
			$filename = str_replace('/', DIRECTORY_SEPARATOR, $filename);
		} else {
			$filename = $this->getBaseDir() . str_replace('/', DIRECTORY_SEPARATOR, $filename);
		}
		
		if(file_exists($filename)) $isWriteable = is_writeable($filename);
		else $isWriteable = is_writeable(dirname($filename));
		
		if(!$isWriteable) {
			if(function_exists('posix_getgroups')) {
				$userID = posix_geteuid();
				$user = posix_getpwuid($userID);

				$currentOwnerID = fileowner(file_exists($filename) ? $filename : dirname($filename) );
				$currentOwner = posix_getpwuid($currentOwnerID);

				$testDetails[2] .= "User '$user[name]' needs to be able to write to this file:\n$filename\n\nThe file is currently owned by '$currentOwner[name]'.  ";

				if($user['name'] == $currentOwner['name']) {
					$testDetails[2] .= "We recommend that you make the file writeable.";
				} else {
					
					$groups = posix_getgroups();
					$groupList = array();
					foreach($groups as $group) {
						$groupInfo = posix_getgrgid($group);
						if(in_array($currentOwner['name'], $groupInfo['members'])) $groupList[] = $groupInfo['name'];
					}
					if($groupList) {
						$testDetails[2] .= "	We recommend that you make the file group-writeable and change the group to one of these groups:\n - ". implode("\n - ", $groupList)
							. "\n\nFor example:\nchmod g+w $filename\nchgrp " . $groupList[0] . " $filename";  		
					} else {
						$testDetails[2] .= "  There is no user-group that contains both the web-server user and the owner of this file.  Change the ownership of the file, create a new group, or temporarily make the file writeable by everyone during the install process.";
					}
				}

			} else {
				$testDetails[2] .= "The webserver user needs to be able to write to this file:\n$filename";
			}
			
			$this->error($testDetails);
		}
	}

	function getTempFolder() {
		if(file_exists($this->getBaseDir() . 'silverstripe-cache')) {
			$sysTmp = $this->getBaseDir();
		} elseif(function_exists('sys_get_temp_dir')) {
			$sysTmp = sys_get_temp_dir();
		} elseif(isset($_ENV['TMP'])) {
			$sysTmp = $_ENV['TMP'];
		} else {
			$tmpFile = @tempnam('adfadsfdas', '');
			@unlink($tmpFile);
			$sysTmp = dirname($tmpFile);
		}

	    $worked = true;
	    $ssTmp = $sysTmp . DIRECTORY_SEPARATOR . 'silverstripe-cache';

		if(!@file_exists($ssTmp)) {
			$worked = @mkdir($ssTmp);

			if(!$worked) {
				$ssTmp = dirname($_SERVER['SCRIPT_FILENAME']) . DIRECTORY_SEPARATOR . 'silverstripe-cache';
				$worked = true;
				if(!@file_exists($ssTmp)) {
					$worked = @mkdir($ssTmp);
				}
			}
		}

		if($worked) return $ssTmp;
		else return false;
	}

	function requireTempFolder($testDetails) {
		$this->testing($testDetails);

		$tempFolder = $this->getTempFolder();
		if(!$tempFolder) {
			$testDetails[2] = "Permission problem gaining access to a temp directory. " .
				"Please create a folder named silverstripe-cache in the base directory " .
				"of the installation and ensure it has the adequate permissions";
			$this->error($testDetails);
	    }
	}
	
	function requireApacheModule($moduleName, $testDetails) {
		$this->testing($testDetails);
		if(!in_array($moduleName, apache_get_modules())) {
			$this->error($testDetails);
			return false;
		} else {
			return true;
		}
	}

	function testApacheRewriteExists($moduleName = 'mod_rewrite') {
		if(function_exists('apache_get_modules') && in_array($moduleName, apache_get_modules())) {
			return true;
		} elseif(isset($_SERVER['HTTP_MOD_REWRITE']) && $_SERVER['HTTP_MOD_REWRITE'] == 'On') {
			return true;
		} else {
			return false;
		}
	}
	
	function testIISRewriteModuleExists($moduleName = 'IIS_UrlRewriteModule') {
		if(isset($_SERVER[$moduleName]) && $_SERVER[$moduleName]) {
			return true;
		} else {
			return false;
		}
	}
	
	function requireApacheRewriteModule($moduleName, $testDetails) {
		$this->testing($testDetails);
		if($this->testApacheRewriteExists()) {
			return true;
		} else {
			$this->warning($testDetails);
			return false;
		}
	}
	
	/**
	 * Determines if the web server has any rewriting capability.
	 * @return boolean
	 */
	function hasRewritingCapability() {
		return ($this->testApacheRewriteExists() || $this->testIISRewriteModuleExists());
	}
	
	function requireIISRewriteModule($moduleName, $testDetails) {
		$this->testing($testDetails);
		if($this->testIISRewriteModuleExists()) {
			return true;
		} else {
			$this->warning($testDetails);
			return false;
		}
	}

	function getDatabaseTypeNice($databaseClass) {
		return substr($databaseClass, 0, -8);
	}

	/**
	 * Get an instance of a helper class for the specific database.
	 * @param string $databaseClass e.g. MySQLDatabase or MSSQLDatabase
	 */
	function getDatabaseConfigurationHelper($databaseClass) {
		$adapters = \DatabaseAdapterRegistry::get_adapters();
		if(isset($adapters[$databaseClass])) {
			$helperPath = $adapters[$databaseClass]['helperPath'];
			$class = '\\'.str_replace('.php', '', basename($helperPath));
		}
		return (class_exists($class)) 
		? new $class() : 
		new \MySQLDatabaseConfigurationHelper();
	}
	
	function requireDatabaseFunctions($databaseConfig, $testDetails) {
		$this->testing($testDetails);
		$helper = $this->getDatabaseConfigurationHelper($databaseConfig['type']);
		$result = $helper->requireDatabaseFunctions($databaseConfig);
		if($result) {
			return true;
		} else {
			$this->error($testDetails);
			return false;
		}
	}
	
	function requireDatabaseConnection($databaseConfig, $testDetails) {
		$this->testing($testDetails);
		$helper = $this->getDatabaseConfigurationHelper($databaseConfig['type']);		
		$result = $helper->requireDatabaseConnection($databaseConfig);
		if($result['success']) {
			return true;
		} else {
			$testDetails[2] .= ": " . $result['error'];
			$this->error($testDetails);
			return false;
		}
	}

	function requireDatabaseVersion($databaseConfig, $testDetails) {
		$this->testing($testDetails);
		$helper = $this->getDatabaseConfigurationHelper($databaseConfig['type']);
		if(method_exists($helper, 'requireDatabaseVersion')) {
			$result = $helper->requireDatabaseVersion($databaseConfig);
			if($result['success']) {
				return true;
			} else {
				$testDetails[2] .= $result['error'];
				$this->warning($testDetails);
				return false;
			}
		}
		// Skipped test because this database has no required version
		return true;
	}

	function requireDatabaseServer($databaseConfig, $testDetails) {
		$this->testing($testDetails);
		$helper = $this->getDatabaseConfigurationHelper($databaseConfig['type']);
		$result = $helper->requireDatabaseServer($databaseConfig);
		if($result['success']) {
			return true;
		} else {
			$testDetails[2] .= ": " . $result['error'];
			$this->error($testDetails);
			return false;
		}
	}
	
	function requireDatabaseOrCreatePermissions($databaseConfig, $testDetails) {
		$this->testing($testDetails);
		$helper = $this->getDatabaseConfigurationHelper($databaseConfig['type']);
		$result = $helper->requireDatabaseOrCreatePermissions($databaseConfig);
		if($result['success']) {
			if($result['alreadyExists']) $testDetails[3] = "Database $databaseConfig[database]";
			else $testDetails[3] = "Able to create a new database";
			$this->testing($testDetails);
			return true;
		} else {
			if(!@$result['cannotCreate']) {
				$testDetails[2] .= ". Please create the database manually.";
			} else {
				$testDetails[2] .= " (user '$databaseConfig[username]' doesn't have CREATE DATABASE permissions.)";
			}
			
			$this->error($testDetails);
			return false;
		}
	}
	
	function getInstalledThemes() {
		$themes = array();
		if (is_dir('themes')) {
			chdir('themes');
			$t_themes = glob('*',GLOB_ONLYDIR);
			chdir('../');
			
			// cleanup...
			foreach ($t_themes as $th) {
				if (strpos($th, '_') === false && substr($th,0,1) != '.') {
					$themes[] = $th;
				}
			}
		}

		return $themes;
	}
	
	function getAllDatabases($databaseConfig, $testDetails) {
		$this->testing($testDetails);

		try {
			require_once FRAMEWORK_NAME.'/model/DB.php';
			require_once FRAMEWORK_NAME.'/model/Query.php';
			require_once FRAMEWORK_NAME.'/model/MySQLQuery.php';
			require_once FRAMEWORK_NAME.'/model/Database.php';
			require_once FRAMEWORK_NAME.'/model/MySQLDatabase.php';
			require_once 'postgresql/code/PostgreSQLDatabase.php';
			if (file_exists('mssql/code/MSSQLDatabase.php')) require_once 'mssql/code/MSSQLDatabase.php';
			
			// get tmp connection
			$dbClass = '\\'.$databaseConfig['type'];
			
			// if pg, we connect to 'Postgres' database
			$tmpConfig = $databaseConfig;
			if ($tmpConfig['type'] == 'PostgreSQLDatabase') {
				$tmpConfig['database'] = 'postgres';
			}
			$tmpConn = new $dbClass($tmpConfig);
			
			$forbiden_dbs = array('postgres');
			$databases = array_diff($tmpConn->allDatabaseNames(), $forbiden_dbs);
			$this->testing($testDetails);
			return $databases;
			
		} catch (Exception $e) {
			$this->error($testDetails);
			return false;
		}
	}

	function requireServerVariables($varNames, $errorMessage) {
		//$this->testing($testDetails);
		foreach($varNames as $varName) {
			if(!$_SERVER[$varName]) $missing[] = '$_SERVER[' . $varName . ']';
		}
		if(!isset($missing)) {
			return true;
		} else {
			$testDetails[2] .= " (the following PHP variables are missing: " . implode(", ", $missing) . ")";
			$this->error($testDetails);
		}
	}

	function isRunningWebServer($testDetails) {
		$this->testing($testDetails);
		if($testDetails[3]) {
			return true;
		} else {
			$this->warning($testDetails);
			return false;
		}
	}

	// Must be PHP4 compatible
	var $baseDir;
	function getBaseDir() {
		// Cache the value so that when the installer mucks with SCRIPT_FILENAME half way through, this method
		// still returns the correct value.
		if(!$this->baseDir) $this->baseDir = realpath(dirname($_SERVER['SCRIPT_FILENAME']) . '/../../') . DIRECTORY_SEPARATOR;
		return $this->baseDir;
	}
	
	function testing($testDetails) {
		if(!$testDetails) return;
		
		$section = $testDetails[0];
		$test = $testDetails[1];
		
		$message = "OK";
		if(isset($testDetails[3])) $message .= " ($testDetails[3])";

		$this->tests[$section][$test] = array("good", $message);
	}
	
	function error($testDetails) {
		$section = $testDetails[0];
		$test = $testDetails[1];

		$this->tests[$section][$test] = array("error", $testDetails[2]);
		$this->errors[] = $testDetails;
	}
	
	function warning($testDetails) {
		$section = $testDetails[0];
		$test = $testDetails[1];

		$this->tests[$section][$test] = array("warning", $testDetails[2]);
		$this->warnings[] = $testDetails;
	}
	
	function hasErrors() {
		return sizeof($this->errors);
	}
	
	function hasWarnings() {
		return sizeof($this->warnings);
	}
	
}

class Installer extends InstallRequirements {
	function __construct() {
		// Cache the baseDir value
		$this->getBaseDir();
	}
	
	function install($config) {
		global $base_href; 
?>
<html>
	<head>
		<title>Installing SilverStripe...</title>
		<base href="<?php echo $base_href;?>"/>
		<link rel="stylesheet" type="text/css" href="mysite/css/layout.css" />
		<link rel="stylesheet" type="text/css" href="mysite/css/typography.css" />
		<link rel="stylesheet" type="text/css" href="mysite/css/form.css" />
		<link rel="stylesheet" type="text/css" href="innovatif/css/install.css" />
		<script src="<?php echo FRAMEWORK_NAME; ?>/thirdparty/jquery/jquery.js"></script>
	</head>
	<body>
		<div id="BgContainer">
			<div id="Container">
				<div id="Header">
					<h1>SilverStripe CMS Installation</h1>
				</div>

				<div id="Navigation">&nbsp;</div>
				<div class="clear"><!-- --></div>

				<div id="Layout">
					<div class="typography">
						<h1>Installing SilverStripe...</h1>
						<p>I am now running through the installation steps (this should take about 30 seconds)</p>
						<p>If you receive a fatal error, refresh this page to continue the installation</p>
						<ul>
<?php
		$webserver = $this->findWebserver();
		$isIIS = $this->isIIS(''); // any version!
		$isApache = $this->isApache();
		
		flush();
		
		if(file_exists('mysite/_config.php')) {
			// Truncate the contents of _config instead of deleting it - we can't re-create it because Windows handles permissions slightly
			// differently to UNIX based filesystems - it takes the permissions from the parent directory instead of retaining them
			$fh = fopen('mysite/_config.php', 'wb');
			fclose($fh);
		}
		
		$locale = isset($_POST['locale']) ? addcslashes($_POST['locale'], "\'") : 'en_US';
		$type = $config['db']['type'];
		$dbConfig = $config['db'][$type];
		$theme = $config['theme'];
		$sitemode = $config['mode'];
		$forceSLL = ($config['forcesecure'] == 'yes');
		$useLDAP = ($config['ldap'] == 'yes');
		$logdir = $config['logdir'];
		$logprefix = $config['logprefix'];
		
		if(!$dbConfig) {
			echo "<p style=\"color: red\">Bad config submitted</p><pre>";
			print_r($config);
			echo "</pre>";
			die();
		}
		
		// Write the config file
		$this->statusMessage("Setting up 'mysite/_config.php'...");
		$escapedPassword = addslashes($dbConfig['password']);
		
		$configTxt = <<<PHP
<?php
global \$project;
\$project = 'mysite';

global \$databaseConfig;
\$databaseConfig = array(
	"type" => '{$type}',
	"server" => '{$dbConfig['server']}',
	"username" => '{$dbConfig['username']}',
	"password" => '{$escapedPassword}',
	"database" => '{$dbConfig['database']}',
);

// Set the site locale
i18n::set_locale('$locale');

// Set the sites default translatable locale
Translatable::set_default_locale('$locale');

// Set site mode [dev|test|live]
Director::set_environment_type('{$sitemode}');

// Get the environment
\$INN_environment = \\innovatif\\Environment::get();

PHP;
	
		if ($type == 'MySQLDatabase') {
			$configTxt .= <<<PHP

MySQLDatabase::set_connection_charset('utf8');

PHP;
		}
		
		if ($theme != 'NONE') {
			$configTxt .= <<<PHP

// Setting the theme			
SSViewer::set_theme('$theme');

PHP;
		}

		if ($forceSLL) {
			$configTxt .= <<<PHP

/**
 * Force SSL for admin and security. Uses secure URL from _ss_environment.
 * You can also pass custom secure URL to the function!
 * Or remove the line, if you don't wish to use secure admin.
 */
\$INN_environment->forceSecureAdmin();

PHP;
		}
		
		if ($useLDAP) {
			$configTxt .= <<<PHP

/**
 * Use LDAP, with settings from _ss_environment.
 * You can also pass custom settings to the function!
 * Or remove the line, if you don't wish to use LDAP
 */
\$INN_environment->configureLdapAuth();

PHP;
		}
		
		if (!empty($logdir)) {
			$configTxt .= <<<PHP

/** 
 * Logging
 * 1. SS logging (errors, warnings and notices)
 * 2. fallback to PHP
 */
SS_Log::add_writer(new SS_LogFileWriter('{$logdir}{$logprefix}SilverStripe.log'), SS_Log::NOTICE, '<=');
 
ini_set('log_errors', 'On');
ini_set('error_log', '{$logdir}{$logprefix}PHP_error.log');

PHP;
		}
		
		$configTxt .= <<<PHP

// require mysite/_config_common.php if exists.
if (Director::fileExists("\$project/_config_common.php")) require_once "../\$project/_config_common.php";

PHP;
		
		$this->writeToFile("mysite/_config.php", $configTxt);

		// Write the appropriate web server configuration file for rewriting support
		if($this->hasRewritingCapability()) {
			if($isApache) {
				$this->statusMessage("Setting up '.htaccess' file...");
				$this->createHtaccess();
			} elseif($isIIS) {
				$this->statusMessage("Setting up 'web.config' file...");
				$this->createWebConfig();	
			}
		}

		// Load the SilverStripe runtime
		$_SERVER['SCRIPT_FILENAME'] = dirname(realpath($_SERVER['SCRIPT_FILENAME'])) . '/' . FRAMEWORK_NAME . '/main.php';
		chdir(FRAMEWORK_NAME);

		// Rebuild the manifest
		$_GET['flush'] = true;
		// Show errors as if you're in development mode 
		$_SESSION['isDev'] = 1;
		
		$this->statusMessage("Building database schema...");

		require_once 'core/Core.php';
		
		
		// Build database
		$con = new \Controller();
		$con->pushCurrent();

		global $databaseConfig;
		\DB::connect($databaseConfig);
		
		$dbAdmin = new \DatabaseAdmin();
		$dbAdmin->init();
		
		$dbAdmin->doBuild(true);
		
		
		if (isset($config['admin']['do']) && $config['admin']['do']) {
			
			// Create default administrator user and group in database 
			// (not using Security::setDefaultAdmin())
			$adminMember = \Security::findAnAdministrator();
			
			// if member is already set, get new one.
			if (!empty($adminMember->Email) && $adminMember->Email != $config['admin']['username']) {
				// maybe we already have a member...
				$adminMember = \Member::get_one('Member', '"Email"=\''.\Convert::raw2sql($config['admin']['username']).'\'');
				
				if (empty($adminMember)) {
					// no, we don't have one, create it.
					$adminMember = \Object::create('Member');
					$adminMember->FirstName = $config['admin']['username'];
					$adminMember->write();
				}
			
				// make it admin.	
				$adminMember->addToGroupByCode('administrators');
			}  
			
			// and update it.
			$adminMember->FirstName = $config['admin']['username'];
			$adminMember->Email = $config['admin']['username'];
			$adminMember->Password = $config['admin']['password'];
			$adminMember->PasswordEncryption = \Security::get_password_encryption_algorithm();
			
			// @todo Exception thrown if database with admin already exists with same Email
			try {
				$adminMember->write();
			} catch(Exception $e) {
			}
		}
		
		// Syncing filesystem (so /assets/Uploads is available instantly, see ticket #2266)
		// show a warning if there was a problem doing so
		try {
			$this->statusMessage('Creating initial filesystem assets...');
			\Filesystem::sync();
		} catch(Exception $e) {
			$this->statusMessage(
				sprintf('Warning: Creating initial filesystem assets failed (error: %s)', $e->getMessage())
			);
		}

		
		if (isset($config['admin']['do']) && $config['admin']['do']) {
			$_SESSION['username'] = $config['admin']['username'];
			$_SESSION['password'] = $config['admin']['password'];
		}

		if(!$this->errors) {
			if(isset($_SERVER['HTTP_HOST']) && $this->hasRewritingCapability()) {
				$this->statusMessage("Checking that friendly URLs work...");
				$this->checkRewrite();
			} else {
				$destinationURL = 'index.php/' .
					($this->checkModuleExists('cms') ? 'home/successfullyinstalled?flush=1' : '?flush=1');

				echo <<<HTML
				<li>SilverStripe successfully installed; I am now redirecting you to your SilverStripe site...</li>
				<script>
					setTimeout(function() {
						window.location = "$destinationURL";
					}, 2000);
				</script>
				<noscript>
				<li><a href="$destinationURL">Click here to access your site.</li>
				</noscript>
HTML;
			}
		}
		
		return $this->errors;
	}
	
	function writeToFile($filename, $content) {
		$base = $this->getBaseDir();
		$this->statusMessage("Setting up $base$filename");

		if((@$fh = fopen($base . $filename, 'wb')) && fwrite($fh, $content) && fclose($fh)) {
			return true;
		} else {
			$this->error("Couldn't write to file $base$filename");
		}
	}
	
	function createHtaccess() {
		$start = "### SILVERSTRIPE START ###\n";
		$end = "\n### SILVERSTRIPE END ###";
		
		$modulePath = FRAMEWORK_NAME;
		$rewrite = <<<TEXT
<Files *.ss>
	Order deny,allow
	Deny from all
	Allow from 127.0.0.1
</Files>

<Files innovatif/installer/*>
	Order deny,allow
	Deny from all
</Files>
<Files innovatif/hooks/*>
	Order deny,allow
	Deny from all
</Files>

<Files web.config>
	Order deny,allow
	Deny from all
</Files>

ErrorDocument 404 /assets/error-404.html
ErrorDocument 500 /assets/error-500.html

<IfModule mod_alias.c>
	RedirectMatch 403 /silverstripe-cache(/|$)
</IfModule>

<IfModule mod_rewrite.c>
	SetEnv HTTP_MOD_REWRITE On
	RewriteEngine On

	RewriteCond %{REQUEST_URI} ^(.*)$
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule .* $modulePath/main.php?url=%1&%{QUERY_STRING} [L]
</IfModule>
TEXT;
		
		if(file_exists('.htaccess')) {
			$htaccess = file_get_contents('.htaccess');
			
			if(strpos($htaccess, '### SILVERSTRIPE START ###') === false && strpos($htaccess, '### SILVERSTRIPE END ###') === false) {
				$htaccess .= "\n### SILVERSTRIPE START ###\n### SILVERSTRIPE END ###\n";
			}
		
			if(strpos($htaccess, '### SILVERSTRIPE START ###') !== false && strpos($htaccess, '### SILVERSTRIPE END ###') !== false) {
				$start = substr($htaccess, 0, strpos($htaccess, '### SILVERSTRIPE START ###')) . "### SILVERSTRIPE START ###\n";
				$end = "\n" . substr($htaccess, strpos($htaccess, '### SILVERSTRIPE END ###'));
			}
		}
		
		$this->writeToFile('.htaccess', $start . $rewrite . $end);
	}
	
	/**
	 * Writes basic configuration to the web.config for IIS
	 * so that rewriting capability can be use.
	 */
	function createWebConfig() {
		$modulePath = FRAMEWORK_NAME;
		$content = <<<TEXT
<?xml version="1.0" encoding="utf-8"?>
<configuration>
	<location path="innovatif" allowOverride="false">
		<system.webServer>
			<security>
				<requestFiltering>
					<hiddenSegments applyToWebDAV="false">
						<add segment="installer" />
						<add segment="hooks" />
					</hiddenSegments>
				</requestFiltering>
			</security>
		</system.webServer>
	</location>	
	<system.webServer>
		<security>
			<requestFiltering>
				<hiddenSegments applyToWebDAV="false">
					<add segment="silverstripe-cache" />
				</hiddenSegments>
			</requestFiltering>
		</security>
		<rewrite>
			<rules>
				<rule name="SilverStripe Clean URLs" stopProcessing="true">
					<match url="^(.*)$" />
					<conditions>
						<add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
					</conditions>
					<action type="Rewrite" url="$modulePath/main.php?url={R:1}" appendQueryString="true" />
				</rule>
			</rules>
		</rewrite>
	</system.webServer>
</configuration>
TEXT;
		
		$this->writeToFile('web.config', $content);
	}
	
	function checkRewrite() {
		$destinationURL = ($this->checkModuleExists('cms') ? 'home/successfullyinstalled?flush=1' : '?flush=1');

		echo <<<HTML
<li id="ModRewriteResult">Testing...</li>
<script>
	if(typeof $ == 'undefined') {
		document.getElemenyById('ModeRewriteResult').innerHTML = "I can't run jQuery ajax to set rewriting; I will redirect you to the homepage to see if everything is working.";
		setTimeout(function() {
			window.location = "$destinationURL";
		}, 10000);
	} else {
		$.ajax({
			method: 'get',
			url: 'InstallerTest/testrewrite',
			complete: function(response) {
				var r = response.responseText.replace(/[^A-Z]?/g,"");
				if(r === "OK") {
					$('#ModRewriteResult').html("Friendly URLs set up successfully; I am now redirecting you to your SilverStripe site...")
					setTimeout(function() {
						window.location = "$destinationURL";
					}, 2000);
				} else {
					$('#ModRewriteResult').html("Friendly URLs are not working. This is most likely because a rewrite module isn't configured "
						+ "correctly on your site. You may need to get your web host or server administrator to do this for you: "
						+ "<ul>"
						+ "<li><strong>mod_rewrite</strong> or other rewrite module is enabled on your web server</li>"
						+ "<li><strong>AllowOverride All</strong> is set for the directory where SilverStripe is installed</li>"
						+ "</ul>");
				}
			}
		});
	}
</script>
<noscript>
	<li><a href="$destinationURL">Click here</a> to check friendly URLs are working. If you get a 404 then something is wrong.</li>
</noscript>
HTML;
	}
	
	function var_export_array_nokeys($array) {
		$retval = "array(\n";
		foreach($array as $item) {
			$retval .= "\t'";
			$retval .= trim($item);
			$retval .= "',\n";
		}
		$retval .= ")";
		return $retval;
	}
	
	/**
	 * Show an installation status message.
	 */
	function statusMessage($msg) {
		echo "<li>$msg</li>\n";
		flush();
	}
}
