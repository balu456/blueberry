$(document).ready(function() {
	/**
	 * Returns next (cnt) form field
	 */
	$.fn.nextInputField = function(cnt) {
    	var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,select');
    	var index = fields.index( this );
    	if (typeof cnt == 'undefined') cnt=1;
    	if ( index > -cnt && ( index + cnt ) < fields.length ) {
    		var ret = fields.eq( index + cnt ); 
        	return $(ret);
    	}
    	return false;
	};

	// Toggle disabled for reuse database
	$('.reusedb').change(function(e) {
		var a = $(this).nextInputField();
		if ($(this).val()=='0') {
			a.removeAttr('readonly').focus();
		} else {
			a.attr('readonly',true);
		}
	});
	$('.reusedb').trigger('change');
	
	// Toggle admin fields
	$('#admin_do').change(function(e) {
		if ($(this).attr('checked')) {
			$(this).nextInputField().removeAttr('disabled');
			$(this).nextInputField(2).removeAttr('disabled');
		} else {
			$(this).nextInputField().attr('disabled',true);
			$(this).nextInputField(2).attr('disabled',true);
		}
	});
	$('#admin_do').trigger('change');
	
	// Toggle SSL mode
	$('input:radio[name=mode]').change(function (e) {
		if ($('input:radio[name=mode]:checked').val()=='live') {
			$('input:radio[name=forcesecure]').filter('[value="yes"]').attr('checked',true);
			$('input:radio[name=forcesecure]:not(:checked)').attr('disabled', true);
		} else {
			$('input:radio[name=forcesecure]').removeAttr('disabled');
		}
	});
	$('input:radio[name=mode]').trigger('change');
	
	
	/**
	 * If database was selected from dropdown, copy the value...
	 */
	$('form').submit(function(e) {
		if ($('.reusedb') && $('.reusedb').val() != '0') {
			$('.reusedb').nextInputField().val($('.reusedb').val());
		}
	});
	
	/**
	 * Hide all existing database warnings, and show only current one
	 */
	$('#database_selection li label, #database_selection input:radio').click(function(e) {
		$('.dbfields').hide();
		// only show fields if there's no db error
		if(!$('.databaseError', $(this).parent()).length) $('.dbfields', $(this).parent()).show();
		$('.databaseError').hide();
		$('.databaseError', $(this).parent()).show();
	});
	
	// Select first
	$('#database_selection li input:checked').siblings('label').click();
	
	/**
	 * Install button
	 */
	$('#reinstall_confirmation').click(function() {
		$('#install_button').attr('disabled', !$(this).is(':checked'));
	});
	
	$('#install_button').click(function() {
		$('#saving_top').hide(); 
		$(this).val('Installing SilverStripe...');
	});
	
	/**
	 * Show all the requirements 
	 */
	$('h5.requirement a').click(function() {
		if($(this).text() == 'Hide All Requirements') {
			// hide the shown requirements
			$(this).parents('h5').next('table.testResults').find('.good').hide();
			$(this).text('See All Requirements');
		}
		else {
			// show the requirements.
			$(this).parents('h5').next('table.testResults').find('.good').show();
			$(this).text('Hide All Requirements');			
		}
		
		return false;
	});
});