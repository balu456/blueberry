<?php
namespace innovatif;
/*********************************************************************************
 * DEFAULTS for installer. Please read and check all bellow
 ********************************************************************************/

// Defaults for database connection
const DEF_DATABASECLASS		= 'PostgreSQLDatabase';
const DEF_DATABASESERVER	= 'localhost';
// Default username, %s resolved to project name
const DEF_DATABASEUSERNAME	= '';
const DEF_DATABASEPASSWORD	= '';

// Database name, %s resolved to project name
const DEF_DATABASENAME		= '%s';

// Create/update admin? bool 
const DEF_ADMINDO			= false;
// Default username and password for create/update admin
const DEF_ADMINUSERNAME		= 'USERNAME';
const DEF_ADMINPASSWORD		= '';

// Default locale
const DEF_LOCALE			= 'sl_SI';

// Default site mode [dev|test|live]
const DEF_SITEMODE			= 'live';

// Default force SSL [true|false]
const DEF_FORCESSL			= true;

// Default use LDAP [true|false]
const DEF_USELDAP			= true;

/**
 * Default log directory location, relative to project webdir (BASE_PATH)
 * It can be also absolute, if it begins with '\' or has ':' on second char.
 
 * %s will be replaced with projectfolder name.
 *
 * usually: '../logs/'
 */
const DEF_LOGDIR			= '../logs/';

/**
 * Default logfiles prefix
 * If you have a common log directory, you better have some prefix
 * 
 * eg.: '%s_'
 *  
 * %s will be replaced with projectfolder name.
 */
const DEF_LOGPREFIX			= '';

