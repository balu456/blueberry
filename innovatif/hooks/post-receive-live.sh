#!/usr/bin/env bash
#
# The "post-receive" script is run after receive-pack has accepted a pack
# and the repository has been updated.  It is passed arguments in through
# stdin in the form
#  <oldrev> <newrev> <refname>
# For example:
#  aa453216d1b3e49e7f6f98441fa56946ddcd6a20 68f7abf4e6f922807889f52bc043ecd31b79f814 refs/heads/master
#
# Live post receive should:
# 1. get the last live tag of site branch
# 2. compare it with checkedout live tag
# 3. if newer
# 4. 	tmp upgrade msg
# 5. 	checkout
# 6.	dev/build
# 7. 	undo upgrade msg
# 8. 	write new live tag
# 9. end if

# working dir is repo dir.

# branch name
branch="site"

# webdir
webdir="../webdir"

# tag prefix
tag_prefix="LIVE*"


# find tag
describe=$(git describe --tags --long --always --match=$tag_prefix $branch 2>&1)  

# error...
if [[ $? != 0 ]]; then
	echo "Branch $branch doesn't exits"
	exit 1
fi

# not found?
if [[ `expr index "$describe" -` -eq 0 ]] 
then 
	echo "Tag with prefix $tag_prefix not found."
	exit 1
fi

last_commit=${describe##*-g}
tmp_str=${describe%-*}
last_commit2tag=${tmp_str##*-}
last_tag=${tmp_str%-*}

# resolve tag to hash
last_tag_hash=$(git show-ref --tags --hash --verify --quiet refs/tags/$last_tag)

# error...
if [[ $? != 0 ]]; then
	echo "Error resolving $last_tag"
	exit 1
fi


if [[ -f HEAD ]]; then
	last_checkout=$(cat HEAD)
	if [[ $last_tag_hash == $last_checkout ]]; then
		echo "Branch $branch already on last tag $last_tag."
		exit 0
	fi
fi

# TODO upgrade MSG

# checkout

if [[ -d $webdir ]]; then
	umask 0002
	checkout_output=$(GIT_WORK_TREE=$webdir git checkout -f -q $last_tag_hash 2>&1)
	if [[ $? != 0 ]]; then
		echo "Checkout error."
		echo $checkout_output
		exit 1
	fi
	
	cd $webdir
	build_output=$(php sapphire/cli-script.php dev/build flush=1)
	echo "Checkedout new version $last_tag"
	echo $build_output
else
	echo "Folder $webdir doesn't exist, can't checkout"
fi

# TODO back to site

