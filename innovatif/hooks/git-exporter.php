#!/usr/bin/env bash
#
# The "post-receive" script is run after receive-pack has accepted a pack
# and the repository has been updated.  It is passed arguments in through
# stdin in the form
#  <oldrev> <newrev> <refname>
# For example:
#  aa453216d1b3e49e7f6f98441fa56946ddcd6a20 68f7abf4e6f922807889f52bc043ecd31b79f814 refs/heads/master
#
# Live post receive should:
# 1. get the last live tag of site branch
# 2. compare it with checkedout live tag
# 3. if newer
# 4. 	tmp upgrade msg
# 5. 	checkout
# 6.	dev/build
# 7. 	undo upgrade msg
# 8. 	write new live tag
# 9. end if

# working dir is repo dir.





# TODO upgrade MSG

# checkout

if [[ -d $webdir ]]; then
	checkout_output=$(GIT_WORK_TREE=$webdir git checkout -f -q $last_tag_hash 2>&1)
	if [[ $? != 0 ]]; then
		echo "Checkout error."
		echo "$checkout_output"
		exit 0
	fi
  echo "Checkedout new version $last_tag"
  cd $webdir
  umask 0002
  echo "$(php sapphire/cli-script.php dev/build flush=1)"
else
	echo "Folder $webdir doesn't exist, can't checkout"
fi

# TODO back to site




# Find SHA-1 of last commit of branch
# param $1  branchname
# return COMMIT2CHECKOUT
function lastCommitOfBranch
{
  COMMIT2CHECKOUT=$(git log --format=format:%H -1 heads/$1 2>&1)  

  if [[ $? != 0 ]]; then
    echo "Branch $branch doesn't exits"
    exit 0
  fi
}

# Find SHA-1 of last Tag on brancname that matches TagPrefix
# param $1  branchname
# param $2  tagprefix
# return COMMIT2CHECKOUT
function lastCommitFromTagPrefix
{
  # find tag
  describe=$(git describe --tags --long --always --match=$tag_prefix $branch 2>&1)  

  # error...
  if [[ $? != 0 ]]; then
    echo "Branch $branch doesn't exits"
    exit 1
  fi

  # not found?
  if [[ `expr index "$describe" -` -eq 0 ]] 
  then 
    echo "Tag with prefix $tag_prefix not found."
    exit 1
  fi

  #resolve description into tage name
  
  # tag name with number of commits.
  tmp_str=${describe%-*}

  # last matching tag name  
  last_tag=${tmp_str%-*}

  # resolve tag to hash
  COMMIT2CHECKOUT=$(git show-ref --tags --hash --verify --quiet refs/tags/$last_tag)

  # error...
  if [[ $? != 0 ]]; then
    echo "Error resolving $last_tag"
    exit 1
  fi
  
}

# Check if we are already on last commit
# param $1  commit to check
function alreadyOnLastCommit
{
  if [[ -f HEAD ]]; then
    last_checkout=$(cat HEAD)
    if [[ $1 == $last_checkout ]]; then
      echo "No need to check out, already there."
      exit 0
    fi
  fi
}

# check out 
# param $1  SHA-1 commit to checkout
# param $2  folder to checkout to.
# uses $QUIET
function checkout
{
  if [[ -d $2 ]]; then
    local CHECKOUT_OUTPUT=$(GIT_WORK_TREE=$2 git checkout -f -q $1 2>&1)
    if [[ $? != 0 && ! $QUIET ]]; then
      echo "Checkout error."
      echo "$checkout_output"
      exit 0
    fi
    echo "Checkedout new version $1"
  else
    echo "Folder $2 doesn't exist, can't checkout"
    return 1
  fi    
}

# rebuild the database and flush
# param $1 webdir
# uses $QUIET
function rebuild()
{
  # we need to move to webdir and set umask
  local OLD_DIR=$(pwd)
  cd $webdir
  umask 0002
  local BUILD_OUTPUT="$(php sapphire/cli-script.php dev/build flush=1)"
  if [ ! $QUIET ]
  then
    echo "$BUILD_OUTPUT"
  fi
  cd $OLD_DIR
}



function usage
{
  echo "Usage: git-ss-exporter.sh [-b branchName] [-w workingDir] [-d|--norebuild] [[-l|--last] | [-t tagPrefix]]"
  echo "  -b branchName"
  echo "      Branch name to operate on. Defaults to 'site'"
  echo "  -w workingDir"
  echo "      Working dir to export to. Defaults to '../webdir'"
  echo "  -l,--last"
  echo "      Export last commit on branch."
  echo "  -t tagPrefix"
  echo "      Export last tag with tagPrefix. Defaults to 'LIVE*'. This is default behaviour."
  echo "  -d,--norebuild"
  echo "      Don't run Silverstripe dev/build after export."
}


##### Main

# branch name
branch="site"

# webdir
webdir="../webdir"

# tag prefix
tag_prefix="LIVE*"

# last commit
lastcommit=0

# no rebuild 
norebuild=0

while [ "$1" != "" ]; do
    case $1 in
        -b )                    shift
                                branch=$1
                                ;;
        -w )                    shift                    
                                webdir=$1
                                ;;
        -t )                    shift                    
                                tag_prefix=$1
                                ;;
        -l | --last )           lastcommit=1
                                ;;
        -d | --norebuild )      norebuild=1
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


