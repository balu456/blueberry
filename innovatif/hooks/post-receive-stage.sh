#!/usr/bin/env bash
#
# The "post-receive" script is run after receive-pack has accepted a pack
# and the repository has been updated.  It is passed arguments in through
# stdin in the form
#  <oldrev> <newrev> <refname>
# For example:
#  aa453216d1b3e49e7f6f98441fa56946ddcd6a20 68f7abf4e6f922807889f52bc043ecd31b79f814 refs/heads/master
#
# Stage post receive should:
# 1. get the last commit of site branch
# 2. compare it with checkedout commit (HEAD)
# 3. if newer
# 4. 	tmp upgrade msg
# 5. 	checkout
# 6.	dev/build
# 7. 	undo upgrade msg
# 8. 	write new commit
# 9. end if

# working dir is repo dir.

# branch name
branch="site"

# webdir
webdir="../webdir"

last_branch_commit=$(git log --format=format:%H -1 heads/$branch 2>&1)  

if [[ $? != 0 ]]; then
	echo "Branch $branch doesn't exits"
	exit 0
fi

if [[ -f HEAD ]]; then
	last_checkout=$(cat HEAD)
	if [[ $last_branch_commit == $last_checkout ]]; then
		echo "Branch $branch already on last commit."
		exit 0
	fi
fi

# TODO upgrade MSG

# checkout

if [[ -d $webdir ]]; then
	umask 0002
	checkout_output=$(GIT_WORK_TREE=$webdir git checkout -f -q $last_branch_commit 2>&1)
	if [[ $? != 0 ]]; then
		echo "Checkout error."
		echo $checkout_output
		exit 0
	fi
	
	cd $webdir
	build_output=$(php sapphire/cli-script.php dev/build flush=1)
	echo "Checkedout new version."
	echo $build_output
else
	echo "Folder $webdir doesn't exist, can't checkout"
fi

# TODO back to site



		
