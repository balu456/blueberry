<?php
/**
 * Git deployer for INNOVATIF sites.
 * 
 * Implements:
 * Dev site:
 * 	- checkout (only site or production_*)
 * 	- branch (only from Stage or production_*)
 * 	- refreshstage: pull Stage (only if on stage)
 * 	- stage: push Stage (only if on stage)
 *  - merge (only if on stage or production_*, only from production_*)
 *  - commit
 * 
 * Stage site:
 * 	- checkout (only site and site tags)
 * 	- pushtolive (only if on stage, push live)
 * 	- publish (only if on stage, create tag, pushtolive)
 * 	- fetch live tags.
 *  - 
 * 
 *  Live site:
 *  - show tags (site live*)
 *  - show history (site branch)
 *  - switch to commit (create tag on site branch, checkout tag)
 * 	- checkout tag (site live*)
 *  - 
 * 	- on receive: if we receive newer live tag, check it out.
 * 
 */
class INN_Deploy {
	
	static protected $environment_type;
	
	public function pull () {
		
	}
	
	public function push () {
		
	}
	
	public function merge() {
		
	}
	
}

class INN_Git {
	protected static $git_pathname = '/usr/bin/git';
	
	protected static $repository = null;
	protected static $workdir = null;
	protected static $is_bare = false;
	
		
	public static function git ($command, $args, $repo = null) {
		if (!is_null($repo)) {
			self::setRepository($repo);
		}
		if (is_null(self::$git_repo)) {
			user_error('Repository not set.');
			return false;			
		}
		
		
		
	}
	
	public static function setRepository ($repository) {
		self::$repository = $repository;
	}
	public static function setWorkdir ($workdir) {
		self::$workdir = $workdir;
	}
	public static function setBare ($is_bare) {
		self::$is_bare = $is_bare;
	}
	
	public static function setEnvironment ($workdir,$repository=null,$is_bare=false) {
		self::setWorkdir($workdir);
		self::setRepository(is_null($repository)? $workdir : $repository);
	}

}


class INN_sys {
	
}
	
class INN_proc {
  const 
	protected $process = null;
	
	// stdin is a pipe that the child will read from
	protected $IO = array (
		0 => array("pipe", "r"), // stdin is a pipe that the child will read from
		1 => array("pipe", "w"), // stdout is a pipe that the child will write to
		2 => array("pipe", "w")  // stderr is a pipe that the child will write to 	
	);
	
	protected $cwd = null;
	
	protected $env = null;
	
	
	public function __construct($command, $cwd, $env, $stdin = null, $stdout = null, $stderr = null) {
		
		
//		$this
		
	}
	
	public function setIO(int $resource, array $desc) {
		$this->IO[$resource] = $desc;
	}
	
	public function setStdIn (array $desc) {
		$this->IO[0] = $desc;
	}
	public function setStdOut (array $desc) {
		$this->IO[1] = $desc;
	}
	public function setStdErr (array $desc) {
		$this->IO[2] = $desc;
	}

	public static function execute ($command,$cwd,$env) {
		$descriptorspec = array(
		);

		$process = proc_open($command, $descriptorspec, $pipes, $cwd, $env);		

		if (is_resource($process)) {
		    // $pipes now looks like this:
		    // 0 => writeable handle connected to child stdin
		    // 1 => readable handle connected to child stdout
		    // 2 => readable handle connected to child stdout

			if (!empty($stdin)) {
				fwrite($pipes[0], $stdin);	
			}
    		fclose($pipes[0]);

    		$stdout = stream_get_contents($pipes[1]);
			fclose($pipes[1]);
    		$stderr = stream_get_contents($pipes[2]);
			fclose($pipes[2]);
			
		    // It is important that you close any pipes before calling
    		// proc_close in order to avoid a deadlock
    		return proc_close($process);
		}
	}
}
