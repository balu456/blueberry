<?php
class INN_process {
	const FD_STDIN 	= 0;
	const FD_STDOUT = 1;
	const FD_STDERR = 2;
	 
	protected $ptr = null;
	
	protected $pipes = null;
	
	// stdin is a pipe that the child will read from
	protected $IO = array (
		FD_STDIN 	=> array("pipe", "r"), // stdin is a pipe that the child will read from
		FD_STDOUT 	=> array("pipe", "w"), // stdout is a pipe that the child will write to
		FD_STDERR	=> array("pipe", "w")  // stderr is a pipe that the child will write to 	
	);
	
	protected $command = null;
	
	protected $cwd = null;
	
	protected $env = null;
	
	protected $options = null;
	
	protected $output = array (
		FD_STDOUT 	=> null,
		FD_STDERR	=> null 	
	);
	
	protected $exitCode = null;
	
	public function __construct($command, $cwd = null, $env = null, $stdin = null, $stdout = null, $stderr = null, $options = null) {

		$this->command = $command;
		
		if (!is_null($cwd)) $this->cwd = $cwd;

		if (!is_null($env)) $this->env = $env;

		if (!is_null($options)) $this->options = $options;

		if (!is_null($stdin)) $this->setIO(FD_STDIN, $stdin);
		
		if (!is_null($stdout)) $this->setIO(FD_STDOUT, $stdout);

		if (!is_null($stderr)) $this->setIO(FD_STDERR, $stderr);
		
	}
	
	/**
	 * @param	int		one of FD_ enums or bigger for passing data to process
	 * @param	mixed 	$desc	Array with descriptor or stream resource
	 */
	public function setIO(int $resource, $desc) {
		$this->IO[$resource] = $desc;
	}
	
	/**
	 * @
	 */
	public function start() {
		if (is_null($this->ptr)) {
			$this->ptr = proc_open($this->command, $this->IO, $this->pipes, $this->cwd, $this->env, $this->options);
			
		} else {
			// already open
			return false;
		}
	}

	/**
	 * Send $what to stdin of process.
	 * Make sure, that stdin is writable pipe (it is by default)... 
	 * before using...
	 * 
	 * @param	string	$what to send
	 * @return	bool	success
	 */
	public function send2StdIn($what) {
		if (is_resource($this->ptr) && 
			is_array($this->pipes) && 
			isset($this->pipes[FD_STDIN]) &&
			is_resource($this->pipes[FD_STDIN])) {
				
		
			fwrite($this->pipes[FD_STDIN],$what);
			return true;	
		}
		return false;
	}
	
	/**
	 * Read from $resource from process, and return it
	 * 
	 * @param	int	$resource	FD_STDOUT or FD_STDERR
	 * @param	int	$buffer lenth
	 * @return	string|bool		line read or FALSE
	 */
	public function readLine($resource, $bufferLength = 1024) {
		if (is_resource($this->ptr) && 
			is_array($this->pipes) && 
			isset($this->pipes[$resource]) &&
			is_resource($this->pipes[$resource])) {
				
			$line = fgets($this->pipes[$resource], $bufferLength);
			
			if ($line === FALSE) return false;
			
			if (is_null($this->output[$resource])) {
				$this->output[$resource] = $line;
			} else {
				$this->output[$resource] .= $line;
			}
			return $line;	
		}
		return false;
	}

	/**
	 * Read from $resource from process, and return it
	 * 
	 * @param	int	$resource	FD_STDOUT or FD_STDERR
	 * @return	string|bool		cont read or FALSE
	 */
	public function readOutput($resource) {
		if (is_resource($this->ptr) && 
			is_array($this->pipes) && 
			isset($this->pipes[$resource]) &&
			is_resource($this->pipes[$resource])) {
				
			$content = stream_get_contents($this->pipes[$resource]);
			
			if ($content === FALSE) return false;
			
			if (is_null($this->output[$resource])) {
				$this->output[$resource] = $content;
			} else {
				$this->output[$resource] .= $content;
			}
			return $this->output[$resource];	
		}
		return false;
	}

	public function closePipes() {
		if (is_resource($this->ptr) && 
			is_array($this->pipes)) {
		
		    foreach ($pipes as $pipe) {
		    	fclose($pipe);
		    }
		}	
	}


	public function close() {
		if (is_resource($this->ptr)) {
			return proc_close($this->ptr);
		} 
	}
	
	public function terminate() {
		if (is_resource($this->ptr)) {
			return proc_terminate($this->ptr);
		} 
	}
	
	public function getStdOut() {
		return $this->output[FD_STDOUT];
	}
	
	public function getStdErr() {
		return $this->output[FD_STDERR];
		
	}
	
	public function getStatus() {
		if (is_resource($this->ptr)) {
			return proc_get_status($this->ptr);
		} 
	}
	
	/**
	 * Sync execute command
	 * 
	 * 
	 * @param int	$timeout in seconds
	 */
	public static function execute($command, $stdIn = null, & $stdOut = null, & $stdErr = null, $timeout = 60) {
		$startTime = microtime(true);
		 
		$myProcess = new INN_Process($command);
		
		if (!is_null($stdIn)) $myProcess->send2StdIn($stdIn);
		
		$pstatus = $myProcess->getStatus();
		while ($pstatus['running'] && ($startTime+($timeout*1000000) > microtime(true))) {
			// wait a little longer.
			usleep(10000);
			$pstatus = $myProcess->getStatus();
		}
		
		// read the outputs.
		$stdOut = $myProcess->readOutput(FD_STDOUT);
		$stdErr = $myProcess->readOutput(FD_STDERR);
		
		// close handles.
		$myProcess->closePipes();
		
		if ($pstatus['running']) {
			$myProcess->terminate();
			return -1;
		} else {
			// process has finished, it should close at once...
			return $myProcess->close();
		}
	}
}
