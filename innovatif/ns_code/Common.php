<?php
/**
 * Innovatif common extensions to Silverstripe.
 * 
 * @author blaz
 */
namespace innovatif;
 
class Common {
	
	/**
	 * Force the site to run on SSL, with customSSLBaseURL option.
	 * @see Director::forceCustomSSL
	 * 
	 * @example
	 * INN_Common::forceCustomSSL(array('/^admin/', '/^Security/'),'https://rozle.innovatif.lan/www.silverstripe.lan/');
	 * 
	 * 
	 * @param array		$patterns			Array of regular expressions, matches will be 
	 * 										forcedToSSL. If empty, force entire site. 
	 * @param string	$customSSLBaseURL	Custom base URL, used for forcing SSL
	 */
	public static function forceCustomSSL($patterns = null, $customSSLBaseURL = "") {
		if (\Director::is_cli()) return;
		
		$matched = false;
		if($patterns) {
			// protect portions of the site based on the pattern
			$relativeURL = \Director::makeRelative(\Director::absoluteURL($_SERVER['REQUEST_URI']));
			foreach($patterns as $pattern) {
				if(preg_match($pattern, $relativeURL)) {
					$matched = true;
					break;
				}
			}
		} else {
			// protect the entire site
			$matched = true;
		}
		
		if($matched && (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off')) {
			if ($customSSLBaseURL) {
				$destURL = \Director::makeRelative($_SERVER['REQUEST_URI']);
				if ($destURL[0] != '/') '/'.$destURL;
				$destURL = rtrim($customSSLBaseURL,'/').'/'.$destURL;
			} else {
				$destURL = str_replace('http:', 'https:', \Director::absoluteURL());
			}
			
			if(!headers_sent()) header("Location: $destURL");
			die("<h1>Your browser is not accepting header redirects</h1><p>Please <a href=\"$destURL\">click here</a>");
		} else {
			return false;
		}
	}
	
	/**
	 * Adds Ldap auth_external.
	 * 
	 * @param	$array 	$settings =array(
	 *  					sourceID,
	 *  					niceName,
	 *  					netmask*,
	 *  					hostname,
	 *  					port,
	 *  					enc*,
	 *  					autoadd,
	 *  					options
	 * 						   basedn
	 * 						   ldapversion*
	 * 						   bind_as
	 * 						   bind_pw
	 * 						   extra_attributes*
	 *
	 * 						 * means optional.
	 * @return	bool	success
	 */
	public static function addLdapAuth(array $settings) {
		if (!class_exists('\\ExternalAuthenticator')){
			return false;
		}
		
		// check settings
		$req_settings = array('sourceID','niceName','hostname','port','options','autoadd');
		$req_options = array('basedn','bind_as','bind_pw');
		foreach ($req_settings as $r_setting) {
			if (!isset($settings[$r_setting])) {
				return false;
			}
		}
		foreach ($req_options as $r_option) {
			if (!isset($settings['options'][$r_option])) {
				return false;
			}
		}
		\Authenticator::register_authenticator('ExternalAuthenticator');

		\ExternalAuthenticator::setAuditLogSStripe(true);
		
		\ExternalAuthenticator::createSource($settings['sourceID'],'LDAP',$settings['niceName']);

		if (isset($settings['netmask']) && $settings['netmask']) {
			\ExternalAuthenticator::setValidAddress($settings['sourceID'],$settings['netmask']);
		}

		\ExternalAuthenticator::setUseAnchor(false); 

		\ExternalAuthenticator::setAuthSSLock($settings['sourceID'],false);

		\ExternalAuthenticator::setAuthServer($settings['sourceID'],$settings['hostname']);
		\ExternalAuthenticator::setAuthPort($settings['sourceID'],$settings['port']);

		if (isset($settings['enc']) && $settings['enc']) {
			\ExternalAuthenticator::setAuthEnc($settings['sourceID'], $settings['enc']);
		}

		\ExternalAuthenticator::setAutoAdd($settings['sourceID'], $settings['autoadd']);
		
		foreach($settings['options'] as $oKey => $oVal) {
			\ExternalAuthenticator::setOption($settings['sourceID'], $oKey,$oVal);
		}

		\ExternalAuthenticator::setOption($settings['sourceID'], 'attribute', 'uid');
		\ExternalAuthenticator::setOption($settings['sourceID'], 'firstname_attr', 'givenName');
		\ExternalAuthenticator::setOption($settings['sourceID'], 'surname_attr', 'sn');
		\ExternalAuthenticator::setOption($settings['sourceID'], 'email_attr', 'mail');
		\ExternalAuthenticator::setOption($settings['sourceID'], 'group_attr', 'gidNumber');
		
		return true;
	}

	/**
	 * Sets/resets file to url mapping, used mainly in cli mode, where url is unknown.
	 * 
	 * @param string  $filepath   Filepath
	 * @param string  $url        Url
	 * @return  bool  Success  
	 */
	public static function setFileToUrlMapping ($filepath, $url) {
		global $_FILE_TO_URL_MAPPING;
		if (is_null($filepath)) return false;
		
		$_FILE_TO_URL_MAPPING[$filepath] = $url;
		return true;
	
	}
	
	/**
	 * is_writable rewrite for windows.
	 * 
	 * will work in despite of Windows ACLs bug
	 * NOTE: use a trailing slash for folders!!!
	 * see http://bugs.php.net/bug.php?id=27609
	 * see http://bugs.php.net/bug.php?id=30931
	 * 
	 * @param	string	path
	 * @return	bool	
	 */
	public static function is__writable($path) {
		if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') return is_writable($path);
		
	    if ($path{strlen($path)-1}=='/') // recursively return a temporary file path
	        return self::is__writable($path.uniqid(mt_rand()).'.tmp');
	    else if (is_dir($path))
	        return self::is__writable($path.'/'.uniqid(mt_rand()).'.tmp');
	    // check tmp file for read/write capabilities
	    $rm = file_exists($path);
	    $f = @fopen($path, 'a');
	    if ($f===false)
	        return false;
	    fclose($f);
	    if (!$rm)
	        unlink($path);
	    return true;
	}
	

}