<?php
namespace innovatif;

// Project base path is two folders above us.
define(__NAMESPACE__.'\\BASE_PATH', realpath(dirname(__FILE__).'/../../'));

/**
 * Innovatif autoloader for namespaces code.
 * 
 */
spl_autoload_register('\innovatif\Environment::autoload');

/**
 * Environment
 * 
 * Class used to setup SS in Innovatif environment
 * Uses _ss_environment
 * 
 * Initialization will:
 *  - register our autoloader for namespaced code.
 * 	- try to resolve/find project name (unless manually specified!)
 * 	- try to set File To Url Mapping (if Env_DefProjectURL)
 *  - try to remap assets (if Env_AssetsRemapUNC is set and not on Env_AssetsRemapBlackList)
 *  
 * @singleton 
 * @see 	\innovatif\Common
 * @see 	_ss_environment.php
 * @author  Blaz
 */
class Environment {
	const DEF_PROJECTNAME = 'PROJECTNAME';
	/**
	 * Holds project name - that is specific file folder name for this project.
	 * 
	 * @var	string
	 */
	public $projectName = NULL;
	
	// Is this SS 3
	const IS_SS3 = true;

	// should hardcoded HTML be XHTML compatible, default TRUE - legacy...
	private static $XHTML_compatibility=true;
	
    // Hold an instance of the class
    private static $instance;
    
    // The singleton method
    public static function get($projectName = null) {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c($projectName);
        }

        return self::$instance;
    }
    
    // Prevent users to clone the instance
    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }
	
	/**
	 * A private constructor; prevents direct creation of object
	 * 
	 * It will:
	 * 	- try to resolve/find project name (unless manually specified!) and set
	 * 		this->$projectName 
	 * 	- try to set File To Url Mapping (if Env_DefProjectURL)
	 */
	private function __construct ($projectName = null) {
		if (!empty($projectName)) {
			$this->projectName = $projectName;
		} else {
			$this->projectName = $this->resolveNameFromSSENV();
		}
		if (empty($this->projectName)) $this->projectName = Environment::DEF_PROJECTNAME; 

		// set default url
		if (defined('\\'.__NAMESPACE__.'\\ENV_DEFPROJECTURL'))
			Common::setFileToUrlMapping(BASE_PATH, $this->projectify(ENV_DEFPROJECTURL));
		
	
	}
	
	/**
	 * Returns true, if output should be XHTML compatible. 
	 * Defaults to true.
	 * 
	 * @return bool 
	 */
	public static function isXHTMLcompatible() {
		return self::$XHTML_compatibility;
	}
	
	/**
	 * Set XHTML compatibility for the site
	 * 
	 * @param bool $is_compatible
	 */
	public static function setXHTMLcompatible($is_compatible) {
		self::$XHTML_compatibility = $is_compatible;
	}
	
	/**
	 * Returns slash ("/") if site is to be XHTML compatible.
	 *   
	 * @return string
	 */
	public static function getTagClosing() {
		return self::$XHTML_compatibility ? ' /' : '';
	}
		
	/**
	 * Returns project name - resolved as first directory under directory where environment file resides.
	 * For this to work, SS_ENVIRONMENT_FILE must be defined
	 * 
	 * @return string|bool		Project|directory name or false.	 
	 */
	private function resolveNameFromSSENV () {
		if (!defined('\\SS_ENVIRONMENT_FILE') && !defined(__NAMESPACE__.'\\SS_ENVIRONMENT_FILE')) return false;
		
		$env_dir_rp = dirname(realpath(SS_ENVIRONMENT_FILE));
		$cur_dir_rp = dirname(__FILE__);
		
		if (substr($cur_dir_rp, 0, strlen($env_dir_rp)) != $env_dir_rp) {
			// weird... error
			return false;
		}
		$tmp = explode('/',trim(str_replace('\\','/',substr($cur_dir_rp, strlen($env_dir_rp))),'/'));
		if (count($tmp)>1) {
			return $tmp[0];
		} else {
			return false;
		}
	}
	
	/**
	 * Remaps assets to custom UNC.
	 * 
	 * If param is null, tries to use const Env_AssetsRemapUNC if 
	 * projectName is not in Env_AssetsRemapBlackList
	 * 
	 * Assets remaping must be done very early, before ASSETS_BASE_PATH
	 * is defined in sapphire/Core.php
	 * So only option is when 
	 * 
	 * @param 	string	$remoteBase
	 * @return	bool	success
	 */
	public function remapAssets($remoteBaseUNC = null) {
		
		if (empty($remoteBaseUNC) 
			&& defined('\\'.__NAMESPACE__.'\\ENV_ASSETSREMAPUNC')
			&& (!defined('\\'.__NAMESPACE__.'\\ENV_ASSETSREMAPBLACKLIST')
				|| !in_array($this->projectName, explode(',',ENV_ASSETSREMAPBLACKLIST))
				)
			) {
		
			
			$remoteBaseUNC = $this->projectify(ENV_ASSETSREMAPUNC);
		}

		if (empty($remoteBaseUNC) || !is_dir($remoteBaseUNC)) {
			return false;
		}
		
		if (defined('\\REMOTE_BASE_PATH') || defined('\\ASSETS_BASE_PATH')) {
			return (defined('\\ASSETS_BASE_PATH') && (\ASSETS_BASE_PATH == $remoteBaseUNC));
		}
		
		define('REMOTE_BASE_PATH', $remoteBaseUNC);
		define('ASSETS_BASE_PATH', \REMOTE_BASE_PATH);

		return true;
	}

	/**
	 * Forces /admin and /Security to $secureURLroot.
	 * If param is null, tries to use const Env_SecureURL
	 * Calling causes redirect, if in /admin or /Security URL and not on HTTPS
	 * 
	 * @param	string	$secureURLroot
	 * @return	void
	 */
	public function forceSecureAdmin($secureURLroot = null) {
		if (empty($secureURLroot) && defined('\\'.__NAMESPACE__.'\\ENV_SECUREURL')) {
			$secureURLroot = $this->projectify(ENV_SECUREURL);
		}
		if (!is_null($secureURLroot)) {
			Common::forceCustomSSL(array('/^admin/', '/^Security/'),$secureURLroot);	
		}
	} 

	/**
	 * Configures auth_external LDAP authentication.
	 * if param is null, tries to use const Env_LdapSettings
	 * 
	 * @param	array 	$ldapSettings	@see Common::addLdapAuth
	 * @return	bool	success
	 */
	public function configureLdapAuth($ldapSettings = null) {
		if (empty($ldapSettings) && defined('\\'.__NAMESPACE__.'\\ENV_LDAPSETTINGS')) {
			$ldapSettings = unserialize(ENV_LDAPSETTINGS);
		}
		
		if (is_null($ldapSettings) || !is_array($ldapSettings)) {
			return false;
		}
		return Common::addLdapAuth($ldapSettings);	
	}

	/**
	 * will sprintf $input with projectname
	 * 
	 * @param	string
	 * @return	string
	 */
	public function projectify($input) {
		if (!empty($this->projectName)) {
			return sprintf($input, $this->projectName);
		} else {
			return $input;
		}
	}
	
	/**
	 * Autoloader for namespaced classes
	 * 
	 * It expects file for a class \a\b\class
	 * to be in BASE_PATH/a/ns_code/b/class.php file
	 * 
	 * @author Blaz
	 */
	public static function autoload($className)	{
		//only for namespaced classes!
		if (strpos($className, '\\') === false) return false;
		
		$pathParts = explode('/', trim(str_replace('\\', '/', $className) . '.php','/'),2);
		
		$path = BASE_PATH .'/'. $pathParts[0] . '/ns_code/' . $pathParts[1];  

		@include $path;

		return class_exists($className, false) || interface_exists($className, false);
	}
}
