<?php

class BigInt extends Int {

	function __construct($name, $defaultVal = 0) {
		parent::__construct($name, $defaultVal);
	}


	function requireField() {
		$parts=Array('datatype'=>'bigint', 'precision'=>11, 'null'=>'not null', 'default'=>$this->defaultVal, 'arrayValue'=>$this->arrayValue);
		$values=Array('type'=>'bigint', 'parts'=>$parts);
		DB::requireField($this->tableName, $this->name, $values);
	}
}