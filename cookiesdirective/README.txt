Navodila za uporabo CookiesDirective modula

1. Inštaliraš CookiesDirective module
	- mergaj CookiesDirective_2 oziroma CookiesDirective_3 vejo.
	- poženi dev/build 
	  
2. Preveriš, če spletno mesto uporablja jQuery. Če ga ne, dodaj! jQuery mora bit naložen na vseh straneh! 

3. Dodaš "razlagalne" strani. V vseh jezikih se doda "CookiesDirectiveExplain" stran. Za tekste in umestitev se posvetuj z urednikom? 
   Umestitev strani ni pomembna, pomembna je samo konstanta!
   
4. Kamorkoli na stran (v nogo?, posvetuj se z urednikom) umesti gumb/ikono, ki ponovno odpre popup. HTML je lahko kakršenkoli, 
   OBVEZNO pa mora imeti class="iCD_show". Sistem bo na vse elemente, ki imajo ta class bindal click event, ki odpre popup.

5. Prilagodi nastavitve. Nastaviš lahko štiri konstante (če ne uporabljaš defaultov), to naredi v _config_common.php fajlu. 
	Važna sta predvsem defaultUse, ki nastavi stanje, dokler se uporabnik ne odloči in pa 
	Type of message, ki se veže na tip templata in tekste...
	
/**
 * Default state of using third party...
 * 
 * @var boolean
 */
CookiesDirective_ControllerExtension::$cdDefaultUse = false;

/**
 * Type of message. Default = A
 * 
 */
CookiesDirective_ControllerExtension::$cdType = 'A';

/**
 * Expiration time in days of decision persistent cookie. Default = 90 
 * 
 */
CookiesDirective_ControllerExtension::$cdCookieExpiration = 90;

/**
 * Name of cookie for storing decision
 *
 */
CookiesDirective_ControllerExtension::$cdCookieName = '_iCD';


6. Vključi CD template, v vse strani, v <HEAD>, torej najbolje v nek header.ss. Vključi čim višje, najbolje, če kar prva stvar...
<% include CookiesDirective %>


Če impletmentiraš tip A (samo OK), si končal. POTESTIRAJ!


7. Če implementiraš tip B oziroma C (custom), kjer je potrebno izklapljat določene funkcionalnosti v samih templateih, to narediš takole:

A. v javascriptu, PREFERIRANO, ker nimaš "problemov" s cachem. Celoten vsadek, oziroma inicializacijo le tega, lahko zaifaš na dva načina:

A.1. s callbackom, ki poskrbi, da se zadeva pokliče ob spremembah nastavitve:
_iCD.canRegister(function(allowed) {
	if (allowed) {
		// tukaj pride vsadek oziroma inicializacija le tega
	}
});	

A.2. z navadnim ifom, ki se izvede samo ob inicializaciji strani:
if (_iCD.canUse()) {
	// tukaj pride vsadek oziroma inicializacija le tega
}
	
B. serverside.
	- pripravljeno imaš funckcijo CookiesDirective_ControllerExtension::cdCanUse() ki vrne, ali je dovoljena uporaba ali ne
	- vsadke oziroma inicializacijo le teh poifaš glede na gornjo funkcijo.
	
	- vsi templati, v katerih boš ifal, morajo biti includani z odvisnostjo od gornje funkcije!!! TO JE ZELO POMEMBNO.
	zato TESTIRAJ, TESTIRAJ, TESTIRAJ!
