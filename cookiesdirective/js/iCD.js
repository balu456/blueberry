/**
 * CookiesDirective module
 * 
 * @see README.txt for usage.
 */
var _iCD = {
	v: 1.08,
	snipet: null,
	_canUse: null,
	options: {
		cookieName: "_iCD",
		cookieExpires: 90,
		path: "/",
		domain: "",
		defaultUse: false,
		showOnlyOnce: true,
		profileUse: null
	},
	_cbs:[],
	jQ: function(){},
	defSnipets: {},

	/**
	 * Initializes snippet.
	 * If options.profileUse is set, it overides cookies settigns,
	 * otherwise, setting is read from cookie value and extended,
	 * if needed.
	 * 
	 * Param snipet can be full URIComponentencoced HTML snippet,
	 * or twochar code, which refers to options.defSnipets, where is
	 * the actual URIComponentencoced HTML snippet.
	 * 
	 * Init can and should called immediately after loading js.
	 * 
	 * After init is called, you can safely use canUse and canRegister.
	 * 
	 * @param 		string		snipet
	 * @param		object		options
	 * @return 		void
	 */
	init: function (snipet, options) {
		if (typeof(options) == "object") {
			for(key in options) this.options[key] = options[key];
		}
		
		if (this.options.profileUse ==  null) {
			if (snipet.length==2 && typeof(this.defSnipets[snipet]) != "undefined" ) {
				this.snipet = this.defSnipets[snipet];
			} else {
				this.snipet = snipet;
			}
			this._cookieVal = this.cks.cookie(this.options.cookieName);
	
			if (typeof(this._cookieVal) != "undefined") {
				var cookieArr = this._cookieVal.split(";");
				this._canUse = (cookieArr[0] == "true");
				this._persistent = (cookieArr[1] == "P");
				
				// extend expiration
				if (this._persistent) this._setCookie();
			}
		} else {
			this._canUse = this.options.profileUse;
		}
	},
	
	/**
	 * Sets cookie usage cookie according to current settings:
	 * - this.canUse(),
	 * - this._persistent
	 * - this.options.cookieName
	 * - this.options.cookieExpires
	 * - this.options.path
	 * - this.options.domain
	 * 
	 * @internal
	 * @return 		void
	 */
	_setCookie: function (canUse, isPersistant) {
		var cVal = ((typeof canUse != 'undefined') ? canUse : (this.canUse() ? "true" : "false")) + ";" + ((typeof isPersistant != 'undefined') ? isPersistant : (this._persistent ? "P" : "S"));
		//var cVal = (this.canUse() ? "true" : "false") + ";" + (this._persistent ? "P" : "S");
		var cExp = this._persistent ? this.options.cookieExpires : 0;
		this.cks.cookie(this.options.cookieName,cVal,{expires: cExp, path: this.options.path, domain: this.options.domain});
	},
	
	/**
	 * Shows snipet, if this.options.profileUse is not set and cookie is not set.
	 * If force = true, it shows snipet regardless of cookie setting.
	 * 
	 * @param 		bool	force
	 * @return		void
	 */
	show: function (force) {
		// nothing, if forcing use
		if (this.options.showOnlyOnce && typeof(this._cookieVal) == "undefined") this._setCookie('true', 'S');
		//if (this.options.profileUse !=  null) return;
		
		// show only if force, or no cookie is set
		if (force===true || typeof(this._cookieVal) == "undefined") {
			this._initSnipet();
			this.$snipet.fadeIn();
			this.hide();
		}
	},
	
	/**
	 * Binds jQuery referenc and elements with clas iCD_show to show
	 * snippet.
	 * It should be called imidiately after jQuery is loaded.
	 * 
	 * @param 		function ref	pointer to jQuery
	 * @return		void
	 */
	bind: function (jqRef) {
		this.jQ=jqRef;
		var self=this;
		this.jQ(".iCD_show").click(function(e) {
			e.preventDefault();
			self.show(true);
		});
	},
	
	/**
	 * Bind close 
	 */
	hide: function() {
		this.jQ('#_iCD .close').click(function(e){
			e.preventDefault();
			_iCD.jQ('#_iCD').hide().addClass('is-closed');
		});
	},
	
	/**
	 * Prepares snipet - creates a DOM element and inserts it.
	 * 
	 * @internal
	 * @return		void
	 */
	_initSnipet: function () {
		if (typeof(this.$snipet) == "undefined") {
			this.$snipet = this.jQ(decodeURIComponent(this.snipet));
			var self=this;
			this.jQ(".iCD_conf",this.$snipet).click(function(e) {
				e.preventDefault();
				self.decide(true, self.jQ("#iCD_allw",self.$snipet).is(":checked"));
				self.$snipet.hide();
			});
			this.jQ(".iCD_deny",this.$snipet).click(function(e) {
				e.preventDefault();
				self.decide(false, self.jQ("#iCD_allw",self.$snipet).is(":checked"));
				self.$snipet.hide();
			});
			this.$snipet.hide();
			
			this.jQ("body").append(this.$snipet);
		}		
	},
	
	/**
	 * Receives decision on cookies. This function is binded to click events
	 * on agree and disagree buttons.
	 * 
	 * 
	 * @param 		bool	decision	True, if cookies allowed
	 * @param 		bool	remember	True, if remember setting
	 * @return		void
	 */
	decide: function(decision,remember) {
		if (this._canUse!=decision) {
			this._canUse=decision;
			this._dispatch();
		}
		this._persistent=(remember);
		this._setCookie();
	},
	
	/**
	 * Resets the state of plugin/cookies.
	 * 
	 * @returns 	void
	 */
	reset: function () {
		this.cks.removeCookie(this.options.cookieName);
		
		this._canUse = null;
		delete(this._persistent);
		return true;
	},
	
	/**
	 * Return true if we are allowed to use cookies.
	 * 
	 * See README.txt for examples
	 * 
	 * @returns		bool
	 */
	canUse: function () {
		return this._canUse == null ? this.options.defaultUse : this._canUse;
	},
	
	/**
	 * Registers a callback, which is called imidiately (unless onFirst=false) and
	 * on each change of state of canUse()
	 * 
	 * See README.txt for examples
	 * 
	 * @param 		functionRef		callback	receives bool argument
	 * @param 		bool			onFirst		call imidiately, true by default
	 * @return		void
	 */
	canRegister: function (callback, onFirst) {
		if (typeof(callback)=="function") {
			this._cbs.push(callback);
			if (onFirst !== false) callback(this.canUse());
		}
	},
	
	/**
	 * Dispatches change to all registered handlers.
	 * 
	 * @internal
	 * @return		void
	 */
	_dispatch: function() {
		for(var i=0;i<this._cbs.length;i++) {
			this._cbs[i](this.canUse());
		}
	},
	
	/**
	 * Simple helper function for asnyc loading of scripts
	 * 
	 * @param		string		Script URL
	 * @param		string		element tag, before which to insert new script tag. Default 'script'
	 * @return		void
	 */
	loadJs: function (src,beforeTag) {
	    var scriptEl = document.createElement('script'); scriptEl.type = 'text/javascript';
	    scriptEl.src = src; scriptEl.async=true;
	    if (typeof(beforeTag)=="undefined")beforeTag="script";
	    var s = document.getElementsByTagName(beforeTag)[0]; s.parentNode.insertBefore(scriptEl, s);
	},
	
	// cookies plugin taken from https://github.com/carhartl/jquery-cookie
	cks: {
		extend: function (obj1,obj2) {
		    var obj3 = {};
		    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
		    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
		    return obj3;
		},

		pluses: /\+/g,

		raw: function (s) {
			return s;
		},
		
		decoded: function (s) {
			return decodeURIComponent(s.replace(this.pluses, " "));
		},
		
		converted: function (s) {
			if (s.indexOf("\"") === 0) {
				// This is a quoted cookie as according to RFC2068, unescape
				s = s.slice(1, -1).replace(/\\"/g, "\"").replace(/\\\\/g, "\\");
			}
			try {
				return this.config.json ? JSON.parse(s) : s;
			} catch(er) {}
		},
		
		cookie: function (key, value, options) {
		
			// write
			if (value !== undefined) {
				options = this.extend(this.config.defaults, options);
		
				if (typeof options.expires === "number" && options.expires>0) {
					var days = options.expires, t = options.expires = new Date();
					t.setDate(t.getDate() + days);
				}
		
				value = this.config.json ? JSON.stringify(value) : String(value);
		
				return (document.cookie = [
					this.config.raw ? key : encodeURIComponent(key),
					"=",
					this.config.raw ? value : encodeURIComponent(value),
					options.expires ? "; expires=" + options.expires.toUTCString() : "", // use expires attribute, max-age is not supported by IE
					options.path    ? "; path=" + options.path : "",
					options.domain  ? "; domain=" + options.domain : "",
					options.secure  ? "; secure" : ""
				].join(""));
			}
		
			// read
			var decode = this.config.raw ? this.raw : this.decoded;
			var cookies = document.cookie.split("; ");
			var result = key ? undefined : {};
			for (var i = 0, l = cookies.length; i < l; i++) {
				var parts = cookies[i].split("=");
				var name = decode(parts.shift());
				var cookie = decode(parts.join("="));
		
				if (key && key === name) {
					result = this.converted(cookie);
					break;
				}
		
				if (!key) {
					result[name] = this.converted(cookie);
				}
			}
		
			return result;
		},
		
		config: {defaults: {}},
		
		removeCookie: function (key, options) {
			if (this.cookie(key) !== undefined) {
				// Must not alter options, thus extending a fresh object...
				this.cookie(key, "", this.extend(options, { expires: -1 }));
				return true;
			}
			return false;
		}
	}
}