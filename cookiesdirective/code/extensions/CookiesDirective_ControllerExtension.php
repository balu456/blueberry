<?php
/**
 * Cookies Directive extension
 * 
 * 
 * @author blaz
 *
 */
class CookiesDirective_ControllerExtension extends Extension {
	
	/**
	 * Default state of using third party...
	 * 
	 * @var boolean
	 */
	public static $cdDefaultUse = false;
	
	/**
	 * Type of message. Default = A
	 * 
	 * @var string
	 */
	public static $cdType = 'A';
	
	/**
	 * Expiration time in days of decision persistent cookie. Default = 90
	 * 
	 * @var int
	 */
	public static $cdCookieExpiration = 90;
	
	/**
	 * Name of cookie for storing decision
	 * 
	 * @var string
	 */
	public static $cdCookieName = '_iCD';
	
	
	/**
	 * Show dialog box only once
	 *
	 */
	public static $cdShowOnlyOnce = true;
	
	
	/**
	 * Getter for $cdType
	 * 
	 * @return string
	 */
	public function getcdType() {
		return self::$cdType;
	}
	
	/**
	 * Getter for $cdCookieExpiration
	 * 
	 * @return number
	 */
	public function getcdCookieExpiration() {
		return self::$cdCookieExpiration;
	}
	
	/**
	 * Getter for $cdCookieName
	 * 
	 * @return string
	 */
	public function getcdCookieName() {
		return self::$cdCookieName;
	}

	/**
	 * Getter for $cdDefaultUse
	 *
	 * @return string
	 */
	public function getcdDefaultUse() {
		return self::$cdDefaultUse ? 'true' : 'false';
	}
	
	/**
	 * Getter for $cdShowOnlyOnce
	 *
	 * @return string
	 */
	public function getcdShowOnlyOnce() {
		return self::$cdShowOnlyOnce ? 'true' : 'false';
	}
	
	
	/**
	 * Returns cookie path. Defaults to baseURL. 
	 * Change only, if you know, what you're doing!
	 * 
	 * @return string
	 */
	public function getcdCookiePath () {
		return Director::baseURL();
	}
	
	/**
	 * Prepares snippet. Finds link to explanation page, loads correct template, compiles it, and
	 * urlencodes it.
	 * 
	 * @return string
	 */
	public function getcdSnippet() {		
		$template =  new SSViewer('CookiesDirectiveType' . self::getcdType());
		
		$explainPage = CookiesDirectiveExplain::get_one('CookiesDirectiveExplain');

		$explainLink = $explainPage ? $explainPage->RelativeLink() : '';
		
		$content = sprintf(_t('CookiesDirective.content_'.self::getcdType()),$explainLink);
		
		$data = new ArrayData(array('cdContent'	=> $content));
		
		$snippet = rawurlencode(preg_replace( '/\s+/', ' ', $template->dontRewriteHashlinks()->process($data)));		
		
		return $snippet;
	}
	
	/**
	 * Returns setting of Cookies Directive cookie.
	 * 
	 * @return boolean
	 */
	public static function cdCanUse() {
		if (!isset($_COOKIE[self::getcdCookieName()])) return self::$cdDefaultUse;
		
		return (substr($_COOKIE[self::getcdCookieName()],0,4) == "true");
	}
}