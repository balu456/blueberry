<div id="_iCD">
	<div class="iCD_wrp">
		<div class="iCD_lf">
			<div class="iCD_inr">
				<span class="iCD_bkg"></span>
				<h3><% _t('CookiesDirective.headline_A') %></h3>
				<p>$cdContent</p>
			</div>
		</div>
		<div class="iCD_rt">
			<div class="iCD_inr">
				<h3><% _t('CookiesDirective.question_A') %></h3>
				<div class="iCD_btn">
					<a class="iCD_conf" href="#"><% _t('CookiesDirective.confirm_A') %></a>
				</div>
				<div class="iCD_chk"><input id="iCD_allw" type="checkbox" name="iCD_allw" checked="checked" /><label for="iCD_allw"><% _t('CookiesDirective.remember_A') %></label></div>
			</div>
		</div>
	</div>
</div>