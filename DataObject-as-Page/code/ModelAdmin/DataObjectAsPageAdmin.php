<?php

class DataObjectAsPageAdmin extends ModelAdmin 
{
	
	private static $tree_class = 'DataObjectAsPage';
	
	public function init() 
	{
		Translatable::disable_locale_filter();
		
	    parent::init();

	    //if versioned we need to tell ModelAdmin to read from stage
		if(Singleton($this->modelClass)->isVersioned)
		{		
			Versioned::reading_stage('Stage');
		}
		//Styling for preview links and status
		Requirements::CSS(MOD_DOAP_DIR . '/css/dataobjectaspageadmin.css');
	}
	
	public function getEditForm($id = null, $fields = null) {
	    $list = $this->getList();
	    
	    $config = GridFieldConfig_RecordEditor::create();
	    if($detailForm = $config->getComponentByType('GridFieldDetailForm')) {
	    	$detailForm->setItemRequestClass('DOAsPageAdminDetailForm_ItemRequest');
	    }
	    
	    $listField = GridField::create($this->sanitiseClassName($this->modelClass), false, $list, $config);
	    
	    // Validation
	    if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
	    	$detailValidator = singleton($this->modelClass)->getCMSValidator();
	    	$config->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
	    }
	    
	    $form = new Form(
    		$this,
    		'EditForm',
    		new FieldList($listField),
    		new FieldList()
	    );
	    $form->addExtraClass('cms-edit-form cms-panel-padded center');
	    $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
	    $form->setFormAction(Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm'));
	    $form->setAttribute('data-pjax-fragment', 'CurrentForm');
	    
	    
	    
	    
	    if(Singleton($this->modelClass)->isVersioned)
	    {
	    	$listfield = $form->Fields()->fieldByName($this->modelClass);
	    
	    	$gridFieldConfig = $listfield->getConfig();
	    
	    	$gridFieldConfig->getComponentByType('GridFieldDetailForm')
	    	->setItemRequestClass('VersionedGridFieldDetailForm_ItemRequest');
	    
	    	$gridFieldConfig->removeComponentsByType('GridFieldDeleteAction');
	    	$gridFieldConfig->addComponent(new VersionedGridFieldDeleteAction());
	    }	    
	    
	    
	    
	    
	    $this->extend('updateEditForm', $form);
	    
	    return $form;	    
	}
}

/*
 * Temporary Fix for HTML editor Image/Link popup
 */
class ModelAdminHtmlEditorField_Toolbar extends HtmlEditorField_Toolbar {
	 
   public function forTemplate() { 
      return sprintf( 
         '<div id="cms-editor-dialogs" data-url-linkform="%s" data-url-mediaform="%s"></div>', 
         Controller::join_links($this->controller->Link(), $this->name, 'LinkForm', 'forTemplate'), 
         Controller::join_links($this->controller->Link(), $this->name, 'MediaForm', 'forTemplate') 
      ); 
   } 
}