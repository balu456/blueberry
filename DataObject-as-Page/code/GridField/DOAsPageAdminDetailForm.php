<?php

class DOAsPageAdminDetailForm extends GridFieldDetailForm {

}

class DOAsPageAdminDetailForm_ItemRequest extends GridFieldDetailForm_ItemRequest {

	private static $allowed_actions = array(
		'ItemEditForm',
		'createtranslation'
	);

	function ItemEditForm() {
		$form = parent::ItemEditForm();
		$form->addExtraClass('LeftAndMain');
		return $form;
	}

	function createtranslation($data, $form) {
		$controller = Controller::curr();
		$request    = $controller->getRequest();
		if(!$this->record) return $controller->httpError(404);
		$langCode   = Convert::raw2sql($request->postVar('NewTransLang'));
		$translation = $this->record->createTranslation($langCode);
		$this->record = $translation;
		return $this->edit($controller->getRequest());
	}
}