<?php

class DataObjectAsPageHolder extends Page {

	private static $description = 'DataObjectAsPage holder seznam';
	private static $singular_name = 'DataObjectAsPage holder seznam';
}

class DataObjectAsPageHolder_Controller extends Page_Controller {
	
	private static $url_handlers = array(
		'$ID/$OtherID' => 'handleAction'
	);
	
	/**
	 * Override handle action
	 */
	protected function handleAction($request, $action) {
		$params = $request->allParams();
		
		$id = $params['ID'];
		if (!isset($id) && $action != 'index') $id = $action;
		if (!isset($id)) return $this->index();
	
		if ($this->hasMethod($id)) return parent::handleAction($request, $id);
	
		$item = $this->getCurrentItem();
		if ($item) return $this->showDetail($item);
	
		return $this->httpError(404);
	}
	
	public function index() {
		return $this;
	}
	
	/**
	 * Get current item based on url segment
	 *
	 * @return DataObjectAsPage|boolean
	 */
	public function getCurrentItem() {
		$params = $this->getRequest()->allParams();
	
		$URLSegment = $params['ID'];
		if (!isset($URLSegment)) return false;
		
		$class = $this->getItemClass();
	
		return $class::get()->filter('URLSegment', Convert::raw2sql($URLSegment))->first();
	}
	
	/**
	 * Render item detail
	 *
	 * @param DataObjectAsPage $item
	 */
	public function showDetail($item) {
		if (!$item->canView()) return Security::permissionFailure($this);
	
		$data = array(
			'Item'			=> $item,
			'Breadcrumbs'	=> $item->Breadcrumbs(false, false, 'RedirectorPage', true),
			'MetaTags'		=> $item->MetaTags(false),
			'Title'			=> $item->Title
		);
		
		if ($this->hasMethod('updateShowDetailData')) $data = $this->updateShowDetailData($data);
	
		return $this->customise($data)->renderWith(array($this->getItemClass() . 'Detail', 'Page', 'ContentController'));
	}
	
}