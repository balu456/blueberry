<?php

class Innalytics extends Controller {
	
	function generateEtag() {
		$id = round(2147483647 * (float)rand() / (float)getrandmax());
		$id = sha1($id);
		$id = str_replace(array('.', '/', '\\'), '', $id);
		$id = substr($id, 0, 18);
		
		return $id;
	}
	
	function setEtag($id) {
		Session::set('etag', $id);
	}
	
	function getEtag() {
		$etag = Session::get('etag');
		
		if (!isset($etag) || empty($etag)) {
			$etag = $this->generateEtag();
			$this->setEtag($etag);
		}
		
		return $etag;
	}
	
	function getAnalyticsClientID() {
		$key = 'key1234554321yek';
		return sha1($this->getEtag());
	}
	
	public function index() {
		
		$siteConfig = SiteConfig::get()->filter('Locale', Translatable::get_current_locale())->First();
		$ga = $siteConfig->GoogleAnalyticsCode;
		
		if (!Director::isLive() || !$ga) return new SS_HTTPResponse('Not allowed', 405);
		
		$response = $this->getResponse();
		
		if (!empty($_SERVER['HTTP_IF_NONE_MATCH'])) $etag = $_SERVER['HTTP_IF_NONE_MATCH'];
		else $etag = $this->getEtag();
		
		$this->setEtag($etag);

		$headers = array(
			'Content-Type' => 'text/javascript',
			'Cache-Control' => 'private, must-revalidate, proxy-revalidate',
			'ETag' => $etag
		);
		
		foreach ($headers as $k => $v) $response->addHeader($k, $v);
		
		$js = "
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		";
		
		$js .= str_replace('$ClientID', $this->getAnalyticsClientID(), $ga);
		
		$response->setBody($js);
		
		return $response->output();
	}
		
}
